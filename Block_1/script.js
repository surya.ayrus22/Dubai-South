TDV.PlayerAPI.defineScript({ "definitions": [
 {
  "thumbnailUrl": "media/panorama_B868E676_BEAB_5F51_41D7_6B136C89F0F3_t.jpg",
  "hfovMin": 60,
  "id": "panorama_B868E676_BEAB_5F51_41D7_6B136C89F0F3",
  "pitch": 0,
  "hfov": 360,
  "vfov": 180,
  "label": "A BLock 1",
  "partial": false,
  "frames": [
   {
    "top": {
     "class": "ImageResource",
     "levels": [
      {
       "height": 1904,
       "url": "media/panorama_B868E676_BEAB_5F51_41D7_6B136C89F0F3_u_hq.jpeg",
       "width": 1904,
       "class": "ImageResourceLevel"
      },
      {
       "height": 800,
       "url": "media/panorama_B868E676_BEAB_5F51_41D7_6B136C89F0F3_u.jpeg",
       "width": 800,
       "class": "ImageResourceLevel"
      }
     ]
    },
    "front": {
     "class": "ImageResource",
     "levels": [
      {
       "height": 1904,
       "url": "media/panorama_B868E676_BEAB_5F51_41D7_6B136C89F0F3_f_hq.jpeg",
       "width": 1904,
       "class": "ImageResourceLevel"
      },
      {
       "height": 800,
       "url": "media/panorama_B868E676_BEAB_5F51_41D7_6B136C89F0F3_f.jpeg",
       "width": 800,
       "class": "ImageResourceLevel"
      }
     ]
    },
    "right": {
     "class": "ImageResource",
     "levels": [
      {
       "height": 1904,
       "url": "media/panorama_B868E676_BEAB_5F51_41D7_6B136C89F0F3_r_hq.jpeg",
       "width": 1904,
       "class": "ImageResourceLevel"
      },
      {
       "height": 800,
       "url": "media/panorama_B868E676_BEAB_5F51_41D7_6B136C89F0F3_r.jpeg",
       "width": 800,
       "class": "ImageResourceLevel"
      }
     ]
    },
    "overlays": [
     {
      "useHandCursor": true,
      "rollOverDisplay": false,
      "items": [
       {
        "image": {
         "class": "ImageResource",
         "levels": [
          {
           "height": 46,
           "url": "media/panorama_B868E676_BEAB_5F51_41D7_6B136C89F0F3_0_HS_0_0.png",
           "width": 46,
           "class": "ImageResourceLevel"
          }
         ]
        },
        "yaw": 0.7672398278880409,
        "pitch": 2.453812803690921,
        "roll": 0,
        "hfov": 2.7950722829894783,
        "class": "HotspotPanoramaOverlayImage"
       }
      ],
      "areas": [
       {
        "toolTip": "Reception",
        "mapColor": "#FF0000",
        "class": "HotspotPanoramaOverlayArea",
        "click": "this.mainPlayList.set('selectedIndex', 6)"
       }
      ],
      "id": "overlay_B6A0E81C_BEAC_D2D1_41B4_D71B60606C31",
      "maps": [
       {
        "image": {
         "class": "ImageResource",
         "levels": [
          {
           "height": 23,
           "url": "media/panorama_B868E676_BEAB_5F51_41D7_6B136C89F0F3_0_HS_0_0_0_map.gif",
           "width": 23,
           "class": "ImageResourceLevel"
          }
         ]
        },
        "yaw": 0.7672398278880409,
        "pitch": 2.453812803690921,
        "roll": 0,
        "class": "HotspotPanoramaOverlayMap",
        "hfov": 2.7950722829894783
       }
      ],
      "class": "HotspotPanoramaOverlay",
      "enabledInCardboard": true
     }
    ],
    "back": {
     "class": "ImageResource",
     "levels": [
      {
       "height": 1904,
       "url": "media/panorama_B868E676_BEAB_5F51_41D7_6B136C89F0F3_b_hq.jpeg",
       "width": 1904,
       "class": "ImageResourceLevel"
      },
      {
       "height": 800,
       "url": "media/panorama_B868E676_BEAB_5F51_41D7_6B136C89F0F3_b.jpeg",
       "width": 800,
       "class": "ImageResourceLevel"
      }
     ]
    },
    "bottom": {
     "class": "ImageResource",
     "levels": [
      {
       "height": 1904,
       "url": "media/panorama_B868E676_BEAB_5F51_41D7_6B136C89F0F3_d_hq.jpeg",
       "width": 1904,
       "class": "ImageResourceLevel"
      },
      {
       "height": 800,
       "url": "media/panorama_B868E676_BEAB_5F51_41D7_6B136C89F0F3_d.jpeg",
       "width": 800,
       "class": "ImageResourceLevel"
      }
     ]
    },
    "thumbnailUrl": "media/panorama_B868E676_BEAB_5F51_41D7_6B136C89F0F3_t.jpg",
    "left": {
     "class": "ImageResource",
     "levels": [
      {
       "height": 1904,
       "url": "media/panorama_B868E676_BEAB_5F51_41D7_6B136C89F0F3_l_hq.jpeg",
       "width": 1904,
       "class": "ImageResourceLevel"
      },
      {
       "height": 800,
       "url": "media/panorama_B868E676_BEAB_5F51_41D7_6B136C89F0F3_l.jpeg",
       "width": 800,
       "class": "ImageResourceLevel"
      }
     ]
    },
    "class": "CubicPanoramaFrame"
   }
  ],
  "hfovMax": 120,
  "cardboardMenu": {
   "rollOverBackgroundColor": "#000000",
   "backgroundColor": "#404040",
   "opacity": 0.4,
   "id": "Menu_CB8FF3CE_C0CD_DF70_41D3_23232EE4C562",
   "rollOverFontColor": "#FFFFFF",
   "rollOverOpacity": 0.8,
   "fontFamily": "Arial",
   "label": "Panoramas",
   "selectedFontColor": "#FFFFFF",
   "selectedBackgroundColor": "#202020",
   "children": [
    {
     "click": "this.mainPlayList.set('selectedIndex', 0)",
     "class": "MenuItem",
     "label": "A BLock 1"
    },
    {
     "click": "this.mainPlayList.set('selectedIndex', 1)",
     "class": "MenuItem",
     "label": "A BLock 8"
    },
    {
     "click": "this.mainPlayList.set('selectedIndex', 2)",
     "class": "MenuItem",
     "label": "A Block 5"
    },
    {
     "click": "this.mainPlayList.set('selectedIndex', 3)",
     "class": "MenuItem",
     "label": "A Block 7"
    },
    {
     "click": "this.mainPlayList.set('selectedIndex', 4)",
     "class": "MenuItem",
     "label": "A Block 6"
    },
    {
     "click": "this.mainPlayList.set('selectedIndex', 5)",
     "class": "MenuItem",
     "label": "A BLock 3"
    },
    {
     "click": "this.mainPlayList.set('selectedIndex', 6)",
     "class": "MenuItem",
     "label": "A BLock 2"
    },
    {
     "click": "this.mainPlayList.set('selectedIndex', 7)",
     "class": "MenuItem",
     "label": "A Block 4"
    }
   ],
   "fontColor": "#FFFFFF",
   "class": "Menu"
  },
  "class": "Panorama"
 },
 {
  "buttonMoveUp": {
   "width": 32,
   "height": 32,
   "id": "IconButton_C9A4076F_C016_AD3C_41D5_1891DE7BEAB9",
   "mode": "push",
   "paddingTop": 0,
   "transparencyActive": false,
   "class": "IconButton",
   "pressedIconURL": "skin/IconButton_C9A4076F_C016_AD3C_41D5_1891DE7BEAB9_pressed.png",
   "cursor": "hand",
   "horizontalAlign": "center",
   "borderRadius": 0,
   "minWidth": 0,
   "verticalAlign": "middle",
   "minHeight": 0,
   "paddingBottom": 0,
   "borderSize": 0,
   "iconURL": "skin/IconButton_C9A4076F_C016_AD3C_41D5_1891DE7BEAB9.png",
   "rollOverIconURL": "skin/IconButton_C9A4076F_C016_AD3C_41D5_1891DE7BEAB9_rollover.png",
   "paddingLeft": 0,
   "backgroundOpacity": 0,
   "paddingRight": 0,
   "shadow": false
  },
  "buttonRestart": {
   "width": 40,
   "height": 40,
   "id": "IconButton_C9A4076F_C016_AD3C_41E4_D71E62A0309A",
   "mode": "push",
   "paddingTop": 0,
   "transparencyActive": false,
   "class": "IconButton",
   "pressedIconURL": "skin/IconButton_C9A4076F_C016_AD3C_41E4_D71E62A0309A_pressed.png",
   "cursor": "hand",
   "horizontalAlign": "center",
   "borderRadius": 0,
   "minWidth": 0,
   "verticalAlign": "middle",
   "minHeight": 0,
   "paddingBottom": 0,
   "borderSize": 0,
   "iconURL": "skin/IconButton_C9A4076F_C016_AD3C_41E4_D71E62A0309A.png",
   "rollOverIconURL": "skin/IconButton_C9A4076F_C016_AD3C_41E4_D71E62A0309A_rollover.png",
   "paddingLeft": 0,
   "backgroundOpacity": 0,
   "paddingRight": 0,
   "shadow": false
  },
  "mouseControlMode": "drag_acceleration",
  "buttonZoomOut": {
   "width": 32,
   "height": 32,
   "id": "IconButton_C9A4076E_C016_AD3D_41E4_2C26E9C95BF3",
   "mode": "push",
   "paddingTop": 0,
   "transparencyActive": false,
   "class": "IconButton",
   "pressedIconURL": "skin/IconButton_C9A4076E_C016_AD3D_41E4_2C26E9C95BF3_pressed.png",
   "cursor": "hand",
   "horizontalAlign": "center",
   "borderRadius": 0,
   "minWidth": 0,
   "verticalAlign": "middle",
   "minHeight": 0,
   "paddingBottom": 0,
   "borderSize": 0,
   "iconURL": "skin/IconButton_C9A4076E_C016_AD3D_41E4_2C26E9C95BF3.png",
   "rollOverIconURL": "skin/IconButton_C9A4076E_C016_AD3D_41E4_2C26E9C95BF3_rollover.png",
   "paddingLeft": 0,
   "backgroundOpacity": 0,
   "paddingRight": 0,
   "shadow": false
  },
  "buttonPlayRight": {
   "width": 40,
   "height": 40,
   "id": "IconButton_C9A4076F_C016_AD3C_41D0_F7C35F666D44",
   "mode": "push",
   "paddingTop": 0,
   "transparencyActive": false,
   "class": "IconButton",
   "pressedIconURL": "skin/IconButton_C9A4076F_C016_AD3C_41D0_F7C35F666D44_pressed.png",
   "cursor": "hand",
   "horizontalAlign": "center",
   "borderRadius": 0,
   "minWidth": 0,
   "verticalAlign": "middle",
   "minHeight": 0,
   "paddingBottom": 0,
   "borderSize": 0,
   "iconURL": "skin/IconButton_C9A4076F_C016_AD3C_41D0_F7C35F666D44.png",
   "rollOverIconURL": "skin/IconButton_C9A4076F_C016_AD3C_41D0_F7C35F666D44_rollover.png",
   "paddingLeft": 0,
   "backgroundOpacity": 0,
   "paddingRight": 0,
   "shadow": false
  },
  "id": "MainViewerPanoramaPlayer",
  "class": "PanoramaPlayer",
  "viewerArea": "this.MainViewer",
  "buttonPause": {
   "width": 40,
   "height": 40,
   "id": "IconButton_C9A4076F_C016_AD3C_41E4_270B93306BD7",
   "mode": "toggle",
   "paddingTop": 0,
   "transparencyActive": false,
   "class": "IconButton",
   "pressedIconURL": "skin/IconButton_C9A4076F_C016_AD3C_41E4_270B93306BD7_pressed.png",
   "cursor": "hand",
   "horizontalAlign": "center",
   "borderRadius": 0,
   "minWidth": 0,
   "verticalAlign": "middle",
   "minHeight": 0,
   "paddingBottom": 0,
   "borderSize": 0,
   "iconURL": "skin/IconButton_C9A4076F_C016_AD3C_41E4_270B93306BD7.png",
   "paddingLeft": 0,
   "backgroundOpacity": 0,
   "paddingRight": 0,
   "shadow": false
  },
  "buttonMoveLeft": {
   "width": 32,
   "height": 32,
   "id": "IconButton_C9A4076F_C016_AD3C_41B3_7354899C7681",
   "mode": "push",
   "paddingTop": 0,
   "transparencyActive": false,
   "class": "IconButton",
   "pressedIconURL": "skin/IconButton_C9A4076F_C016_AD3C_41B3_7354899C7681_pressed.png",
   "cursor": "hand",
   "horizontalAlign": "center",
   "borderRadius": 0,
   "minWidth": 0,
   "verticalAlign": "middle",
   "minHeight": 0,
   "paddingBottom": 0,
   "borderSize": 0,
   "iconURL": "skin/IconButton_C9A4076F_C016_AD3C_41B3_7354899C7681.png",
   "rollOverIconURL": "skin/IconButton_C9A4076F_C016_AD3C_41B3_7354899C7681_rollover.png",
   "paddingLeft": 0,
   "backgroundOpacity": 0,
   "paddingRight": 0,
   "shadow": false
  },
  "touchControlMode": "drag_rotation",
  "buttonMoveDown": {
   "width": 32,
   "height": 32,
   "id": "IconButton_C9A4076F_C016_AD3C_41B6_2F9CA2436D0D",
   "mode": "push",
   "paddingTop": 0,
   "transparencyActive": false,
   "class": "IconButton",
   "pressedIconURL": "skin/IconButton_C9A4076F_C016_AD3C_41B6_2F9CA2436D0D_pressed.png",
   "cursor": "hand",
   "horizontalAlign": "center",
   "borderRadius": 0,
   "minWidth": 0,
   "verticalAlign": "middle",
   "minHeight": 0,
   "paddingBottom": 0,
   "borderSize": 0,
   "iconURL": "skin/IconButton_C9A4076F_C016_AD3C_41B6_2F9CA2436D0D.png",
   "rollOverIconURL": "skin/IconButton_C9A4076F_C016_AD3C_41B6_2F9CA2436D0D_rollover.png",
   "paddingLeft": 0,
   "backgroundOpacity": 0,
   "paddingRight": 0,
   "shadow": false
  },
  "buttonZoomIn": {
   "width": 32,
   "height": 32,
   "id": "IconButton_C9A4076F_C016_AD3C_41D2_872690316C57",
   "mode": "push",
   "paddingTop": 0,
   "transparencyActive": false,
   "class": "IconButton",
   "pressedIconURL": "skin/IconButton_C9A4076F_C016_AD3C_41D2_872690316C57_pressed.png",
   "cursor": "hand",
   "horizontalAlign": "center",
   "borderRadius": 0,
   "minWidth": 0,
   "verticalAlign": "middle",
   "minHeight": 0,
   "paddingBottom": 0,
   "borderSize": 0,
   "iconURL": "skin/IconButton_C9A4076F_C016_AD3C_41D2_872690316C57.png",
   "rollOverIconURL": "skin/IconButton_C9A4076F_C016_AD3C_41D2_872690316C57_rollover.png",
   "paddingLeft": 0,
   "backgroundOpacity": 0,
   "paddingRight": 0,
   "shadow": false
  },
  "buttonMoveRight": {
   "width": 32,
   "height": 32,
   "id": "IconButton_C9A4076F_C016_AD3C_41AE_DED47DEB69C8",
   "mode": "push",
   "paddingTop": 0,
   "transparencyActive": false,
   "class": "IconButton",
   "pressedIconURL": "skin/IconButton_C9A4076F_C016_AD3C_41AE_DED47DEB69C8_pressed.png",
   "cursor": "hand",
   "horizontalAlign": "center",
   "borderRadius": 0,
   "minWidth": 0,
   "verticalAlign": "middle",
   "minHeight": 0,
   "paddingBottom": 0,
   "borderSize": 0,
   "iconURL": "skin/IconButton_C9A4076F_C016_AD3C_41AE_DED47DEB69C8.png",
   "rollOverIconURL": "skin/IconButton_C9A4076F_C016_AD3C_41AE_DED47DEB69C8_rollover.png",
   "paddingLeft": 0,
   "backgroundOpacity": 0,
   "paddingRight": 0,
   "shadow": false
  },
  "buttonPlayLeft": {
   "width": 40,
   "height": 40,
   "id": "IconButton_C9A4076F_C016_AD3C_41E1_98C3CF14C671",
   "mode": "push",
   "paddingTop": 0,
   "transparencyActive": false,
   "class": "IconButton",
   "pressedIconURL": "skin/IconButton_C9A4076F_C016_AD3C_41E1_98C3CF14C671_pressed.png",
   "cursor": "hand",
   "horizontalAlign": "center",
   "borderRadius": 0,
   "minWidth": 0,
   "verticalAlign": "middle",
   "minHeight": 0,
   "paddingBottom": 0,
   "borderSize": 0,
   "iconURL": "skin/IconButton_C9A4076F_C016_AD3C_41E1_98C3CF14C671.png",
   "rollOverIconURL": "skin/IconButton_C9A4076F_C016_AD3C_41E1_98C3CF14C671_rollover.png",
   "paddingLeft": 0,
   "backgroundOpacity": 0,
   "paddingRight": 0,
   "shadow": false
  }
 },
 {
  "initialSequence": {
   "movements": [
    {
     "easing": "cubic_in",
     "yawSpeed": 8.95,
     "yawDelta": 18.5,
     "class": "DistancePanoramaCameraMovement"
    },
    {
     "easing": "linear",
     "yawSpeed": 8.95,
     "yawDelta": 323,
     "class": "DistancePanoramaCameraMovement"
    },
    {
     "easing": "cubic_out",
     "yawSpeed": 8.95,
     "yawDelta": 18.5,
     "class": "DistancePanoramaCameraMovement"
    }
   ],
   "restartMovementOnUserInteraction": false,
   "class": "PanoramaCameraSequence"
  },
  "initialPosition": {
   "yaw": 0,
   "pitch": 0,
   "class": "PanoramaCameraPosition"
  },
  "id": "panorama_B868E676_BEAB_5F51_41D7_6B136C89F0F3_camera",
  "class": "PanoramaCamera",
  "automaticZoomSpeed": 10
 },
 {
  "thumbnailUrl": "media/panorama_B89D82CF_BEAB_774E_41D6_4F5B54AC25AF_t.jpg",
  "hfovMin": 60,
  "id": "panorama_B89D82CF_BEAB_774E_41D6_4F5B54AC25AF",
  "pitch": 0,
  "hfov": 360,
  "vfov": 180,
  "label": "A BLock 8",
  "partial": false,
  "frames": [
   {
    "top": {
     "class": "ImageResource",
     "levels": [
      {
       "height": 1000,
       "url": "media/panorama_B89D82CF_BEAB_774E_41D6_4F5B54AC25AF_u_hq.jpeg",
       "width": 1000,
       "class": "ImageResourceLevel"
      },
      {
       "height": 800,
       "url": "media/panorama_B89D82CF_BEAB_774E_41D6_4F5B54AC25AF_u.jpeg",
       "width": 800,
       "class": "ImageResourceLevel"
      }
     ]
    },
    "front": {
     "class": "ImageResource",
     "levels": [
      {
       "height": 1000,
       "url": "media/panorama_B89D82CF_BEAB_774E_41D6_4F5B54AC25AF_f_hq.jpeg",
       "width": 1000,
       "class": "ImageResourceLevel"
      },
      {
       "height": 800,
       "url": "media/panorama_B89D82CF_BEAB_774E_41D6_4F5B54AC25AF_f.jpeg",
       "width": 800,
       "class": "ImageResourceLevel"
      }
     ]
    },
    "right": {
     "class": "ImageResource",
     "levels": [
      {
       "height": 1000,
       "url": "media/panorama_B89D82CF_BEAB_774E_41D6_4F5B54AC25AF_r_hq.jpeg",
       "width": 1000,
       "class": "ImageResourceLevel"
      },
      {
       "height": 800,
       "url": "media/panorama_B89D82CF_BEAB_774E_41D6_4F5B54AC25AF_r.jpeg",
       "width": 800,
       "class": "ImageResourceLevel"
      }
     ]
    },
    "overlays": [
     {
      "useHandCursor": true,
      "rollOverDisplay": false,
      "items": [
       {
        "image": {
         "class": "ImageResource",
         "levels": [
          {
           "height": 100,
           "url": "media/panorama_B89D82CF_BEAB_774E_41D6_4F5B54AC25AF_0_HS_0_0.png",
           "width": 100,
           "class": "ImageResourceLevel"
          }
         ]
        },
        "yaw": 103.24462461053972,
        "pitch": -1.9267298736976044,
        "roll": 0,
        "hfov": 11.451192559323076,
        "class": "HotspotPanoramaOverlayImage"
       }
      ],
      "areas": [
       {
        "toolTip": "Reception",
        "mapColor": "#FF0000",
        "class": "HotspotPanoramaOverlayArea",
        "click": "this.mainPlayList.set('selectedIndex', 6)"
       }
      ],
      "id": "overlay_C8572987_C00D_A5EC_41C6_47E2344EDA3C",
      "maps": [
       {
        "image": {
         "class": "ImageResource",
         "levels": [
          {
           "height": 50,
           "url": "media/panorama_B89D82CF_BEAB_774E_41D6_4F5B54AC25AF_0_HS_0_0_0_map.gif",
           "width": 50,
           "class": "ImageResourceLevel"
          }
         ]
        },
        "yaw": 103.24462461053972,
        "pitch": -1.9267298736976044,
        "roll": 0,
        "class": "HotspotPanoramaOverlayMap",
        "hfov": 11.451192559323076
       }
      ],
      "class": "HotspotPanoramaOverlay",
      "enabledInCardboard": true
     }
    ],
    "back": {
     "class": "ImageResource",
     "levels": [
      {
       "height": 1000,
       "url": "media/panorama_B89D82CF_BEAB_774E_41D6_4F5B54AC25AF_b_hq.jpeg",
       "width": 1000,
       "class": "ImageResourceLevel"
      },
      {
       "height": 800,
       "url": "media/panorama_B89D82CF_BEAB_774E_41D6_4F5B54AC25AF_b.jpeg",
       "width": 800,
       "class": "ImageResourceLevel"
      }
     ]
    },
    "bottom": {
     "class": "ImageResource",
     "levels": [
      {
       "height": 1000,
       "url": "media/panorama_B89D82CF_BEAB_774E_41D6_4F5B54AC25AF_d_hq.jpeg",
       "width": 1000,
       "class": "ImageResourceLevel"
      },
      {
       "height": 800,
       "url": "media/panorama_B89D82CF_BEAB_774E_41D6_4F5B54AC25AF_d.jpeg",
       "width": 800,
       "class": "ImageResourceLevel"
      }
     ]
    },
    "thumbnailUrl": "media/panorama_B89D82CF_BEAB_774E_41D6_4F5B54AC25AF_t.jpg",
    "left": {
     "class": "ImageResource",
     "levels": [
      {
       "height": 1000,
       "url": "media/panorama_B89D82CF_BEAB_774E_41D6_4F5B54AC25AF_l_hq.jpeg",
       "width": 1000,
       "class": "ImageResourceLevel"
      },
      {
       "height": 800,
       "url": "media/panorama_B89D82CF_BEAB_774E_41D6_4F5B54AC25AF_l.jpeg",
       "width": 800,
       "class": "ImageResourceLevel"
      }
     ]
    },
    "class": "CubicPanoramaFrame"
   }
  ],
  "hfovMax": 120,
  "cardboardMenu": "this.Menu_CB8FF3CE_C0CD_DF70_41D3_23232EE4C562",
  "class": "Panorama"
 },
 {
  "initialSequence": {
   "movements": [
    {
     "easing": "cubic_in",
     "yawSpeed": 8.95,
     "yawDelta": 18.5,
     "class": "DistancePanoramaCameraMovement"
    },
    {
     "easing": "linear",
     "yawSpeed": 8.95,
     "yawDelta": 323,
     "class": "DistancePanoramaCameraMovement"
    },
    {
     "easing": "cubic_out",
     "yawSpeed": 8.95,
     "yawDelta": 18.5,
     "class": "DistancePanoramaCameraMovement"
    }
   ],
   "restartMovementOnUserInteraction": false,
   "class": "PanoramaCameraSequence"
  },
  "initialPosition": {
   "yaw": 0,
   "pitch": 0,
   "class": "PanoramaCameraPosition"
  },
  "id": "panorama_B89D82CF_BEAB_774E_41D6_4F5B54AC25AF_camera",
  "class": "PanoramaCamera",
  "automaticZoomSpeed": 10
 },
 {
  "thumbnailUrl": "media/panorama_B8850684_BEAB_7FB2_41DA_1D5F3F5978FF_t.jpg",
  "hfovMin": 60,
  "id": "panorama_B8850684_BEAB_7FB2_41DA_1D5F3F5978FF",
  "pitch": 0,
  "hfov": 360,
  "vfov": 180,
  "label": "A Block 5",
  "partial": false,
  "frames": [
   {
    "top": {
     "class": "ImageResource",
     "levels": [
      {
       "height": 1904,
       "url": "media/panorama_B8850684_BEAB_7FB2_41DA_1D5F3F5978FF_u_hq.jpeg",
       "width": 1904,
       "class": "ImageResourceLevel"
      },
      {
       "height": 800,
       "url": "media/panorama_B8850684_BEAB_7FB2_41DA_1D5F3F5978FF_u.jpeg",
       "width": 800,
       "class": "ImageResourceLevel"
      }
     ]
    },
    "front": {
     "class": "ImageResource",
     "levels": [
      {
       "height": 1904,
       "url": "media/panorama_B8850684_BEAB_7FB2_41DA_1D5F3F5978FF_f_hq.jpeg",
       "width": 1904,
       "class": "ImageResourceLevel"
      },
      {
       "height": 800,
       "url": "media/panorama_B8850684_BEAB_7FB2_41DA_1D5F3F5978FF_f.jpeg",
       "width": 800,
       "class": "ImageResourceLevel"
      }
     ]
    },
    "right": {
     "class": "ImageResource",
     "levels": [
      {
       "height": 1904,
       "url": "media/panorama_B8850684_BEAB_7FB2_41DA_1D5F3F5978FF_r_hq.jpeg",
       "width": 1904,
       "class": "ImageResourceLevel"
      },
      {
       "height": 800,
       "url": "media/panorama_B8850684_BEAB_7FB2_41DA_1D5F3F5978FF_r.jpeg",
       "width": 800,
       "class": "ImageResourceLevel"
      }
     ]
    },
    "overlays": [
     {
      "useHandCursor": true,
      "rollOverDisplay": false,
      "items": [
       {
        "image": {
         "class": "ImageResource",
         "levels": [
          {
           "height": 93,
           "url": "media/panorama_B8850684_BEAB_7FB2_41DA_1D5F3F5978FF_0_HS_0_0.png",
           "width": 93,
           "class": "ImageResourceLevel"
          }
         ]
        },
        "yaw": 140.18893617840124,
        "pitch": -3.675392333913126,
        "roll": 0,
        "hfov": 5.583766973528798,
        "class": "HotspotPanoramaOverlayImage"
       }
      ],
      "areas": [
       {
        "toolTip": "Lounge ",
        "mapColor": "#FF0000",
        "class": "HotspotPanoramaOverlayArea",
        "click": "this.mainPlayList.set('selectedIndex', 7)"
       }
      ],
      "id": "overlay_C8E93613_C012_AEE4_41D4_706B0B4C6C70",
      "maps": [
       {
        "image": {
         "class": "ImageResource",
         "levels": [
          {
           "height": 46,
           "url": "media/panorama_B8850684_BEAB_7FB2_41DA_1D5F3F5978FF_0_HS_0_0_0_map.gif",
           "width": 46,
           "class": "ImageResourceLevel"
          }
         ]
        },
        "yaw": 140.18893617840124,
        "pitch": -3.675392333913126,
        "roll": 0,
        "class": "HotspotPanoramaOverlayMap",
        "hfov": 5.583766973528798
       }
      ],
      "class": "HotspotPanoramaOverlay",
      "enabledInCardboard": true
     }
    ],
    "back": {
     "class": "ImageResource",
     "levels": [
      {
       "height": 1904,
       "url": "media/panorama_B8850684_BEAB_7FB2_41DA_1D5F3F5978FF_b_hq.jpeg",
       "width": 1904,
       "class": "ImageResourceLevel"
      },
      {
       "height": 800,
       "url": "media/panorama_B8850684_BEAB_7FB2_41DA_1D5F3F5978FF_b.jpeg",
       "width": 800,
       "class": "ImageResourceLevel"
      }
     ]
    },
    "bottom": {
     "class": "ImageResource",
     "levels": [
      {
       "height": 1904,
       "url": "media/panorama_B8850684_BEAB_7FB2_41DA_1D5F3F5978FF_d_hq.jpeg",
       "width": 1904,
       "class": "ImageResourceLevel"
      },
      {
       "height": 800,
       "url": "media/panorama_B8850684_BEAB_7FB2_41DA_1D5F3F5978FF_d.jpeg",
       "width": 800,
       "class": "ImageResourceLevel"
      }
     ]
    },
    "thumbnailUrl": "media/panorama_B8850684_BEAB_7FB2_41DA_1D5F3F5978FF_t.jpg",
    "left": {
     "class": "ImageResource",
     "levels": [
      {
       "height": 1904,
       "url": "media/panorama_B8850684_BEAB_7FB2_41DA_1D5F3F5978FF_l_hq.jpeg",
       "width": 1904,
       "class": "ImageResourceLevel"
      },
      {
       "height": 800,
       "url": "media/panorama_B8850684_BEAB_7FB2_41DA_1D5F3F5978FF_l.jpeg",
       "width": 800,
       "class": "ImageResourceLevel"
      }
     ]
    },
    "class": "CubicPanoramaFrame"
   }
  ],
  "hfovMax": 120,
  "cardboardMenu": "this.Menu_CB8FF3CE_C0CD_DF70_41D3_23232EE4C562",
  "class": "Panorama"
 },
 {
  "initialSequence": {
   "movements": [
    {
     "easing": "cubic_in",
     "yawSpeed": 8.95,
     "yawDelta": 18.5,
     "class": "DistancePanoramaCameraMovement"
    },
    {
     "easing": "linear",
     "yawSpeed": 8.95,
     "yawDelta": 323,
     "class": "DistancePanoramaCameraMovement"
    },
    {
     "easing": "cubic_out",
     "yawSpeed": 8.95,
     "yawDelta": 18.5,
     "class": "DistancePanoramaCameraMovement"
    }
   ],
   "restartMovementOnUserInteraction": false,
   "class": "PanoramaCameraSequence"
  },
  "initialPosition": {
   "yaw": 0,
   "pitch": 0,
   "class": "PanoramaCameraPosition"
  },
  "id": "panorama_B8850684_BEAB_7FB2_41DA_1D5F3F5978FF_camera",
  "class": "PanoramaCamera",
  "automaticZoomSpeed": 10
 },
 {
  "thumbnailUrl": "media/panorama_D5B820D0_C07D_A365_41E6_7478E075D015_t.jpg",
  "hfovMin": 60,
  "id": "panorama_D5B820D0_C07D_A365_41E6_7478E075D015",
  "pitch": 0,
  "hfov": 360,
  "vfov": 180,
  "label": "A Block 7",
  "partial": false,
  "frames": [
   {
    "top": {
     "class": "ImageResource",
     "levels": [
      {
       "height": 1904,
       "url": "media/panorama_D5B820D0_C07D_A365_41E6_7478E075D015_u_hq.jpeg",
       "width": 1904,
       "class": "ImageResourceLevel"
      },
      {
       "height": 800,
       "url": "media/panorama_D5B820D0_C07D_A365_41E6_7478E075D015_u.jpeg",
       "width": 800,
       "class": "ImageResourceLevel"
      }
     ]
    },
    "front": {
     "class": "ImageResource",
     "levels": [
      {
       "height": 1904,
       "url": "media/panorama_D5B820D0_C07D_A365_41E6_7478E075D015_f_hq.jpeg",
       "width": 1904,
       "class": "ImageResourceLevel"
      },
      {
       "height": 800,
       "url": "media/panorama_D5B820D0_C07D_A365_41E6_7478E075D015_f.jpeg",
       "width": 800,
       "class": "ImageResourceLevel"
      }
     ]
    },
    "right": {
     "class": "ImageResource",
     "levels": [
      {
       "height": 1904,
       "url": "media/panorama_D5B820D0_C07D_A365_41E6_7478E075D015_r_hq.jpeg",
       "width": 1904,
       "class": "ImageResourceLevel"
      },
      {
       "height": 800,
       "url": "media/panorama_D5B820D0_C07D_A365_41E6_7478E075D015_r.jpeg",
       "width": 800,
       "class": "ImageResourceLevel"
      }
     ]
    },
    "overlays": [
     {
      "useHandCursor": true,
      "rollOverDisplay": false,
      "items": [
       {
        "image": {
         "class": "ImageResource",
         "levels": [
          {
           "height": 71,
           "url": "media/panorama_D5B820D0_C07D_A365_41E6_7478E075D015_0_HS_0_0.png",
           "width": 71,
           "class": "ImageResourceLevel"
          }
         ]
        },
        "yaw": 112.05107607127401,
        "pitch": -10.039348592394743,
        "roll": 0,
        "hfov": 4.209068815743004,
        "class": "HotspotPanoramaOverlayImage"
       }
      ],
      "areas": [
       {
        "toolTip": "Office Area",
        "mapColor": "#FF0000",
        "class": "HotspotPanoramaOverlayArea",
        "click": "this.mainPlayList.set('selectedIndex', 4)"
       }
      ],
      "id": "overlay_D5B820D1_C07D_A364_41B2_BBE4ADD0362B",
      "maps": [
       {
        "image": {
         "class": "ImageResource",
         "levels": [
          {
           "height": 35,
           "url": "media/panorama_D5B820D0_C07D_A365_41E6_7478E075D015_0_HS_0_0_0_map.gif",
           "width": 35,
           "class": "ImageResourceLevel"
          }
         ]
        },
        "yaw": 112.05107607127401,
        "pitch": -10.039348592394743,
        "roll": 0,
        "class": "HotspotPanoramaOverlayMap",
        "hfov": 4.209068815743004
       }
      ],
      "class": "HotspotPanoramaOverlay",
      "enabledInCardboard": true
     }
    ],
    "back": {
     "class": "ImageResource",
     "levels": [
      {
       "height": 1904,
       "url": "media/panorama_D5B820D0_C07D_A365_41E6_7478E075D015_b_hq.jpeg",
       "width": 1904,
       "class": "ImageResourceLevel"
      },
      {
       "height": 800,
       "url": "media/panorama_D5B820D0_C07D_A365_41E6_7478E075D015_b.jpeg",
       "width": 800,
       "class": "ImageResourceLevel"
      }
     ]
    },
    "bottom": {
     "class": "ImageResource",
     "levels": [
      {
       "height": 1904,
       "url": "media/panorama_D5B820D0_C07D_A365_41E6_7478E075D015_d_hq.jpeg",
       "width": 1904,
       "class": "ImageResourceLevel"
      },
      {
       "height": 800,
       "url": "media/panorama_D5B820D0_C07D_A365_41E6_7478E075D015_d.jpeg",
       "width": 800,
       "class": "ImageResourceLevel"
      }
     ]
    },
    "thumbnailUrl": "media/panorama_D5B820D0_C07D_A365_41E6_7478E075D015_t.jpg",
    "left": {
     "class": "ImageResource",
     "levels": [
      {
       "height": 1904,
       "url": "media/panorama_D5B820D0_C07D_A365_41E6_7478E075D015_l_hq.jpeg",
       "width": 1904,
       "class": "ImageResourceLevel"
      },
      {
       "height": 800,
       "url": "media/panorama_D5B820D0_C07D_A365_41E6_7478E075D015_l.jpeg",
       "width": 800,
       "class": "ImageResourceLevel"
      }
     ]
    },
    "class": "CubicPanoramaFrame"
   }
  ],
  "hfovMax": 120,
  "cardboardMenu": "this.Menu_CB8FF3CE_C0CD_DF70_41D3_23232EE4C562",
  "class": "Panorama"
 },
 {
  "initialSequence": {
   "movements": [
    {
     "easing": "cubic_in",
     "yawSpeed": 8.95,
     "yawDelta": 18.5,
     "class": "DistancePanoramaCameraMovement"
    },
    {
     "easing": "linear",
     "yawSpeed": 8.95,
     "yawDelta": 323,
     "class": "DistancePanoramaCameraMovement"
    },
    {
     "easing": "cubic_out",
     "yawSpeed": 8.95,
     "yawDelta": 18.5,
     "class": "DistancePanoramaCameraMovement"
    }
   ],
   "restartMovementOnUserInteraction": false,
   "class": "PanoramaCameraSequence"
  },
  "initialPosition": {
   "yaw": 0,
   "pitch": 0,
   "class": "PanoramaCameraPosition"
  },
  "id": "panorama_D5B820D0_C07D_A365_41E6_7478E075D015_camera",
  "class": "PanoramaCamera",
  "automaticZoomSpeed": 10
 },
 {
  "thumbnailUrl": "media/panorama_B88505D1_BEAB_5D52_41E1_372D9871530A_t.jpg",
  "hfovMin": 60,
  "id": "panorama_B88505D1_BEAB_5D52_41E1_372D9871530A",
  "pitch": 0,
  "hfov": 360,
  "vfov": 180,
  "label": "A Block 6",
  "partial": false,
  "frames": [
   {
    "top": {
     "class": "ImageResource",
     "levels": [
      {
       "height": 1904,
       "url": "media/panorama_B88505D1_BEAB_5D52_41E1_372D9871530A_u_hq.jpeg",
       "width": 1904,
       "class": "ImageResourceLevel"
      },
      {
       "height": 800,
       "url": "media/panorama_B88505D1_BEAB_5D52_41E1_372D9871530A_u.jpeg",
       "width": 800,
       "class": "ImageResourceLevel"
      }
     ]
    },
    "front": {
     "class": "ImageResource",
     "levels": [
      {
       "height": 1904,
       "url": "media/panorama_B88505D1_BEAB_5D52_41E1_372D9871530A_f_hq.jpeg",
       "width": 1904,
       "class": "ImageResourceLevel"
      },
      {
       "height": 800,
       "url": "media/panorama_B88505D1_BEAB_5D52_41E1_372D9871530A_f.jpeg",
       "width": 800,
       "class": "ImageResourceLevel"
      }
     ]
    },
    "right": {
     "class": "ImageResource",
     "levels": [
      {
       "height": 1904,
       "url": "media/panorama_B88505D1_BEAB_5D52_41E1_372D9871530A_r_hq.jpeg",
       "width": 1904,
       "class": "ImageResourceLevel"
      },
      {
       "height": 800,
       "url": "media/panorama_B88505D1_BEAB_5D52_41E1_372D9871530A_r.jpeg",
       "width": 800,
       "class": "ImageResourceLevel"
      }
     ]
    },
    "overlays": [
     {
      "useHandCursor": true,
      "rollOverDisplay": false,
      "items": [
       {
        "image": {
         "class": "ImageResource",
         "levels": [
          {
           "height": 93,
           "url": "media/panorama_B88505D1_BEAB_5D52_41E1_372D9871530A_0_HS_1_0.png",
           "width": 93,
           "class": "ImageResourceLevel"
          }
         ]
        },
        "yaw": -45.01288876527658,
        "pitch": -0.04395333998658507,
        "roll": 0,
        "hfov": 5.595273454646387,
        "class": "HotspotPanoramaOverlayImage"
       }
      ],
      "areas": [
       {
        "toolTip": "Director's Room",
        "mapColor": "#FF0000",
        "class": "HotspotPanoramaOverlayArea",
        "click": "this.mainPlayList.set('selectedIndex', 3)"
       }
      ],
      "id": "overlay_C918D0DB_C01F_E364_41CB_3C7DFA5BA231",
      "maps": [
       {
        "image": {
         "class": "ImageResource",
         "levels": [
          {
           "height": 46,
           "url": "media/panorama_B88505D1_BEAB_5D52_41E1_372D9871530A_0_HS_1_0_0_map.gif",
           "width": 46,
           "class": "ImageResourceLevel"
          }
         ]
        },
        "yaw": -45.01288876527658,
        "pitch": -0.04395333998658507,
        "roll": 0,
        "class": "HotspotPanoramaOverlayMap",
        "hfov": 5.595273454646387
       }
      ],
      "class": "HotspotPanoramaOverlay",
      "enabledInCardboard": true
     },
     {
      "useHandCursor": true,
      "rollOverDisplay": false,
      "items": [
       {
        "image": {
         "class": "ImageResource",
         "levels": [
          {
           "height": 93,
           "url": "media/panorama_B88505D1_BEAB_5D52_41E1_372D9871530A_0_HS_0_0.png",
           "width": 93,
           "class": "ImageResourceLevel"
          }
         ]
        },
        "yaw": -175.23697939633,
        "pitch": -3.026586809089345,
        "roll": 0,
        "hfov": 5.587470486379858,
        "class": "HotspotPanoramaOverlayImage"
       }
      ],
      "areas": [
       {
        "toolTip": "Lounge ",
        "mapColor": "#FF0000",
        "class": "HotspotPanoramaOverlayArea",
        "click": "this.mainPlayList.set('selectedIndex', 7)"
       }
      ],
      "id": "overlay_C96B99D3_C01D_E564_41E2_4D7E3673A2E0",
      "maps": [
       {
        "image": {
         "class": "ImageResource",
         "levels": [
          {
           "height": 46,
           "url": "media/panorama_B88505D1_BEAB_5D52_41E1_372D9871530A_0_HS_0_0_0_map.gif",
           "width": 46,
           "class": "ImageResourceLevel"
          }
         ]
        },
        "yaw": -175.23697939633,
        "pitch": -3.026586809089345,
        "roll": 0,
        "class": "HotspotPanoramaOverlayMap",
        "hfov": 5.587470486379858
       }
      ],
      "class": "HotspotPanoramaOverlay",
      "enabledInCardboard": true
     }
    ],
    "back": {
     "class": "ImageResource",
     "levels": [
      {
       "height": 1904,
       "url": "media/panorama_B88505D1_BEAB_5D52_41E1_372D9871530A_b_hq.jpeg",
       "width": 1904,
       "class": "ImageResourceLevel"
      },
      {
       "height": 800,
       "url": "media/panorama_B88505D1_BEAB_5D52_41E1_372D9871530A_b.jpeg",
       "width": 800,
       "class": "ImageResourceLevel"
      }
     ]
    },
    "bottom": {
     "class": "ImageResource",
     "levels": [
      {
       "height": 1904,
       "url": "media/panorama_B88505D1_BEAB_5D52_41E1_372D9871530A_d_hq.jpeg",
       "width": 1904,
       "class": "ImageResourceLevel"
      },
      {
       "height": 800,
       "url": "media/panorama_B88505D1_BEAB_5D52_41E1_372D9871530A_d.jpeg",
       "width": 800,
       "class": "ImageResourceLevel"
      }
     ]
    },
    "thumbnailUrl": "media/panorama_B88505D1_BEAB_5D52_41E1_372D9871530A_t.jpg",
    "left": {
     "class": "ImageResource",
     "levels": [
      {
       "height": 1904,
       "url": "media/panorama_B88505D1_BEAB_5D52_41E1_372D9871530A_l_hq.jpeg",
       "width": 1904,
       "class": "ImageResourceLevel"
      },
      {
       "height": 800,
       "url": "media/panorama_B88505D1_BEAB_5D52_41E1_372D9871530A_l.jpeg",
       "width": 800,
       "class": "ImageResourceLevel"
      }
     ]
    },
    "class": "CubicPanoramaFrame"
   }
  ],
  "hfovMax": 120,
  "cardboardMenu": "this.Menu_CB8FF3CE_C0CD_DF70_41D3_23232EE4C562",
  "class": "Panorama"
 },
 {
  "initialSequence": {
   "movements": [
    {
     "easing": "cubic_in",
     "yawSpeed": 8.95,
     "yawDelta": 18.5,
     "class": "DistancePanoramaCameraMovement"
    },
    {
     "easing": "linear",
     "yawSpeed": 8.95,
     "yawDelta": 323,
     "class": "DistancePanoramaCameraMovement"
    },
    {
     "easing": "cubic_out",
     "yawSpeed": 8.95,
     "yawDelta": 18.5,
     "class": "DistancePanoramaCameraMovement"
    }
   ],
   "restartMovementOnUserInteraction": false,
   "class": "PanoramaCameraSequence"
  },
  "initialPosition": {
   "yaw": 0,
   "pitch": 0,
   "class": "PanoramaCameraPosition"
  },
  "id": "panorama_B88505D1_BEAB_5D52_41E1_372D9871530A_camera",
  "class": "PanoramaCamera",
  "automaticZoomSpeed": 10
 },
 {
  "thumbnailUrl": "media/panorama_B8852CEA_BEAB_5371_418E_EB0F8FF7E110_t.jpg",
  "hfovMin": 60,
  "id": "panorama_B8852CEA_BEAB_5371_418E_EB0F8FF7E110",
  "pitch": 0,
  "hfov": 360,
  "vfov": 180,
  "label": "A BLock 3",
  "partial": false,
  "frames": [
   {
    "top": {
     "class": "ImageResource",
     "levels": [
      {
       "height": 1296,
       "url": "media/panorama_B8852CEA_BEAB_5371_418E_EB0F8FF7E110_u_hq.jpeg",
       "width": 1296,
       "class": "ImageResourceLevel"
      },
      {
       "height": 800,
       "url": "media/panorama_B8852CEA_BEAB_5371_418E_EB0F8FF7E110_u.jpeg",
       "width": 800,
       "class": "ImageResourceLevel"
      }
     ]
    },
    "front": {
     "class": "ImageResource",
     "levels": [
      {
       "height": 1296,
       "url": "media/panorama_B8852CEA_BEAB_5371_418E_EB0F8FF7E110_f_hq.jpeg",
       "width": 1296,
       "class": "ImageResourceLevel"
      },
      {
       "height": 800,
       "url": "media/panorama_B8852CEA_BEAB_5371_418E_EB0F8FF7E110_f.jpeg",
       "width": 800,
       "class": "ImageResourceLevel"
      }
     ]
    },
    "right": {
     "class": "ImageResource",
     "levels": [
      {
       "height": 1296,
       "url": "media/panorama_B8852CEA_BEAB_5371_418E_EB0F8FF7E110_r_hq.jpeg",
       "width": 1296,
       "class": "ImageResourceLevel"
      },
      {
       "height": 800,
       "url": "media/panorama_B8852CEA_BEAB_5371_418E_EB0F8FF7E110_r.jpeg",
       "width": 800,
       "class": "ImageResourceLevel"
      }
     ]
    },
    "overlays": [
     {
      "useHandCursor": true,
      "rollOverDisplay": false,
      "items": [
       {
        "image": {
         "class": "ImageResource",
         "levels": [
          {
           "height": 99,
           "url": "media/panorama_B8852CEA_BEAB_5371_418E_EB0F8FF7E110_0_HS_0_0.png",
           "width": 99,
           "class": "ImageResourceLevel"
          }
         ]
        },
        "yaw": 1.072425986842111,
        "pitch": -0.8955838815789416,
        "roll": 0,
        "hfov": 8.787988829582554,
        "class": "HotspotPanoramaOverlayImage"
       }
      ],
      "areas": [
       {
        "toolTip": "Lounge",
        "mapColor": "#FF0000",
        "class": "HotspotPanoramaOverlayArea",
        "click": "this.mainPlayList.set('selectedIndex', 7)"
       }
      ],
      "id": "overlay_B7A8A240_BEB7_36B1_41C1_05002809C51A",
      "maps": [
       {
        "image": {
         "class": "ImageResource",
         "levels": [
          {
           "height": 49,
           "url": "media/panorama_B8852CEA_BEAB_5371_418E_EB0F8FF7E110_0_HS_0_0_0_map.gif",
           "width": 49,
           "class": "ImageResourceLevel"
          }
         ]
        },
        "yaw": 1.072425986842111,
        "pitch": -0.8955838815789416,
        "roll": 0,
        "class": "HotspotPanoramaOverlayMap",
        "hfov": 8.787988829582554
       }
      ],
      "class": "HotspotPanoramaOverlay",
      "enabledInCardboard": true
     },
     {
      "useHandCursor": true,
      "rollOverDisplay": false,
      "items": [
       {
        "image": {
         "class": "ImageResource",
         "levels": [
          {
           "height": 99,
           "url": "media/panorama_B8852CEA_BEAB_5371_418E_EB0F8FF7E110_0_HS_1_0.png",
           "width": 99,
           "class": "ImageResourceLevel"
          }
         ]
        },
        "yaw": -91.50441611842106,
        "pitch": 0.013889802631576076,
        "roll": 0,
        "hfov": 8.789062241738636,
        "class": "HotspotPanoramaOverlayImage"
       }
      ],
      "areas": [
       {
        "toolTip": "Reception",
        "mapColor": "#FF0000",
        "class": "HotspotPanoramaOverlayArea",
        "click": "this.mainPlayList.set('selectedIndex', 6)"
       }
      ],
      "id": "overlay_CF758D6A_C00D_DD24_41E3_D43A6C0F5C51",
      "maps": [
       {
        "image": {
         "class": "ImageResource",
         "levels": [
          {
           "height": 49,
           "url": "media/panorama_B8852CEA_BEAB_5371_418E_EB0F8FF7E110_0_HS_1_0_0_map.gif",
           "width": 49,
           "class": "ImageResourceLevel"
          }
         ]
        },
        "yaw": -91.50441611842106,
        "pitch": 0.013889802631576076,
        "roll": 0,
        "class": "HotspotPanoramaOverlayMap",
        "hfov": 8.789062241738636
       }
      ],
      "class": "HotspotPanoramaOverlay",
      "enabledInCardboard": true
     }
    ],
    "back": {
     "class": "ImageResource",
     "levels": [
      {
       "height": 1296,
       "url": "media/panorama_B8852CEA_BEAB_5371_418E_EB0F8FF7E110_b_hq.jpeg",
       "width": 1296,
       "class": "ImageResourceLevel"
      },
      {
       "height": 800,
       "url": "media/panorama_B8852CEA_BEAB_5371_418E_EB0F8FF7E110_b.jpeg",
       "width": 800,
       "class": "ImageResourceLevel"
      }
     ]
    },
    "bottom": {
     "class": "ImageResource",
     "levels": [
      {
       "height": 1296,
       "url": "media/panorama_B8852CEA_BEAB_5371_418E_EB0F8FF7E110_d_hq.jpeg",
       "width": 1296,
       "class": "ImageResourceLevel"
      },
      {
       "height": 800,
       "url": "media/panorama_B8852CEA_BEAB_5371_418E_EB0F8FF7E110_d.jpeg",
       "width": 800,
       "class": "ImageResourceLevel"
      }
     ]
    },
    "thumbnailUrl": "media/panorama_B8852CEA_BEAB_5371_418E_EB0F8FF7E110_t.jpg",
    "left": {
     "class": "ImageResource",
     "levels": [
      {
       "height": 1296,
       "url": "media/panorama_B8852CEA_BEAB_5371_418E_EB0F8FF7E110_l_hq.jpeg",
       "width": 1296,
       "class": "ImageResourceLevel"
      },
      {
       "height": 800,
       "url": "media/panorama_B8852CEA_BEAB_5371_418E_EB0F8FF7E110_l.jpeg",
       "width": 800,
       "class": "ImageResourceLevel"
      }
     ]
    },
    "class": "CubicPanoramaFrame"
   }
  ],
  "hfovMax": 120,
  "cardboardMenu": "this.Menu_CB8FF3CE_C0CD_DF70_41D3_23232EE4C562",
  "class": "Panorama"
 },
 {
  "initialSequence": {
   "movements": [
    {
     "easing": "cubic_in",
     "yawSpeed": 8.95,
     "yawDelta": 18.5,
     "class": "DistancePanoramaCameraMovement"
    },
    {
     "easing": "linear",
     "yawSpeed": 8.95,
     "yawDelta": 323,
     "class": "DistancePanoramaCameraMovement"
    },
    {
     "easing": "cubic_out",
     "yawSpeed": 8.95,
     "yawDelta": 18.5,
     "class": "DistancePanoramaCameraMovement"
    }
   ],
   "restartMovementOnUserInteraction": false,
   "class": "PanoramaCameraSequence"
  },
  "initialPosition": {
   "yaw": 0,
   "pitch": 0,
   "class": "PanoramaCameraPosition"
  },
  "id": "panorama_B8852CEA_BEAB_5371_418E_EB0F8FF7E110_camera",
  "class": "PanoramaCamera",
  "automaticZoomSpeed": 10
 },
 {
  "thumbnailUrl": "media/panorama_B8852156_BEAB_355E_41C6_BC0327E07F2D_t.jpg",
  "hfovMin": 60,
  "id": "panorama_B8852156_BEAB_355E_41C6_BC0327E07F2D",
  "pitch": 0,
  "hfov": 360,
  "vfov": 180,
  "label": "A BLock 2",
  "partial": false,
  "frames": [
   {
    "top": {
     "class": "ImageResource",
     "levels": [
      {
       "height": 1904,
       "url": "media/panorama_B8852156_BEAB_355E_41C6_BC0327E07F2D_u_hq.jpeg",
       "width": 1904,
       "class": "ImageResourceLevel"
      },
      {
       "height": 800,
       "url": "media/panorama_B8852156_BEAB_355E_41C6_BC0327E07F2D_u.jpeg",
       "width": 800,
       "class": "ImageResourceLevel"
      }
     ]
    },
    "front": {
     "class": "ImageResource",
     "levels": [
      {
       "height": 1904,
       "url": "media/panorama_B8852156_BEAB_355E_41C6_BC0327E07F2D_f_hq.jpeg",
       "width": 1904,
       "class": "ImageResourceLevel"
      },
      {
       "height": 800,
       "url": "media/panorama_B8852156_BEAB_355E_41C6_BC0327E07F2D_f.jpeg",
       "width": 800,
       "class": "ImageResourceLevel"
      }
     ]
    },
    "right": {
     "class": "ImageResource",
     "levels": [
      {
       "height": 1904,
       "url": "media/panorama_B8852156_BEAB_355E_41C6_BC0327E07F2D_r_hq.jpeg",
       "width": 1904,
       "class": "ImageResourceLevel"
      },
      {
       "height": 800,
       "url": "media/panorama_B8852156_BEAB_355E_41C6_BC0327E07F2D_r.jpeg",
       "width": 800,
       "class": "ImageResourceLevel"
      }
     ]
    },
    "overlays": [
     {
      "useHandCursor": true,
      "rollOverDisplay": false,
      "items": [
       {
        "image": {
         "class": "ImageResource",
         "levels": [
          {
           "height": 93,
           "url": "media/panorama_B8852156_BEAB_355E_41C6_BC0327E07F2D_0_HS_0_0.png",
           "width": 93,
           "class": "ImageResourceLevel"
          }
         ]
        },
        "yaw": -123.30377628076288,
        "pitch": 0.48114653445311606,
        "roll": 0,
        "hfov": 5.595077814077234,
        "class": "HotspotPanoramaOverlayImage"
       }
      ],
      "areas": [
       {
        "toolTip": "Office Corridor",
        "mapColor": "#FF0000",
        "class": "HotspotPanoramaOverlayArea",
        "click": "this.mainPlayList.set('selectedIndex', 5)"
       }
      ],
      "id": "overlay_B7942D68_BEAB_2D71_41B8_305B108CE860",
      "maps": [
       {
        "image": {
         "class": "ImageResource",
         "levels": [
          {
           "height": 46,
           "url": "media/panorama_B8852156_BEAB_355E_41C6_BC0327E07F2D_0_HS_0_0_0_map.gif",
           "width": 46,
           "class": "ImageResourceLevel"
          }
         ]
        },
        "yaw": -123.30377628076288,
        "pitch": 0.48114653445311606,
        "roll": 0,
        "class": "HotspotPanoramaOverlayMap",
        "hfov": 5.595077814077234
       }
      ],
      "class": "HotspotPanoramaOverlay",
      "enabledInCardboard": true
     },
     {
      "useHandCursor": true,
      "rollOverDisplay": false,
      "items": [
       {
        "image": {
         "class": "ImageResource",
         "levels": [
          {
           "height": 93,
           "url": "media/panorama_B8852156_BEAB_355E_41C6_BC0327E07F2D_0_HS_3_0.png",
           "width": 93,
           "class": "ImageResourceLevel"
          }
         ]
        },
        "yaw": 73.68605860314449,
        "pitch": -5.172374392618153,
        "roll": 0,
        "hfov": 5.572491040438969,
        "class": "HotspotPanoramaOverlayImage"
       }
      ],
      "areas": [
       {
        "toolTip": "Exterior View",
        "mapColor": "#FF0000",
        "class": "HotspotPanoramaOverlayArea",
        "click": "this.mainPlayList.set('selectedIndex', 0)"
       }
      ],
      "id": "overlay_C8E57A6B_C016_E73B_41C0_BC2C9415334A",
      "maps": [
       {
        "image": {
         "class": "ImageResource",
         "levels": [
          {
           "height": 46,
           "url": "media/panorama_B8852156_BEAB_355E_41C6_BC0327E07F2D_0_HS_3_0_0_map.gif",
           "width": 46,
           "class": "ImageResourceLevel"
          }
         ]
        },
        "yaw": 73.68605860314449,
        "pitch": -5.172374392618153,
        "roll": 0,
        "class": "HotspotPanoramaOverlayMap",
        "hfov": 5.572491040438969
       }
      ],
      "class": "HotspotPanoramaOverlay",
      "enabledInCardboard": true
     },
     {
      "useHandCursor": true,
      "rollOverDisplay": false,
      "items": [
       {
        "image": {
         "class": "ImageResource",
         "levels": [
          {
           "height": 93,
           "url": "media/panorama_B8852156_BEAB_355E_41C6_BC0327E07F2D_0_HS_2_0.png",
           "width": 93,
           "class": "ImageResourceLevel"
          }
         ]
        },
        "yaw": -45.732888765276584,
        "pitch": -0.1323743926181554,
        "roll": 0,
        "hfov": 5.59526016778902,
        "class": "HotspotPanoramaOverlayImage"
       }
      ],
      "areas": [
       {
        "toolTip": "Cafe ",
        "mapColor": "#FF0000",
        "class": "HotspotPanoramaOverlayArea",
        "click": "this.mainPlayList.set('selectedIndex', 1)"
       }
      ],
      "id": "overlay_C90465E3_C015_6D2B_41D5_55B1B7C6E84E",
      "maps": [
       {
        "image": {
         "class": "ImageResource",
         "levels": [
          {
           "height": 46,
           "url": "media/panorama_B8852156_BEAB_355E_41C6_BC0327E07F2D_0_HS_2_0_0_map.gif",
           "width": 46,
           "class": "ImageResourceLevel"
          }
         ]
        },
        "yaw": -45.732888765276584,
        "pitch": -0.1323743926181554,
        "roll": 0,
        "class": "HotspotPanoramaOverlayMap",
        "hfov": 5.59526016778902
       }
      ],
      "class": "HotspotPanoramaOverlay",
      "enabledInCardboard": true
     }
    ],
    "back": {
     "class": "ImageResource",
     "levels": [
      {
       "height": 1904,
       "url": "media/panorama_B8852156_BEAB_355E_41C6_BC0327E07F2D_b_hq.jpeg",
       "width": 1904,
       "class": "ImageResourceLevel"
      },
      {
       "height": 800,
       "url": "media/panorama_B8852156_BEAB_355E_41C6_BC0327E07F2D_b.jpeg",
       "width": 800,
       "class": "ImageResourceLevel"
      }
     ]
    },
    "bottom": {
     "class": "ImageResource",
     "levels": [
      {
       "height": 1904,
       "url": "media/panorama_B8852156_BEAB_355E_41C6_BC0327E07F2D_d_hq.jpeg",
       "width": 1904,
       "class": "ImageResourceLevel"
      },
      {
       "height": 800,
       "url": "media/panorama_B8852156_BEAB_355E_41C6_BC0327E07F2D_d.jpeg",
       "width": 800,
       "class": "ImageResourceLevel"
      }
     ]
    },
    "thumbnailUrl": "media/panorama_B8852156_BEAB_355E_41C6_BC0327E07F2D_t.jpg",
    "left": {
     "class": "ImageResource",
     "levels": [
      {
       "height": 1904,
       "url": "media/panorama_B8852156_BEAB_355E_41C6_BC0327E07F2D_l_hq.jpeg",
       "width": 1904,
       "class": "ImageResourceLevel"
      },
      {
       "height": 800,
       "url": "media/panorama_B8852156_BEAB_355E_41C6_BC0327E07F2D_l.jpeg",
       "width": 800,
       "class": "ImageResourceLevel"
      }
     ]
    },
    "class": "CubicPanoramaFrame"
   }
  ],
  "hfovMax": 120,
  "cardboardMenu": "this.Menu_CB8FF3CE_C0CD_DF70_41D3_23232EE4C562",
  "class": "Panorama"
 },
 {
  "initialSequence": {
   "movements": [
    {
     "easing": "cubic_in",
     "yawSpeed": 8.95,
     "yawDelta": 18.5,
     "class": "DistancePanoramaCameraMovement"
    },
    {
     "easing": "linear",
     "yawSpeed": 8.95,
     "yawDelta": 323,
     "class": "DistancePanoramaCameraMovement"
    },
    {
     "easing": "cubic_out",
     "yawSpeed": 8.95,
     "yawDelta": 18.5,
     "class": "DistancePanoramaCameraMovement"
    }
   ],
   "restartMovementOnUserInteraction": false,
   "class": "PanoramaCameraSequence"
  },
  "initialPosition": {
   "yaw": 0,
   "pitch": 0,
   "class": "PanoramaCameraPosition"
  },
  "id": "panorama_B8852156_BEAB_355E_41C6_BC0327E07F2D_camera",
  "class": "PanoramaCamera",
  "automaticZoomSpeed": 10
 },
 {
  "thumbnailUrl": "media/panorama_B8853786_BEAB_3DB1_41E7_8755CFE34834_t.jpg",
  "hfovMin": 60,
  "id": "panorama_B8853786_BEAB_3DB1_41E7_8755CFE34834",
  "pitch": 0,
  "hfov": 360,
  "vfov": 180,
  "label": "A Block 4",
  "partial": false,
  "frames": [
   {
    "top": {
     "class": "ImageResource",
     "levels": [
      {
       "height": 1904,
       "url": "media/panorama_B8853786_BEAB_3DB1_41E7_8755CFE34834_u_hq.jpeg",
       "width": 1904,
       "class": "ImageResourceLevel"
      },
      {
       "height": 800,
       "url": "media/panorama_B8853786_BEAB_3DB1_41E7_8755CFE34834_u.jpeg",
       "width": 800,
       "class": "ImageResourceLevel"
      }
     ]
    },
    "front": {
     "class": "ImageResource",
     "levels": [
      {
       "height": 1904,
       "url": "media/panorama_B8853786_BEAB_3DB1_41E7_8755CFE34834_f_hq.jpeg",
       "width": 1904,
       "class": "ImageResourceLevel"
      },
      {
       "height": 800,
       "url": "media/panorama_B8853786_BEAB_3DB1_41E7_8755CFE34834_f.jpeg",
       "width": 800,
       "class": "ImageResourceLevel"
      }
     ]
    },
    "right": {
     "class": "ImageResource",
     "levels": [
      {
       "height": 1904,
       "url": "media/panorama_B8853786_BEAB_3DB1_41E7_8755CFE34834_r_hq.jpeg",
       "width": 1904,
       "class": "ImageResourceLevel"
      },
      {
       "height": 800,
       "url": "media/panorama_B8853786_BEAB_3DB1_41E7_8755CFE34834_r.jpeg",
       "width": 800,
       "class": "ImageResourceLevel"
      }
     ]
    },
    "overlays": [
     {
      "useHandCursor": true,
      "rollOverDisplay": false,
      "items": [
       {
        "image": {
         "class": "ImageResource",
         "levels": [
          {
           "height": 93,
           "url": "media/panorama_B8853786_BEAB_3DB1_41E7_8755CFE34834_0_HS_1_0.png",
           "width": 93,
           "class": "ImageResourceLevel"
          }
         ]
        },
        "yaw": -44.58341508106604,
        "pitch": -0.688163866302363,
        "roll": 0,
        "hfov": 5.594871525618934,
        "class": "HotspotPanoramaOverlayImage"
       }
      ],
      "areas": [
       {
        "toolTip": "Conference Room",
        "mapColor": "#FF0000",
        "class": "HotspotPanoramaOverlayArea",
        "click": "this.mainPlayList.set('selectedIndex', 2)"
       }
      ],
      "id": "overlay_B794D6B7_BEB5_5FDE_41B6_694221EDD47F",
      "maps": [
       {
        "image": {
         "class": "ImageResource",
         "levels": [
          {
           "height": 46,
           "url": "media/panorama_B8853786_BEAB_3DB1_41E7_8755CFE34834_0_HS_1_0_0_map.gif",
           "width": 46,
           "class": "ImageResourceLevel"
          }
         ]
        },
        "yaw": -44.58341508106604,
        "pitch": -0.688163866302363,
        "roll": 0,
        "class": "HotspotPanoramaOverlayMap",
        "hfov": 5.594871525618934
       }
      ],
      "class": "HotspotPanoramaOverlay",
      "enabledInCardboard": true
     },
     {
      "useHandCursor": true,
      "rollOverDisplay": false,
      "items": [
       {
        "image": {
         "class": "ImageResource",
         "levels": [
          {
           "height": 93,
           "url": "media/panorama_B8853786_BEAB_3DB1_41E7_8755CFE34834_0_HS_0_0.png",
           "width": 93,
           "class": "ImageResourceLevel"
          }
         ]
        },
        "yaw": 64.47763755051288,
        "pitch": -3.972374392618153,
        "roll": 0,
        "hfov": 5.581832832693702,
        "class": "HotspotPanoramaOverlayImage"
       }
      ],
      "areas": [
       {
        "toolTip": "Office Area",
        "mapColor": "#FF0000",
        "class": "HotspotPanoramaOverlayArea",
        "click": "this.mainPlayList.set('selectedIndex', 4)"
       }
      ],
      "id": "overlay_B7C59C98_BEB5_33D1_41CE_67029BD4096B",
      "maps": [
       {
        "image": {
         "class": "ImageResource",
         "levels": [
          {
           "height": 46,
           "url": "media/panorama_B8853786_BEAB_3DB1_41E7_8755CFE34834_0_HS_0_0_0_map.gif",
           "width": 46,
           "class": "ImageResourceLevel"
          }
         ]
        },
        "yaw": 64.47763755051288,
        "pitch": -3.972374392618153,
        "roll": 0,
        "class": "HotspotPanoramaOverlayMap",
        "hfov": 5.581832832693702
       }
      ],
      "class": "HotspotPanoramaOverlay",
      "enabledInCardboard": true
     },
     {
      "useHandCursor": true,
      "rollOverDisplay": false,
      "items": [
       {
        "image": {
         "class": "ImageResource",
         "levels": [
          {
           "height": 93,
           "url": "media/panorama_B8853786_BEAB_3DB1_41E7_8755CFE34834_0_HS_2_0.png",
           "width": 93,
           "class": "ImageResourceLevel"
          }
         ]
        },
        "yaw": -137.0339413968555,
        "pitch": 1.8128887652765786,
        "roll": 0,
        "hfov": 5.592474493406901,
        "class": "HotspotPanoramaOverlayImage"
       }
      ],
      "areas": [
       {
        "toolTip": "Corridor",
        "mapColor": "#FF0000",
        "class": "HotspotPanoramaOverlayArea",
        "click": "this.mainPlayList.set('selectedIndex', 5)"
       }
      ],
      "id": "overlay_CFD389CA_C017_A564_41D7_A5814B620143",
      "maps": [
       {
        "image": {
         "class": "ImageResource",
         "levels": [
          {
           "height": 46,
           "url": "media/panorama_B8853786_BEAB_3DB1_41E7_8755CFE34834_0_HS_2_0_0_map.gif",
           "width": 46,
           "class": "ImageResourceLevel"
          }
         ]
        },
        "yaw": -137.0339413968555,
        "pitch": 1.8128887652765786,
        "roll": 0,
        "class": "HotspotPanoramaOverlayMap",
        "hfov": 5.592474493406901
       }
      ],
      "class": "HotspotPanoramaOverlay",
      "enabledInCardboard": true
     }
    ],
    "back": {
     "class": "ImageResource",
     "levels": [
      {
       "height": 1904,
       "url": "media/panorama_B8853786_BEAB_3DB1_41E7_8755CFE34834_b_hq.jpeg",
       "width": 1904,
       "class": "ImageResourceLevel"
      },
      {
       "height": 800,
       "url": "media/panorama_B8853786_BEAB_3DB1_41E7_8755CFE34834_b.jpeg",
       "width": 800,
       "class": "ImageResourceLevel"
      }
     ]
    },
    "bottom": {
     "class": "ImageResource",
     "levels": [
      {
       "height": 1904,
       "url": "media/panorama_B8853786_BEAB_3DB1_41E7_8755CFE34834_d_hq.jpeg",
       "width": 1904,
       "class": "ImageResourceLevel"
      },
      {
       "height": 800,
       "url": "media/panorama_B8853786_BEAB_3DB1_41E7_8755CFE34834_d.jpeg",
       "width": 800,
       "class": "ImageResourceLevel"
      }
     ]
    },
    "thumbnailUrl": "media/panorama_B8853786_BEAB_3DB1_41E7_8755CFE34834_t.jpg",
    "left": {
     "class": "ImageResource",
     "levels": [
      {
       "height": 1904,
       "url": "media/panorama_B8853786_BEAB_3DB1_41E7_8755CFE34834_l_hq.jpeg",
       "width": 1904,
       "class": "ImageResourceLevel"
      },
      {
       "height": 800,
       "url": "media/panorama_B8853786_BEAB_3DB1_41E7_8755CFE34834_l.jpeg",
       "width": 800,
       "class": "ImageResourceLevel"
      }
     ]
    },
    "class": "CubicPanoramaFrame"
   }
  ],
  "hfovMax": 120,
  "cardboardMenu": "this.Menu_CB8FF3CE_C0CD_DF70_41D3_23232EE4C562",
  "class": "Panorama"
 },
 {
  "initialSequence": {
   "movements": [
    {
     "easing": "cubic_in",
     "yawSpeed": 8.95,
     "yawDelta": 18.5,
     "class": "DistancePanoramaCameraMovement"
    },
    {
     "easing": "linear",
     "yawSpeed": 8.95,
     "yawDelta": 323,
     "class": "DistancePanoramaCameraMovement"
    },
    {
     "easing": "cubic_out",
     "yawSpeed": 8.95,
     "yawDelta": 18.5,
     "class": "DistancePanoramaCameraMovement"
    }
   ],
   "restartMovementOnUserInteraction": false,
   "class": "PanoramaCameraSequence"
  },
  "initialPosition": {
   "yaw": 0,
   "pitch": 0,
   "class": "PanoramaCameraPosition"
  },
  "id": "panorama_B8853786_BEAB_3DB1_41E7_8755CFE34834_camera",
  "class": "PanoramaCamera",
  "automaticZoomSpeed": 10
 },
 {
  "id": "mainPlayList",
  "items": [
   {
    "media": "this.panorama_B868E676_BEAB_5F51_41D7_6B136C89F0F3",
    "begin": "this.setEndToItemIndex(this.mainPlayList, 0, 1)",
    "camera": "this.panorama_B868E676_BEAB_5F51_41D7_6B136C89F0F3_camera",
    "player": "this.MainViewerPanoramaPlayer",
    "class": "PanoramaPlayListItem"
   },
   {
    "media": "this.panorama_B89D82CF_BEAB_774E_41D6_4F5B54AC25AF",
    "begin": "this.setEndToItemIndex(this.mainPlayList, 1, 2)",
    "camera": "this.panorama_B89D82CF_BEAB_774E_41D6_4F5B54AC25AF_camera",
    "player": "this.MainViewerPanoramaPlayer",
    "class": "PanoramaPlayListItem"
   },
   {
    "media": "this.panorama_B8850684_BEAB_7FB2_41DA_1D5F3F5978FF",
    "begin": "this.setEndToItemIndex(this.mainPlayList, 2, 3)",
    "camera": "this.panorama_B8850684_BEAB_7FB2_41DA_1D5F3F5978FF_camera",
    "player": "this.MainViewerPanoramaPlayer",
    "class": "PanoramaPlayListItem"
   },
   {
    "media": "this.panorama_D5B820D0_C07D_A365_41E6_7478E075D015",
    "begin": "this.setEndToItemIndex(this.mainPlayList, 3, 4)",
    "camera": "this.panorama_D5B820D0_C07D_A365_41E6_7478E075D015_camera",
    "player": "this.MainViewerPanoramaPlayer",
    "class": "PanoramaPlayListItem"
   },
   {
    "media": "this.panorama_B88505D1_BEAB_5D52_41E1_372D9871530A",
    "begin": "this.setEndToItemIndex(this.mainPlayList, 4, 5)",
    "camera": "this.panorama_B88505D1_BEAB_5D52_41E1_372D9871530A_camera",
    "player": "this.MainViewerPanoramaPlayer",
    "class": "PanoramaPlayListItem"
   },
   {
    "media": "this.panorama_B8852CEA_BEAB_5371_418E_EB0F8FF7E110",
    "begin": "this.setEndToItemIndex(this.mainPlayList, 5, 6)",
    "camera": "this.panorama_B8852CEA_BEAB_5371_418E_EB0F8FF7E110_camera",
    "player": "this.MainViewerPanoramaPlayer",
    "class": "PanoramaPlayListItem"
   },
   {
    "media": "this.panorama_B8852156_BEAB_355E_41C6_BC0327E07F2D",
    "begin": "this.setEndToItemIndex(this.mainPlayList, 6, 7)",
    "camera": "this.panorama_B8852156_BEAB_355E_41C6_BC0327E07F2D_camera",
    "player": "this.MainViewerPanoramaPlayer",
    "class": "PanoramaPlayListItem"
   },
   {
    "media": "this.panorama_B8853786_BEAB_3DB1_41E7_8755CFE34834",
    "begin": "this.setEndToItemIndex(this.mainPlayList, 7, 0)",
    "camera": "this.panorama_B8853786_BEAB_3DB1_41E7_8755CFE34834_camera",
    "player": "this.MainViewerPanoramaPlayer",
    "class": "PanoramaPlayListItem"
   }
  ],
  "class": "PlayList"
 },
 "this.Menu_CB8FF3CE_C0CD_DF70_41D3_23232EE4C562"
], "children": [
 {
  "playbackBarBottom": 5,
  "paddingRight": 0,
  "progressBorderColor": "#000000",
  "progressBarBackgroundColor": [
   "#3399FF"
  ],
  "toolTipFontFamily": "Arial",
  "progressBackgroundColor": [
   "#FFFFFF"
  ],
  "progressBackgroundOpacity": 1,
  "paddingTop": 0,
  "playbackBarProgressBackgroundColorDirection": "vertical",
  "playbackBarOpacity": 1,
  "toolTipBorderSize": 1,
  "toolTipPaddingTop": 4,
  "playbackBarHeadBackgroundColor": [
   "#111111",
   "#666666"
  ],
  "playbackBarHeadShadowHorizontalLength": 0,
  "playbackBarBackgroundColor": [
   "#FFFFFF"
  ],
  "toolTipPaddingRight": 6,
  "playbackBarHeight": 10,
  "toolTipShadowColor": "#333333",
  "toolTipFontColor": "#606060",
  "borderRadius": 0,
  "toolTipBackgroundColor": "#F6F6F6",
  "progressBarOpacity": 1,
  "toolTipBorderRadius": 3,
  "playbackBarHeadWidth": 6,
  "progressBottom": 0,
  "playbackBarProgressBorderSize": 0,
  "progressHeight": 10,
  "toolTipOpacity": 1,
  "playbackBarBackgroundColorDirection": "vertical",
  "playbackBarRight": 0,
  "toolTipPaddingLeft": 6,
  "progressBorderSize": 0,
  "progressBarBorderRadius": 0,
  "toolTipShadowOpacity": 1,
  "toolTipTextShadowOpacity": 0,
  "playbackBarHeadBackgroundColorDirection": "vertical",
  "toolTipFontSize": 12,
  "playbackBarProgressBackgroundColor": [
   "#3399FF"
  ],
  "progressBarBorderSize": 0,
  "toolTipShadowHorizontalLength": 0,
  "borderSize": 0,
  "transitionMode": "blending",
  "toolTipTextShadowColor": "#000000",
  "toolTipShadowSpread": 0,
  "toolTipShadowBlurRadius": 3,
  "playbackBarProgressBorderRadius": 0,
  "playbackBarHeadShadowOpacity": 0.7,
  "playbackBarProgressBackgroundColorRatios": [
   0
  ],
  "progressBorderRadius": 0,
  "playbackBarBorderColor": "#FFFFFF",
  "playbackBarBorderRadius": 0,
  "shadow": false,
  "toolTipBorderColor": "#767676",
  "width": "100%",
  "playbackBarProgressBorderColor": "#000000",
  "id": "MainViewer",
  "toolTipTextShadowBlurRadius": 3,
  "height": "100%",
  "toolTipFontWeight": "normal",
  "playbackBarHeadBorderRadius": 0,
  "left": 0,
  "class": "ViewerArea",
  "toolTipShadowVerticalLength": 0,
  "progressLeft": 0,
  "playbackBarProgressOpacity": 1,
  "playbackBarHeadBorderColor": "#000000",
  "toolTipFontStyle": "normal",
  "toolTipPaddingBottom": 4,
  "playbackBarHeadShadowVerticalLength": 0,
  "playbackBarBorderSize": 0,
  "playbackBarBackgroundOpacity": 1,
  "minWidth": 100,
  "top": 0,
  "minHeight": 50,
  "playbackBarHeadBorderSize": 0,
  "playbackBarLeft": 0,
  "playbackBarHeadShadowBlurRadius": 3,
  "playbackBarHeadHeight": 15,
  "playbackBarHeadShadowColor": "#000000",
  "playbackBarHeadBackgroundColorRatios": [
   0,
   1
  ],
  "paddingBottom": 0,
  "progressBackgroundColorRatios": [
   0
  ],
  "progressRight": 0,
  "progressOpacity": 1,
  "progressBarBorderColor": "#000000",
  "progressBarBackgroundColorRatios": [
   0
  ],
  "progressBarBackgroundColorDirection": "vertical",
  "playbackBarHeadOpacity": 1,
  "paddingLeft": 0,
  "playbackBarHeadShadow": true,
  "progressBackgroundColorDirection": "vertical"
 },
 {
  "width": 240,
  "height": 125,
  "id": "Container_CC02269E_C01D_AF1D_41B5_49209598034A",
  "layout": "horizontal",
  "scrollBarOpacity": 0.5,
  "paddingTop": 0,
  "left": "1%",
  "class": "Container",
  "borderRadius": 0,
  "contentOpaque": false,
  "horizontalAlign": "left",
  "children": [
   {
    "width": "52.083%",
    "height": "80%",
    "id": "Image_C9D35E44_C033_7F6D_41D7_4D6BF88F233F",
    "paddingTop": 0,
    "maxHeight": 100,
    "class": "Image",
    "click": "var newWindow = window.open(\"http://dubaisouth.ae/\", \"_blank\"); newWindow.focus()",
    "cursor": "hand",
    "horizontalAlign": "center",
    "borderRadius": 0,
    "minWidth": 1,
    "verticalAlign": "middle",
    "minHeight": 1,
    "paddingBottom": 0,
    "scaleMode": "fit_inside",
    "url": "skin/Image_C9D35E44_C033_7F6D_41D7_4D6BF88F233F.png",
    "borderSize": 0,
    "maxWidth": 125,
    "paddingLeft": 0,
    "backgroundOpacity": 0,
    "paddingRight": 0,
    "shadow": false
   }
  ],
  "minWidth": 1,
  "verticalAlign": "top",
  "top": "0%",
  "minHeight": 1,
  "paddingBottom": 0,
  "scrollBarVisible": "rollOver",
  "scrollBarWidth": 10,
  "borderSize": 0,
  "scrollBarMargin": 2,
  "scrollBarColor": "#000000",
  "gap": 10,
  "paddingLeft": 0,
  "backgroundOpacity": 0,
  "overflow": "hidden",
  "paddingRight": 0,
  "shadow": false
 },
 {
  "width": 305,
  "height": 360,
  "id": "Container_CD727A4B_C00E_E764_41E3_793159D2ED59",
  "layout": "horizontal",
  "scrollBarOpacity": 0.5,
  "paddingTop": 0,
  "left": "0%",
  "class": "Container",
  "borderRadius": 0,
  "contentOpaque": false,
  "horizontalAlign": "left",
  "children": [
   {
    "width": 160,
    "scrollBarOpacity": 0.5,
    "id": "Container_CCF17B1C_C013_651D_41AD_2148596AA203",
    "layout": "vertical",
    "height": "91.111%",
    "paddingTop": 0,
    "class": "Container",
    "borderRadius": 0,
    "contentOpaque": false,
    "horizontalAlign": "center",
    "children": [
     {
      "width": 150,
      "height": 20,
      "id": "Container_CC00937D_C012_E51C_41C9_0219FC24180D",
      "layout": "horizontal",
      "backgroundColor": [
       "#FFFFFF",
       "#FFFFFF"
      ],
      "paddingTop": 0,
      "scrollBarOpacity": 0.5,
      "class": "Container",
      "backgroundColorDirection": "vertical",
      "borderRadius": 0,
      "contentOpaque": false,
      "horizontalAlign": "center",
      "children": [
       {
        "width": 150,
        "height": 20,
        "id": "Label_CAB96F7C_C037_5D1C_41E1_4DCCACFFCE06",
        "paddingTop": 0,
        "class": "Label",
        "click": "this.mainPlayList.set('selectedIndex', 6)",
        "fontSize": 12,
        "borderRadius": 0,
        "horizontalAlign": "center",
        "fontStyle": "normal",
        "minWidth": 150,
        "textDecoration": "none",
        "verticalAlign": "middle",
        "fontFamily": "Tahoma",
        "minHeight": 20,
        "paddingBottom": 0,
        "fontColor": "#000000",
        "borderSize": 0,
        "text": "RECEPTION",
        "fontWeight": "normal",
        "paddingLeft": 0,
        "backgroundOpacity": 0,
        "paddingRight": 0,
        "shadow": false
       }
      ],
      "backgroundColorRatios": [
       0,
       1
      ],
      "minWidth": 150,
      "minHeight": 20,
      "paddingBottom": 0,
      "scrollBarVisible": "rollOver",
      "scrollBarWidth": 10,
      "borderSize": 0,
      "scrollBarMargin": 2,
      "scrollBarColor": "#000000",
      "gap": 10,
      "paddingLeft": 0,
      "verticalAlign": "middle",
      "backgroundOpacity": 0.3,
      "overflow": "hidden",
      "paddingRight": 0,
      "shadow": false
     },
     {
      "width": 150,
      "height": 20,
      "id": "Container_CB684FE7_C015_BD2B_41D7_EDF6007ABBA2",
      "layout": "horizontal",
      "backgroundColor": [
       "#FFFFFF",
       "#FFFFFF"
      ],
      "paddingTop": 0,
      "scrollBarOpacity": 0.5,
      "class": "Container",
      "backgroundColorDirection": "vertical",
      "borderRadius": 0,
      "contentOpaque": false,
      "horizontalAlign": "center",
      "children": [
       {
        "width": 150,
        "height": 20,
        "id": "Label_CA8A364B_C03E_EF7B_41E4_9389BF1DA346",
        "paddingTop": 0,
        "class": "Label",
        "click": "this.mainPlayList.set('selectedIndex', 5)",
        "fontSize": 12,
        "borderRadius": 0,
        "horizontalAlign": "center",
        "fontStyle": "normal",
        "minWidth": 150,
        "textDecoration": "none",
        "verticalAlign": "middle",
        "fontFamily": "Tahoma",
        "minHeight": 20,
        "paddingBottom": 0,
        "fontColor": "#000000",
        "borderSize": 0,
        "text": "CORRIDOR",
        "fontWeight": "normal",
        "paddingLeft": 0,
        "backgroundOpacity": 0,
        "paddingRight": 0,
        "shadow": false
       }
      ],
      "backgroundColorRatios": [
       0,
       1
      ],
      "minWidth": 150,
      "minHeight": 20,
      "paddingBottom": 0,
      "scrollBarVisible": "rollOver",
      "scrollBarWidth": 10,
      "borderSize": 0,
      "scrollBarMargin": 2,
      "scrollBarColor": "#000000",
      "gap": 10,
      "paddingLeft": 0,
      "verticalAlign": "middle",
      "backgroundOpacity": 0.3,
      "overflow": "hidden",
      "paddingRight": 0,
      "shadow": false
     },
     {
      "width": 150,
      "height": 20,
      "id": "Container_CBA6703C_C017_631D_41C5_393DE2517233",
      "layout": "horizontal",
      "backgroundColor": [
       "#FFFFFF",
       "#FFFFFF"
      ],
      "paddingTop": 0,
      "scrollBarOpacity": 0.5,
      "class": "Container",
      "backgroundColorDirection": "vertical",
      "borderRadius": 0,
      "contentOpaque": false,
      "horizontalAlign": "center",
      "children": [
       {
        "width": "41.667%",
        "height": "100%",
        "id": "Label_CA6E7AED_C03F_E73F_41A4_861CF7E0512A",
        "paddingTop": 0,
        "class": "Label",
        "click": "this.mainPlayList.set('selectedIndex', 7)",
        "fontSize": 12,
        "borderRadius": 0,
        "horizontalAlign": "center",
        "fontStyle": "normal",
        "minWidth": 150,
        "textDecoration": "none",
        "verticalAlign": "middle",
        "minHeight": 20,
        "paddingBottom": 0,
        "fontFamily": "Tahoma",
        "fontColor": "#000000",
        "borderSize": 0,
        "text": "LOUNGE",
        "fontWeight": "normal",
        "paddingLeft": 0,
        "backgroundOpacity": 0,
        "paddingRight": 0,
        "shadow": false
       }
      ],
      "backgroundColorRatios": [
       0,
       1
      ],
      "minWidth": 150,
      "minHeight": 20,
      "paddingBottom": 0,
      "scrollBarVisible": "rollOver",
      "scrollBarWidth": 10,
      "borderSize": 0,
      "scrollBarMargin": 2,
      "scrollBarColor": "#000000",
      "gap": 10,
      "paddingLeft": 0,
      "verticalAlign": "middle",
      "backgroundOpacity": 0.3,
      "overflow": "hidden",
      "paddingRight": 0,
      "shadow": false
     },
     {
      "width": 150,
      "height": 20,
      "id": "Container_CB94E93E_C015_E51C_41BE_67AA116C5835",
      "layout": "horizontal",
      "backgroundColor": [
       "#FFFFFF",
       "#FFFFFF"
      ],
      "paddingTop": 0,
      "scrollBarOpacity": 0.5,
      "class": "Container",
      "backgroundColorDirection": "vertical",
      "borderRadius": 0,
      "contentOpaque": false,
      "horizontalAlign": "center",
      "children": [
       {
        "width": 150,
        "height": 20,
        "id": "Label_CA95215D_C03D_E51F_41E6_E4D8F109781B",
        "paddingTop": 0,
        "class": "Label",
        "click": "this.mainPlayList.set('selectedIndex', 2)",
        "fontSize": 12,
        "borderRadius": 0,
        "horizontalAlign": "center",
        "fontStyle": "normal",
        "minWidth": 150,
        "textDecoration": "none",
        "verticalAlign": "middle",
        "fontFamily": "Tahoma",
        "minHeight": 20,
        "paddingBottom": 0,
        "fontColor": "#000000",
        "borderSize": 0,
        "text": "CONFERENCE ROOM",
        "fontWeight": "normal",
        "paddingLeft": 0,
        "backgroundOpacity": 0,
        "paddingRight": 0,
        "shadow": false
       }
      ],
      "backgroundColorRatios": [
       0,
       1
      ],
      "minWidth": 150,
      "minHeight": 20,
      "paddingBottom": 0,
      "scrollBarVisible": "rollOver",
      "scrollBarWidth": 10,
      "borderSize": 0,
      "scrollBarMargin": 2,
      "scrollBarColor": "#000000",
      "gap": 10,
      "paddingLeft": 0,
      "verticalAlign": "middle",
      "backgroundOpacity": 0.3,
      "overflow": "hidden",
      "paddingRight": 0,
      "shadow": false
     },
     {
      "width": 150,
      "height": 20,
      "id": "Container_CB2D7B86_C013_65ED_41A2_C18858FC1E57",
      "layout": "horizontal",
      "backgroundColor": [
       "#FFFFFF",
       "#FFFFFF"
      ],
      "paddingTop": 0,
      "scrollBarOpacity": 0.5,
      "class": "Container",
      "backgroundColorDirection": "vertical",
      "borderRadius": 0,
      "contentOpaque": false,
      "horizontalAlign": "center",
      "children": [
       {
        "width": 150,
        "height": 20,
        "id": "Label_CA623ECC_C032_BF7D_41C6_7CCE4B2CB692",
        "paddingTop": 0,
        "class": "Label",
        "click": "this.mainPlayList.set('selectedIndex', 4)",
        "fontSize": 12,
        "borderRadius": 0,
        "horizontalAlign": "center",
        "fontStyle": "normal",
        "minWidth": 150,
        "textDecoration": "none",
        "verticalAlign": "middle",
        "fontFamily": "Tahoma",
        "minHeight": 20,
        "paddingBottom": 0,
        "fontColor": "#000000",
        "borderSize": 0,
        "text": "OFFICE AREA ",
        "fontWeight": "normal",
        "paddingLeft": 0,
        "backgroundOpacity": 0,
        "paddingRight": 0,
        "shadow": false
       }
      ],
      "backgroundColorRatios": [
       0,
       1
      ],
      "minWidth": 150,
      "minHeight": 20,
      "paddingBottom": 0,
      "scrollBarVisible": "rollOver",
      "scrollBarWidth": 10,
      "borderSize": 0,
      "scrollBarMargin": 2,
      "scrollBarColor": "#000000",
      "gap": 10,
      "paddingLeft": 0,
      "verticalAlign": "middle",
      "backgroundOpacity": 0.3,
      "overflow": "hidden",
      "paddingRight": 0,
      "shadow": false
     },
     {
      "width": 150,
      "height": 20,
      "id": "Container_CB673964_C012_A52D_41B1_5125EF1503E8",
      "layout": "horizontal",
      "backgroundColor": [
       "#FFFFFF",
       "#FFFFFF"
      ],
      "paddingTop": 0,
      "scrollBarOpacity": 0.5,
      "class": "Container",
      "backgroundColorDirection": "vertical",
      "borderRadius": 0,
      "contentOpaque": false,
      "horizontalAlign": "center",
      "children": [
       {
        "width": 150,
        "height": 20,
        "id": "Label_CA55DD93_C032_FDE4_41C2_DFEADEAF818F",
        "paddingTop": 0,
        "class": "Label",
        "click": "this.mainPlayList.set('selectedIndex', 3)",
        "fontSize": 12,
        "borderRadius": 0,
        "horizontalAlign": "center",
        "fontStyle": "normal",
        "minWidth": 150,
        "textDecoration": "none",
        "verticalAlign": "middle",
        "fontFamily": "Tahoma",
        "minHeight": 20,
        "paddingBottom": 0,
        "fontColor": "#000000",
        "borderSize": 0,
        "text": "DIRECTOR 'S OFFICE",
        "fontWeight": "normal",
        "paddingLeft": 0,
        "backgroundOpacity": 0,
        "paddingRight": 0,
        "shadow": false
       }
      ],
      "backgroundColorRatios": [
       0,
       1
      ],
      "minWidth": 150,
      "minHeight": 20,
      "paddingBottom": 0,
      "scrollBarVisible": "rollOver",
      "scrollBarWidth": 10,
      "borderSize": 0,
      "scrollBarMargin": 2,
      "scrollBarColor": "#000000",
      "gap": 10,
      "paddingLeft": 0,
      "verticalAlign": "middle",
      "backgroundOpacity": 0.3,
      "overflow": "hidden",
      "paddingRight": 0,
      "shadow": false
     },
     {
      "width": 150,
      "height": 20,
      "id": "Container_CB125A25_C00F_672F_41E4_B6D1394370CA",
      "layout": "horizontal",
      "backgroundColor": [
       "#FFFFFF",
       "#FFFFFF"
      ],
      "paddingTop": 0,
      "scrollBarOpacity": 0.5,
      "class": "Container",
      "backgroundColorDirection": "vertical",
      "borderRadius": 0,
      "contentOpaque": false,
      "horizontalAlign": "center",
      "children": [
       {
        "width": "41.667%",
        "height": 20,
        "id": "Label_CABF20D0_C036_A365_41DF_941616A029F3",
        "paddingTop": 0,
        "class": "Label",
        "click": "this.mainPlayList.set('selectedIndex', 1)",
        "fontSize": 12,
        "borderRadius": 0,
        "horizontalAlign": "center",
        "fontStyle": "normal",
        "minWidth": 150,
        "textDecoration": "none",
        "verticalAlign": "middle",
        "minHeight": 20,
        "paddingBottom": 0,
        "fontFamily": "Tahoma",
        "fontColor": "#000000",
        "borderSize": 0,
        "text": "CAFE ",
        "fontWeight": "normal",
        "paddingLeft": 0,
        "backgroundOpacity": 0,
        "paddingRight": 0,
        "shadow": false
       }
      ],
      "backgroundColorRatios": [
       0,
       1
      ],
      "minWidth": 150,
      "minHeight": 20,
      "paddingBottom": 0,
      "scrollBarVisible": "rollOver",
      "scrollBarWidth": 10,
      "borderSize": 0,
      "scrollBarMargin": 2,
      "scrollBarColor": "#000000",
      "gap": 10,
      "paddingLeft": 0,
      "verticalAlign": "middle",
      "backgroundOpacity": 0.3,
      "overflow": "hidden",
      "paddingRight": 0,
      "shadow": false
     },
     {
      "width": 150,
      "height": 20,
      "id": "Container_CAC85F45_C00D_DD6C_41E3_6945F08CB6DA",
      "layout": "horizontal",
      "backgroundColor": [
       "#FFFFFF",
       "#FFFFFF"
      ],
      "paddingTop": 0,
      "scrollBarOpacity": 0.5,
      "class": "Container",
      "backgroundColorDirection": "vertical",
      "borderRadius": 0,
      "contentOpaque": false,
      "horizontalAlign": "center",
      "children": [
       {
        "width": 150,
        "height": 20,
        "id": "Label_CA5E887C_C037_A31C_41B1_00D8A0511430",
        "paddingTop": 0,
        "class": "Label",
        "click": "this.mainPlayList.set('selectedIndex', 0)",
        "fontSize": 12,
        "borderRadius": 0,
        "horizontalAlign": "center",
        "fontStyle": "normal",
        "minWidth": 150,
        "textDecoration": "none",
        "verticalAlign": "middle",
        "fontFamily": "Tahoma",
        "minHeight": 20,
        "paddingBottom": 0,
        "fontColor": "#000000",
        "borderSize": 0,
        "text": " EXTERIOR VIEW",
        "fontWeight": "normal",
        "paddingLeft": 0,
        "backgroundOpacity": 0,
        "paddingRight": 0,
        "shadow": false
       }
      ],
      "backgroundColorRatios": [
       0,
       1
      ],
      "minWidth": 240,
      "minHeight": 20,
      "paddingBottom": 0,
      "scrollBarVisible": "rollOver",
      "scrollBarWidth": 10,
      "borderSize": 0,
      "scrollBarMargin": 2,
      "scrollBarColor": "#000000",
      "gap": 10,
      "paddingLeft": 0,
      "verticalAlign": "middle",
      "backgroundOpacity": 0.3,
      "overflow": "hidden",
      "paddingRight": 0,
      "shadow": false
     }
    ],
    "minWidth": 160,
    "verticalAlign": "middle",
    "minHeight": 300,
    "paddingBottom": 0,
    "scrollBarVisible": "rollOver",
    "scrollBarWidth": 10,
    "borderSize": 0,
    "scrollBarMargin": 2,
    "scrollBarColor": "#000000",
    "gap": 20,
    "paddingLeft": 0,
    "backgroundOpacity": 0,
    "overflow": "hidden",
    "paddingRight": 0,
    "shadow": false
   }
  ],
  "minWidth": 305,
  "verticalAlign": "top",
  "top": "11.06%",
  "minHeight": 350,
  "paddingBottom": 0,
  "scrollBarVisible": "rollOver",
  "scrollBarWidth": 10,
  "borderSize": 0,
  "scrollBarMargin": 2,
  "scrollBarColor": "#000000",
  "gap": 10,
  "paddingLeft": 0,
  "backgroundOpacity": 0,
  "overflow": "hidden",
  "paddingRight": 0,
  "shadow": false
 },
 {
  "width": "100%",
  "scrollBarOpacity": 0.5,
  "id": "Container_CDE2E194_C073_A5EC_41D4_85157D696EBB",
  "layout": "vertical",
  "height": "42.518%",
  "paddingTop": 0,
  "left": "0%",
  "class": "Container",
  "borderRadius": 0,
  "contentOpaque": false,
  "horizontalAlign": "center",
  "children": [
   {
    "height": 137,
    "id": "Container_C9A4076F_C016_AD3C_41C9_AAA13B60E1CA",
    "layout": "horizontal",
    "scrollBarOpacity": 0.5,
    "paddingTop": 0,
    "class": "Container",
    "borderRadius": 0,
    "contentOpaque": false,
    "horizontalAlign": "center",
    "children": [
     "this.IconButton_C9A4076E_C016_AD3D_41E4_2C26E9C95BF3",
     "this.IconButton_C9A4076F_C016_AD3C_41E4_D71E62A0309A",
     "this.IconButton_C9A4076F_C016_AD3C_41E1_98C3CF14C671",
     "this.IconButton_C9A4076F_C016_AD3C_41B3_7354899C7681",
     {
      "width": 40,
      "scrollBarOpacity": 0.5,
      "id": "Container_C9A4076F_C016_AD3C_41E0_FA7796716A2B",
      "layout": "vertical",
      "height": "100%",
      "paddingTop": 0,
      "class": "Container",
      "borderRadius": 0,
      "contentOpaque": false,
      "horizontalAlign": "center",
      "children": [
       "this.IconButton_C9A4076F_C016_AD3C_41D5_1891DE7BEAB9",
       "this.IconButton_C9A4076F_C016_AD3C_41E4_270B93306BD7",
       "this.IconButton_C9A4076F_C016_AD3C_41B6_2F9CA2436D0D"
      ],
      "minWidth": 20,
      "verticalAlign": "middle",
      "minHeight": 20,
      "paddingBottom": 0,
      "scrollBarVisible": "rollOver",
      "scrollBarWidth": 10,
      "borderSize": 0,
      "scrollBarMargin": 2,
      "scrollBarColor": "#000000",
      "gap": 4,
      "paddingLeft": 0,
      "backgroundOpacity": 0,
      "overflow": "hidden",
      "paddingRight": 0,
      "shadow": false
     },
     "this.IconButton_C9A4076F_C016_AD3C_41AE_DED47DEB69C8",
     "this.IconButton_C9A4076F_C016_AD3C_41D0_F7C35F666D44",
     {
      "width": 40,
      "height": 40,
      "id": "IconButton_C9A4076F_C016_AD3C_41B2_A7EDCE913DD0",
      "mode": "toggle",
      "paddingTop": 0,
      "transparencyActive": false,
      "class": "IconButton",
      "pressedIconURL": "skin/IconButton_C9A4076F_C016_AD3C_41B2_A7EDCE913DD0_pressed.png",
      "cursor": "hand",
      "horizontalAlign": "center",
      "borderRadius": 0,
      "minWidth": 0,
      "verticalAlign": "middle",
      "minHeight": 0,
      "paddingBottom": 0,
      "borderSize": 0,
      "iconURL": "skin/IconButton_C9A4076F_C016_AD3C_41B2_A7EDCE913DD0.png",
      "paddingLeft": 0,
      "backgroundOpacity": 0,
      "paddingRight": 0,
      "shadow": false
     },
     "this.IconButton_C9A4076F_C016_AD3C_41D2_872690316C57"
    ],
    "minWidth": 20,
    "verticalAlign": "middle",
    "minHeight": 20,
    "paddingBottom": 0,
    "scrollBarVisible": "rollOver",
    "scrollBarWidth": 10,
    "borderSize": 0,
    "scrollBarMargin": 2,
    "scrollBarColor": "#000000",
    "gap": 4,
    "paddingLeft": 0,
    "backgroundOpacity": 0,
    "overflow": "hidden",
    "paddingRight": 0,
    "shadow": false
   }
  ],
  "minWidth": 1,
  "minHeight": 1,
  "bottom": "1.5%",
  "scrollBarVisible": "rollOver",
  "scrollBarWidth": 10,
  "borderSize": 0,
  "scrollBarMargin": 2,
  "paddingBottom": 0,
  "scrollBarColor": "#000000",
  "gap": 10,
  "paddingLeft": 0,
  "verticalAlign": "bottom",
  "backgroundOpacity": 0,
  "overflow": "hidden",
  "paddingRight": 0,
  "shadow": false
 }
], 
 "width": "100%",
 "mouseWheelEnabled": true,
 "layout": "absolute",
 "scrollBarOpacity": 0.5,
 "paddingTop": 0,
 "height": "100%",
 "class": "Player",
 "buttonToggleMute": "this.IconButton_C9A4076F_C016_AD3C_41B2_A7EDCE913DD0",
 "borderRadius": 0,
 "contentOpaque": false,
 "horizontalAlign": "left",
 "minWidth": 20,
 "minHeight": 20,
 "start": "this.mainPlayList.set('selectedIndex', 0)",
 "paddingBottom": 0,
 "scrollBarVisible": "rollOver",
 "scrollBarWidth": 10,
 "borderSize": 0,
 "scrollBarColor": "#000000",
 "gap": 10,
 "scripts": {
  "pauseGlobalAudiosWhilePlayItem": function(playList, index, caller){    var audios = window.currentGlobalAudios;   if(!audios) return;   var resumeFunction = this.resumeGlobalAudios;   var endFunction = function(){       resumeFunction(caller);   };   this.pauseGlobalAudios(caller);   this.executeFunctionWhenChange(playList, index, endFunction, endFunction); },
  "setMediaBehaviour": function(playList, index, mediaDispatcher){    var self = this;   var stateChangeFunction = function(event){       if(event.data.state == 'stopped'){           dispose();       }   };   var changeFunction = function(){       var index = playListDispatcher.get('selectedIndex');       if(index != -1){           indexDispatcher = index;           dispose();       }   };   var dispose = function(){       if(!playListDispatcher) return;       playList.set('selectedIndex', -1);       if(panoramaSequence && panoramaSequenceIndex != -1){           if(panoramaSequence) {               if(panoramaSequenceIndex > 0 && panoramaSequence.get('movements')[panoramaSequenceIndex-1].get('class') == 'TargetPanoramaCameraMovement'){                   var initialPosition = camera.get('initialPosition');                   var oldYaw = initialPosition.get('yaw');                   var oldPitch = initialPosition.get('pitch');                   var oldHfov = initialPosition.get('hfov');                   var previousMovement = panoramaSequence.get('movements')[panoramaSequenceIndex-1];                   initialPosition.set('yaw', previousMovement.get('targetYaw'));                   initialPosition.set('pitch', previousMovement.get('targetPitch'));                   initialPosition.set('hfov', previousMovement.get('targetHfov'));                   var restoreInitialPositionFunction = function(event){                       initialPosition.set('yaw', oldYaw);                       initialPosition.set('pitch', oldPitch);                       initialPosition.set('hfov', oldHfov);                       itemDispatcher.unbind('end', restoreInitialPositionFunction, self);                   };                   itemDispatcher.bind('end', restoreInitialPositionFunction, self);               }               panoramaSequence.set('movementIndex', panoramaSequenceIndex);           }       }       playListDispatcher.set('selectedIndex', indexDispatcher);       if(player){           player.unbind('stateChange', stateChangeFunction, self);       }       if(sameViewerArea){           if(playList != playListDispatcher)               playListDispatcher.unbind('change', changeFunction, self);       }       else{           viewerArea.set('visible', false);       }       playListDispatcher = undefined;   };   if(!mediaDispatcher){       var currentIndex = playList.get('selectedIndex');       var currentPlayer = (currentIndex != -1) ? playList.get('items')[playList.get('selectedIndex')].get('player') : this.getActivePlayerWithViewer(this.MainViewer);       if(currentPlayer) {           if(currentPlayer.get('panorama')) mediaDispatcher = currentPlayer.get('panorama');           else if(currentPlayer.get('video')) mediaDispatcher = currentPlayer.get('video');           else if(currentPlayer.get('photoAlbum')) mediaDispatcher = currentPlayer.get('photoAlbum');           else if(currentPlayer.get('map')) mediaDispatcher = currentPlayer.get('map');       }   }   var playListDispatcher = mediaDispatcher ? this.getPlayListWithMedia(mediaDispatcher, true) : undefined;   if(!playListDispatcher){       playList.set('selectedIndex', index);       return;   }   var indexDispatcher = playListDispatcher.get('selectedIndex');   if(playList.get('selectedIndex') == index || indexDispatcher == -1){       return;   }   var item = playList.get('items')[index];   var itemDispatcher = playListDispatcher.get('items')[indexDispatcher];   var viewerArea = item.get('player').get('viewerArea');   var sameViewerArea = viewerArea == itemDispatcher.get('player').get('viewerArea');   if(sameViewerArea){       if(playList != playListDispatcher){           playListDispatcher.set('selectedIndex', -1);           playListDispatcher.bind('change', changeFunction, this);       }   }   else{       viewerArea.set('visible', true);   }   var panoramaSequenceIndex = -1;   var panoramaSequence = undefined;   var camera = itemDispatcher.get('camera');   if(camera){       panoramaSequence = camera.get('initialSequence');       if(panoramaSequence) {           panoramaSequenceIndex = panoramaSequence.get('movementIndex');       }   }   playList.set('selectedIndex', index);   var player = undefined;   if(item.get('player') != itemDispatcher.get('player')){       player = item.get('player');       player.bind('stateChange', stateChangeFunction, this);   }   this.executeFunctionWhenChange(playList, index, dispose); },
  "pauseGlobalAudios": function(caller){    var audios = window.currentGlobalAudios;   window.currentGlobalAudiosActionCaller = caller;   if(!audios) return;   for(var i = 0, count = audios.length; i<count; i++){       audios[i].pause();   } },
  "showWindow": function(w, autoCloseMilliSeconds, containsAudio){    var closeFunction = function(){       clearAutoClose();       this.resumePlayers(playersPaused, !containsAudio);       w.unbind('close', closeFunction, this);   };   var clearAutoClose = function(){       w.unbind('click', clearAutoClose, this);       if(timeoutID != undefined){           clearTimeout(timeoutID);       }   };   var timeoutID = undefined;   if(autoCloseMilliSeconds){       var autoCloseFunction = function(){           w.hide();       };       w.bind('click', clearAutoClose, this);       timeoutID = setTimeout(autoCloseFunction, autoCloseMilliSeconds);   }   var playersPaused = this.pauseCurrentPlayers(!containsAudio);   w.bind('close', closeFunction, this);   w.show(this, true); },
  "playAudioList": function(audios){    if(audios.length == 0) return;   var currentAudioCount = -1;   var currentAudio;   var playGlobalAudioFunction = this.playGlobalAudio;   var playNext = function(){       if(++currentAudioCount >= audios.length)           currentAudioCount = 0;       currentAudio = audios[currentAudioCount];       playGlobalAudioFunction(currentAudio, playNext);   };   playNext(); },
  "shareFacebook": function(url){    window.open('https://www.facebook.com/sharer/sharer.php?u=' + url, '_blank'); },
  "autotriggerAtStart": function(player, callback){    var stateChangeFunction = function(event){        if(event.data.state == 'playing'){           callback();           player.unbind('stateChange', stateChangeFunction, this);       }   };   player.bind('stateChange', stateChangeFunction, this); },
  "updatePlayListsUI": function(playLists){    var changeFunction = function(event){       var playListDispatched = event.source;       var selectedIndex = playListDispatched.get('selectedIndex');       if(selectedIndex < 0)           return;       var media = playListDispatched.get('items')[selectedIndex].get('media');       for(var i = 0, count = playLists.length; i<count; ++i){           var playList = playLists[i];           if(playList != playListDispatched){               var items = playList.get('items');               for(var j = 0, countJ = items.length; j<countJ; ++j){                   if(items[j].get('media') == media){                       if(playList.get('selectedIndex') != j){                           playList.set('selectedIndex', j);                       }                       break;                   }               }           }       }   };   for(var i = 0, count = playLists.length; i<count; ++i){       playLists[i].bind('change', changeFunction, this);   } },
  "changeBackgroundWhilePlay": function(playList, index, color){    var changeFunction = function(event){       if(event.data.previousSelectedIndex == index){           playList.unbind('change', changeFunction, this);           if((color == viewerArea.get('backgroundColor')) && (colorRatios == viewerArea.get('backgroundColorRatios'))){               viewerArea.set('backgroundColor', backgroundColorBackup);               viewerArea.set('backgroundColorRatios', backgroundColorRatiosBackup);           }       }   };   var playListItem = playList.get('items')[index];   var player = playListItem.get('player');   var viewerArea = player.get('viewerArea');   var backgroundColorBackup = viewerArea.get('backgroundColor');   var backgroundColorRatiosBackup = viewerArea.get('backgroundColorRatios');   var colorRatios = [0];   if((color != backgroundColorBackup) || (colorRatios != backgroundColorRatiosBackup)){       viewerArea.set('backgroundColor', color);       viewerArea.set('backgroundColorRatios', colorRatios);       playList.bind('change', changeFunction, this);   } },
  "showPopupImage": function(image, toggleImage, customWidth, customHeight, showEffect, hideEffect, closeButtonProperties, autoCloseMilliSeconds, audio, stopBackgroundAudio, loadedCallback, hideCallback){    var self = this;   var closed = false;   var playerClickFunction = function() {       zoomImage.unbind('loaded', loadedFunction, self);       hideFunction();   };   var clearAutoClose = function(){       zoomImage.unbind('click', clearAutoClose, this);       if(timeoutID != undefined){           clearTimeout(timeoutID);       }   };   var loadedFunction = function(){       self.unbind('click', playerClickFunction, self);       veil.set('visible', true);       setCloseButtonPosition();       closeButton.set('visible', true);       zoomImage.unbind('loaded', loadedFunction, this);       zoomImage.bind('userInteractionStart', userInteractionStartFunction, this);       zoomImage.bind('userInteractionEnd', userInteractionEndFunction, this);       timeoutID = setTimeout(timeoutFunction, 200);   };   var timeoutFunction = function(){       timeoutID = undefined;       if(autoCloseMilliSeconds){           var autoCloseFunction = function(){               hideFunction();           };           zoomImage.bind('click', clearAutoClose, this);           timeoutID = setTimeout(autoCloseFunction, autoCloseMilliSeconds);       }       zoomImage.bind('backgroundClick', hideFunction, this);       if(toggleImage) {           zoomImage.bind('click', toggleFunction, this);           zoomImage.set('imageCursor', 'hand');       }       closeButton.bind('click', hideFunction, this);       if(loadedCallback)           loadedCallback();   };   var hideFunction = function() {       closed = true;       if(timeoutID)           clearTimeout(timeoutID);       if(autoCloseMilliSeconds)           clearAutoClose();       if(hideCallback)           hideCallback();       zoomImage.set('visible', false);       if(hideEffect && hideEffect.get('duration') > 0){           hideEffect.bind('end', endEffectFunction, this);       }       else{           zoomImage.set('image', null);       }       closeButton.set('visible', false);       veil.set('visible', false);       self.unbind('click', playerClickFunction, self);       zoomImage.unbind('backgroundClick', hideFunction, this);       zoomImage.unbind('userInteractionStart', userInteractionStartFunction, this);       zoomImage.unbind('userInteractionEnd', userInteractionEndFunction, this, true);       if(toggleImage) {           zoomImage.unbind('click', toggleFunction, this);           zoomImage.set('cursor', 'default');       }       closeButton.unbind('click', hideFunction, this);       self.resumePlayers(playersPaused, audio == null || stopBackgroundAudio);       if(audio){           if(stopBackgroundAudio){               self.resumeGlobalAudios();           }           self.stopGlobalAudio(audio);       }   };   var endEffectFunction = function() {       zoomImage.set('image', null);       hideEffect.unbind('end', endEffectFunction, this);   };   var toggleFunction = function() {       zoomImage.set('image', isToggleVisible() ? image : toggleImage);   };   var isToggleVisible = function() {       return zoomImage.get('image') == toggleImage;   };   var setCloseButtonPosition = function() {       var right = zoomImage.get('actualWidth') - zoomImage.get('imageLeft') - zoomImage.get('imageWidth') + 10;       var top = zoomImage.get('imageTop') + 10;       if(right < 10) right = 10;       if(top < 10) top = 10;       closeButton.set('right', right);       closeButton.set('top', top);   };   var userInteractionStartFunction = function() {       if(timeoutUserInteractionID){           clearTimeout(timeoutUserInteractionID);           timeoutUserInteractionID = undefined;       }       else{           closeButton.set('visible', false);       }   };   var userInteractionEndFunction = function() {       if(!closed){           timeoutUserInteractionID = setTimeout(userInteractionTimeoutFunction, 300);       }   };   var userInteractionTimeoutFunction = function() {       timeoutUserInteractionID = undefined;       closeButton.set('visible', true);       setCloseButtonPosition();   };   var veil = this.veilPopupPanorama;   var zoomImage = this.zoomImagePopupPanorama;   var closeButton = this.closeButtonPopupPanorama;   if(closeButtonProperties){       for(var key in closeButtonProperties){           closeButton.set(key, closeButtonProperties[key]);       }   }   var playersPaused = this.pauseCurrentPlayers(audio == null || !stopBackgroundAudio);   if(audio){       if(stopBackgroundAudio){           this.pauseGlobalAudios();       }       this.playGlobalAudio(audio);   }   var timeoutID = undefined;   var timeoutUserInteractionID = undefined;   zoomImage.bind('loaded', loadedFunction, this);   setTimeout(function(){ self.bind('click', playerClickFunction, self, false); }, 0);   zoomImage.set('image', image);   zoomImage.set('customWidth', customWidth);   zoomImage.set('customHeight', customHeight);   zoomImage.set('showEffect', showEffect);   zoomImage.set('hideEffect', hideEffect);   zoomImage.set('visible', true);   return zoomImage; },
  "resumeGlobalAudios": function(caller){    if(window.currentGlobalAudiosActionCaller && window.currentGlobalAudiosActionCaller != caller)       return;   window.currentGlobalAudiosActionCaller = undefined;   var audios = window.currentGlobalAudios;   if(!audios) return;   for(var i = 0, count = audios.length; i<count; i++){       audios[i].play();   } },
  "getPlayListWithMedia": function(media, onlySelected){    var playLists = this.getByClassName('PlayList');   for(var i = 0, count = playLists.length; i<count; ++i){       var playList = playLists[i];       if(onlySelected && playList.get('selectedIndex') == -1)           continue;       var items = playList.get('items');       for(var j = 0, countJ = items.length; j<countJ; ++j){           if(items[j].get('media') == media)               return playList;       }   }   return undefined; },
  "executeFunctionWhenChange": function(playList, index, endFunction, changeFunction){    var self = this;   var changePlayListFunction = function(event){       if(event.data.previousSelectedIndex == index){           if(changeFunction) changeFunction();           if(endFunction) endObject.unbind('end', endFunction, self);           playList.unbind('change', changePlayListFunction, self);       }   };   if(endFunction){       var playListItem = playList.get('items')[index];       var camera = playListItem.get('camera');       if(camera){           var endObject = camera.get('initialSequence');           if(!endObject) return;       }       else{           endObject = playListItem.get('media');       }       endObject.bind('end', endFunction, this);   }   playList.bind('change', changePlayListFunction, this); },
  "stopGlobalAudio": function(audio){    var audios = window.currentGlobalAudios;   if(audios){       var index = audios.indexOf(audio);       if(index != -1){           audios.splice(index, 1);       }   }   audio.stop(); },
  "updateVideoCues": function(playList, index){    var playListItem = playList.get('items')[index];   var video = playListItem.get('media');   if(video.get('cues').length == 0)       return;   var player = playListItem.get('player');   var cues = [];   var changeFunction = function(){       if(playList.get('selectedIndex') != index){           video.unbind('cueChange', cueChangeFunction, this);           playList.unbind('change', changeFunction, this);       }   };   var cueChangeFunction = function(event){       var activeCues = event.data.activeCues;       for(var i = 0, count = cues.length; i<count; ++i){           var cue = cues[i];           if(activeCues.indexOf(cue) == -1 && (cue.get('startTime') > player.get('currentTime') || cue.get('endTime') < player.get('currentTime')+0.5)){               cue.trigger('end');           }       }       cues = activeCues;   };   video.bind('cueChange', cueChangeFunction, this);   playList.bind('change', changeFunction, this); },
  "showComponentsWhileMouseOver": function(parentComponent, components, durationVisibleWhileOut){    var rollOutFunction = function(){       var rollOverFunction = function(){           clearTimeout(timeoutID);           dispose();       };       var timeoutFunction = function(){           setVisibility(false);           dispose();       };       var dispose = function(){           parentComponent.unbind('rollOver', rollOverFunction, this);       };       parentComponent.unbind('rollOut', rollOutFunction, this);       parentComponent.bind('rollOver', rollOverFunction, this);       var timeoutID = setTimeout(timeoutFunction, durationVisibleWhileOut);   };   var setVisibility = function(visible){       for(var i = 0, length = components.length; i<length; i++){           components[i].set('visible', visible);       }   };   parentComponent.bind('rollOut', rollOutFunction, this);   setVisibility(true); },
  "resumePlayers": function(players, onlyResumeCameraIfPanorama){    for(var i = 0; i<players.length; ++i){       var player = players[i];       if(onlyResumeCameraIfPanorama && typeof player.resumeCamera !== 'undefined'){           player.resumeCamera();       }       else{           player.play();       }   } },
  "shareGoogle": function(url){    window.open('https://plus.google.com/share?url=' + url, '_blank'); },
  "fixTogglePlayPauseButton": function(player){    var state = player.get('state');   var button = player.get('buttonPlayPause');   if(typeof button !== 'undefined' && player.get('state') == 'playing'){       button.set('pressed', true);   } },
  "loopAlbum": function(playList, index){    var playListItem = playList.get('items')[index];   var player = playListItem.get('player');   var loopFunction = function(){       player.play();   };   this.executeFunctionWhenChange(playList, index, loopFunction); },
  "getActivePlayerWithViewer": function(viewerArea){    var players = this.getByClassName('PanoramaPlayer');   players = players.concat(this.getByClassName('VideoPlayer'));   players = players.concat(this.getByClassName('PhotoAlbumPlayer'));   players = players.concat(this.getByClassName('MapPlayer'));   var i = players.length;   while(i-- > 0){       var player = players[i];       if(player.get('viewerArea') == viewerArea && player.get('state') == 'playing') {           return player;       }   }   return undefined; },
  "playGlobalAudio": function(audio, endCallback){    var endFunction = function(){       audio.unbind('end', endFunction, this);       this.stopGlobalAudio(audio);       if(endCallback)           endCallback();   };   var audios = window.currentGlobalAudios;   if(!audios){       audios = window.currentGlobalAudios = [audio];   }   else if(audios.indexOf(audio) == -1){       audios.push(audio);   }   audio.bind('end', endFunction, this);   audio.play(); },
  "visibleComponentsIfPlayerFlagEnabled": function(components, playerFlag){    var enabled = this.get(playerFlag);   for(var i in components){       components[i].set('visible', enabled);   } },
  "isCardboardVideMode": function(media, onlySelected){    var players = this.getByClassName('PanoramaPlayer');   return players.length > 0 && players[0].get('viewMode') == 'cardboard'; },
  "setComponentVisibility": function(component, visible, applyAt, effect, propertyEffect, ignoreClearTimeout){    var changeVisibility = function(){        if(effect && propertyEffect){            component.set(propertyEffect, effect);        }        component.set('visible', visible);       if(component.get('class') == 'ViewerArea'){           try{               if(visible) component.restart();               else        component.pause();           }           catch(e){};       }   };   var effectTimeoutName = 'effectTimeout_'+component.get('id');   if(!ignoreClearTimeout && window.hasOwnProperty(effectTimeoutName)){       var effectTimeout = window[effectTimeoutName];       if(effectTimeout instanceof Array){           for(var i=0; i<effectTimeout.length; i++){ clearTimeout(effectTimeout[i]) }       }else{           clearTimeout(effectTimeout);       }       delete window[effectTimeoutName];   }   else if(visible == component.get('visible') && !ignoreClearTimeout)       return;   if(applyAt && applyAt > 0){       var effectTimeout = setTimeout(function(){            if(window[effectTimeoutName] instanceof Array) {                var arrayTimeoutVal = window[effectTimeoutName];               var index = arrayTimeoutVal.indexOf(effectTimeout);               arrayTimeoutVal.splice(index, 1);               if(arrayTimeoutVal.length == 0){                   delete window[effectTimeoutName];               }           }else{               delete window[effectTimeoutName];            }           changeVisibility();        }, applyAt);       if(window.hasOwnProperty(effectTimeoutName)){           window[effectTimeoutName] = [window[effectTimeoutName], effectTimeout];       }else{           window[effectTimeoutName] = effectTimeout;       }   }   else{       changeVisibility();   } },
  "showPopupMedia": function(w, media, playList, popupMaxWidth, popupMaxHeight, autoCloseWhenFinished, containsAudio){    var self = this;   var closeFunction = function(){       this.resumePlayers(playersPaused, !containsAudio);       if(isVideo) {           this.unbind('resize', resizeFunction, this);       }       w.unbind('close', closeFunction, this);   };   var endFunction = function(){       w.hide();   };   var resizeFunction = function(){       var parentWidth = self.get('actualWidth');       var parentHeight = self.get('actualHeight');       var mediaWidth = media.get('width');       var mediaHeight = media.get('height');       var popupMaxWidthNumber = parseFloat(popupMaxWidth) / 100;       var popupMaxHeightNumber = parseFloat(popupMaxHeight) / 100;       var windowWidth = popupMaxWidthNumber * parentWidth;       var windowHeight = popupMaxHeightNumber * parentHeight;       var footerHeight = w.get('footerHeight');       var headerHeight = w.get('headerHeight');       if(!headerHeight) {           var closeButtonHeight = w.get('closeButtonIconHeight') + w.get('closeButtonPaddingTop') + w.get('closeButtonPaddingBottom');           var titleHeight = w.get('titleFontSize') + w.get('titlePaddingTop') + w.get('titlePaddingBottom');           headerHeight = closeButtonHeight > titleHeight ? closeButtonHeight : titleHeight;           headerHeight += w.get('headerPaddingTop') + w.get('headerPaddingBottom');       }       if(!footerHeight) {           footerHeight = 0;       }       var contentWindowWidth = windowWidth - w.get('bodyPaddingLeft') - w.get('bodyPaddingRight') - w.get('paddingLeft') - w.get('paddingRight');       var contentWindowHeight = windowHeight - headerHeight - footerHeight - w.get('bodyPaddingTop') - w.get('bodyPaddingBottom') - w.get('paddingTop') - w.get('paddingBottom');       var parentAspectRatio = contentWindowWidth / contentWindowHeight;       var mediaAspectRatio = mediaWidth / mediaHeight;       if(parentAspectRatio > mediaAspectRatio) {           windowWidth = contentWindowHeight * mediaAspectRatio + w.get('bodyPaddingLeft') + w.get('bodyPaddingRight') + w.get('paddingLeft') + w.get('paddingRight');       }       else {           windowHeight = contentWindowWidth / mediaAspectRatio + headerHeight + footerHeight + w.get('bodyPaddingTop') + w.get('bodyPaddingBottom') + w.get('paddingTop') + w.get('paddingBottom');       }       if(windowWidth > parentWidth * popupMaxWidthNumber) {           windowWidth = parentWidth * popupMaxWidthNumber;       }       if(windowHeight > parentHeight * popupMaxHeightNumber) {           windowHeight = parentHeight * popupMaxHeightNumber;       }       w.set('width', windowWidth);       w.set('height', windowHeight);       w.set('x', (parentWidth - w.get('actualWidth')) * 0.5);       w.set('y', (parentHeight - w.get('actualHeight')) * 0.5);   };   if(autoCloseWhenFinished){       this.executeFunctionWhenChange(playList, 0, endFunction);   }   var isVideo = media.get('video') != undefined;   if(isVideo){       this.bind('resize', resizeFunction, this);       resizeFunction();   }   else {       w.set('width', popupMaxWidth);       w.set('height', popupMaxHeight);   }   var playersPaused = this.pauseCurrentPlayers(!containsAudio);   w.bind('close', closeFunction, this);   w.show(this, true); },
  "loadFromCurrentMediaPlayList": function(playList, delta){    var currentIndex = playList.get('selectedIndex');   var totalItems = playList.get('items').length;   var newIndex = (currentIndex + delta) % totalItems;   while(newIndex < 0){       newIndex = totalItems + newIndex;   };   if(currentIndex != newIndex){       playList.set('selectedIndex', newIndex);   }; },
  "playGlobalAudioWhilePlay": function(playList, index, audio, endCallback){    var changeFunction = function(event){       if(event.data.previousSelectedIndex == index){           this.stopGlobalAudio(audio);           playList.unbind('change', changeFunction, this);           if(endCallback)               endCallback();       }   };   playList.bind('change', changeFunction, this);   this.playGlobalAudio(audio, endCallback); },
  "pauseCurrentPlayers": function(onlyPauseCameraIfPanorama){    var players = this.getByClassName('PanoramaPlayer');   players = players.concat(this.getByClassName('VideoPlayer'));   players = players.concat(this.getByClassName('PhotoAlbumPlayer'));   var i = players.length;   while(i-- > 0){       var player = players[i];       if(player.get('state') == 'playing') {           if(onlyPauseCameraIfPanorama && typeof player.pauseCamera !== 'undefined'){               player.pauseCamera();           }           else{               player.pause();           }       }       else {           players.splice(i, 1);       }   }   return players; },
  "setMapLocation": function(panoramaPlayListItem, mapPlayer){    var resetFunction = function(){       panoramaPlayListItem.unbind('stop', resetFunction, this);       player.set('mapPlayer', null);   };   panoramaPlayListItem.bind('stop', resetFunction, this);   var player = panoramaPlayListItem.get('player');   player.set('mapPlayer', mapPlayer); },
  "updateMediaLabelFromPlayList": function(playList, htmlText, playListItemStopToDispose){    var changeFunction = function(){       var index = playList.get('selectedIndex');       if(index >= 0){           var beginFunction = function(){               playListItem.unbind('begin', beginFunction);               setMediaLabel(index);           };           var setMediaLabel = function(index){               var media = playListItem.get('media');               var text = media.get('data');               if(!text)                   text = media.get('label');               setHtml(text);           };           var setHtml = function(text){               htmlText.set('html', '<div style=\"text-align:left\"><SPAN STYLE=\"color:#FFFFFF;font-size:12px;font-family:Verdana\"><span color=\"white\" font-family=\"Verdana\" font-size=\"12px\">' + text + '</SPAN></div>');                          };           var playListItem = playList.get('items')[index];           if(htmlText.get('html')){               setHtml('Loading...');               playListItem.bind('begin', beginFunction);           }           else{               setMediaLabel(index);           }       }   };   var disposeFunction = function(){       htmlText.set('html', undefined);       playList.unbind('change', changeFunction, this);       playListItemStopToDispose.unbind('stop', disposeFunction, this);   };   if(playListItemStopToDispose){       playListItemStopToDispose.bind('stop', disposeFunction, this);   }   playList.bind('change', changeFunction, this);   changeFunction(); },
  "setEndToItemIndex": function(playList, fromIndex, toIndex){    var endFunction = function(){       if(playList.get('selectedIndex') == fromIndex)           playList.set('selectedIndex', toIndex);   };   this.executeFunctionWhenChange(playList, fromIndex, endFunction); },
  "startPanoramaWithCamera": function(playList, index, camera){    var playListItem = playList.get('items')[index];   var previousCamera = playListItem.get('camera');   playListItem.set('camera', camera);   var restoreCameraOnStop = function(){       playListItem.set('camera', previousCamera);       playListItem.unbind('stop', restoreCameraOnStop, this);   };   playListItem.bind('stop', restoreCameraOnStop, this);   playList.set('selectedIndex', index); },
  "shareTwitter": function(url){    window.open('https://twitter.com/intent/tweet?source=webclient&url=' + url, '_blank'); }
 },
 "paddingLeft": 0,
 "scrollBarMargin": 2,
 "verticalAlign": "top",
 "backgroundPreloadEnabled": true,
 "overflow": "visible",
 "paddingRight": 0,
 "shadow": false
})