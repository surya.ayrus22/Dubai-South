TDV.PlayerAPI.defineScript({ "definitions": [
 {
  "cardboardMenu": {
   "rollOverOpacity": 0.8,
   "rollOverFontColor": "#FFFFFF",
   "opacity": 0.4,
   "selectedFontColor": "#FFFFFF",
   "fontColor": "#FFFFFF",
   "id": "Menu_A68378F1_A868_1DA3_41DD_E0FE28FB864D",
   "fontFamily": "Arial",
   "backgroundColor": "#404040",
   "class": "Menu",
   "selectedBackgroundColor": "#202020",
   "label": "Panoramas",
   "children": [
    {
     "class": "MenuItem",
     "label": "C Block Ext new",
     "click": "this.mainPlayList.set('selectedIndex', 0)"
    },
    {
     "class": "MenuItem",
     "label": "[Group 1]-IMG_0206_IMG_0242-36 images",
     "click": "this.mainPlayList.set('selectedIndex', 1)"
    },
    {
     "class": "MenuItem",
     "label": "office Space area",
     "click": "this.mainPlayList.set('selectedIndex', 2)"
    },
    {
     "class": "MenuItem",
     "label": "Block A Pano",
     "click": "this.mainPlayList.set('selectedIndex', 3)"
    }
   ],
   "rollOverBackgroundColor": "#000000"
  },
  "hfovMin": 60,
  "pitch": 0,
  "hfov": 360,
  "id": "panorama_A08179CC_A838_1FE0_41CF_1A619BE9EBE7",
  "frames": [
   {
    "top": {
     "levels": [
      {
       "height": 2864,
       "url": "media/panorama_A08179CC_A838_1FE0_41CF_1A619BE9EBE7_u_hq.jpeg",
       "class": "ImageResourceLevel",
       "width": 2864
      },
      {
       "height": 800,
       "url": "media/panorama_A08179CC_A838_1FE0_41CF_1A619BE9EBE7_u.jpeg",
       "class": "ImageResourceLevel",
       "width": 800
      }
     ],
     "class": "ImageResource"
    },
    "front": {
     "levels": [
      {
       "height": 2864,
       "url": "media/panorama_A08179CC_A838_1FE0_41CF_1A619BE9EBE7_f_hq.jpeg",
       "class": "ImageResourceLevel",
       "width": 2864
      },
      {
       "height": 800,
       "url": "media/panorama_A08179CC_A838_1FE0_41CF_1A619BE9EBE7_f.jpeg",
       "class": "ImageResourceLevel",
       "width": 800
      }
     ],
     "class": "ImageResource"
    },
    "class": "CubicPanoramaFrame",
    "back": {
     "levels": [
      {
       "height": 2864,
       "url": "media/panorama_A08179CC_A838_1FE0_41CF_1A619BE9EBE7_b_hq.jpeg",
       "class": "ImageResourceLevel",
       "width": 2864
      },
      {
       "height": 800,
       "url": "media/panorama_A08179CC_A838_1FE0_41CF_1A619BE9EBE7_b.jpeg",
       "class": "ImageResourceLevel",
       "width": 800
      }
     ],
     "class": "ImageResource"
    },
    "overlays": [
     {
      "class": "HotspotPanoramaOverlay",
      "items": [
       {
        "yaw": -0.8651504212046524,
        "image": {
         "levels": [
          {
           "height": 72,
           "url": "media/panorama_A08179CC_A838_1FE0_41CF_1A619BE9EBE7_0_HS_0_0.png",
           "class": "ImageResourceLevel",
           "width": 72
          }
         ],
         "class": "ImageResource"
        },
        "pitch": 1.5840835966860998,
        "hfov": 2.885054140989662,
        "roll": 0,
        "class": "HotspotPanoramaOverlayImage"
       }
      ],
      "maps": [
       {
        "yaw": -0.8651504212046524,
        "image": {
         "levels": [
          {
           "height": 36,
           "url": "media/panorama_A08179CC_A838_1FE0_41CF_1A619BE9EBE7_0_HS_0_0_0_map.gif",
           "class": "ImageResourceLevel",
           "width": 36
          }
         ],
         "class": "ImageResource"
        },
        "class": "HotspotPanoramaOverlayMap",
        "pitch": 1.5840835966860998,
        "hfov": 2.885054140989662,
        "roll": 0
       }
      ],
      "areas": [
       {
        "mapColor": "#FF0000",
        "toolTip": "Recpetion",
        "class": "HotspotPanoramaOverlayArea",
        "click": "this.mainPlayList.set('selectedIndex', 1)"
       }
      ],
      "enabledInCardboard": true,
      "id": "overlay_A25BF2E4_A818_0DA0_41DD_E595F009549F",
      "useHandCursor": true,
      "rollOverDisplay": false
     }
    ],
    "bottom": {
     "levels": [
      {
       "height": 2864,
       "url": "media/panorama_A08179CC_A838_1FE0_41CF_1A619BE9EBE7_d_hq.jpeg",
       "class": "ImageResourceLevel",
       "width": 2864
      },
      {
       "height": 800,
       "url": "media/panorama_A08179CC_A838_1FE0_41CF_1A619BE9EBE7_d.jpeg",
       "class": "ImageResourceLevel",
       "width": 800
      }
     ],
     "class": "ImageResource"
    },
    "thumbnailUrl": "media/panorama_A08179CC_A838_1FE0_41CF_1A619BE9EBE7_t.jpg",
    "left": {
     "levels": [
      {
       "height": 2864,
       "url": "media/panorama_A08179CC_A838_1FE0_41CF_1A619BE9EBE7_l_hq.jpeg",
       "class": "ImageResourceLevel",
       "width": 2864
      },
      {
       "height": 800,
       "url": "media/panorama_A08179CC_A838_1FE0_41CF_1A619BE9EBE7_l.jpeg",
       "class": "ImageResourceLevel",
       "width": 800
      }
     ],
     "class": "ImageResource"
    },
    "right": {
     "levels": [
      {
       "height": 2864,
       "url": "media/panorama_A08179CC_A838_1FE0_41CF_1A619BE9EBE7_r_hq.jpeg",
       "class": "ImageResourceLevel",
       "width": 2864
      },
      {
       "height": 800,
       "url": "media/panorama_A08179CC_A838_1FE0_41CF_1A619BE9EBE7_r.jpeg",
       "class": "ImageResourceLevel",
       "width": 800
      }
     ],
     "class": "ImageResource"
    }
   }
  ],
  "class": "Panorama",
  "hfovMax": 84,
  "label": "C Block Ext new",
  "partial": false,
  "vfov": 180,
  "thumbnailUrl": "media/panorama_A08179CC_A838_1FE0_41CF_1A619BE9EBE7_t.jpg"
 },
 {
  "buttonMoveUp": {
   "width": 32,
   "height": 32,
   "shadow": false,
   "id": "IconButton_5645B517_5EBA_61DA_41D6_9282B67C524F",
   "pressedIconURL": "skin/IconButton_5645B517_5EBA_61DA_41D6_9282B67C524F_pressed.png",
   "transparencyActive": true,
   "mode": "push",
   "paddingTop": 0,
   "iconURL": "skin/IconButton_5645B517_5EBA_61DA_41D6_9282B67C524F.png",
   "cursor": "hand",
   "horizontalAlign": "center",
   "borderRadius": 0,
   "class": "IconButton",
   "rollOverIconURL": "skin/IconButton_5645B517_5EBA_61DA_41D6_9282B67C524F_rollover.png",
   "verticalAlign": "middle",
   "minHeight": 0,
   "paddingLeft": 0,
   "paddingBottom": 0,
   "minWidth": 0,
   "borderSize": 0,
   "backgroundOpacity": 0,
   "paddingRight": 0
  },
  "buttonRestart": {
   "width": 40,
   "height": 40,
   "shadow": false,
   "id": "IconButton_5645B517_5EBA_61DA_41D4_85AB00D87A76",
   "pressedIconURL": "skin/IconButton_5645B517_5EBA_61DA_41D4_85AB00D87A76_pressed.png",
   "transparencyActive": true,
   "mode": "push",
   "paddingTop": 0,
   "iconURL": "skin/IconButton_5645B517_5EBA_61DA_41D4_85AB00D87A76.png",
   "cursor": "hand",
   "horizontalAlign": "center",
   "borderRadius": 0,
   "class": "IconButton",
   "rollOverIconURL": "skin/IconButton_5645B517_5EBA_61DA_41D4_85AB00D87A76_rollover.png",
   "verticalAlign": "middle",
   "minHeight": 0,
   "paddingLeft": 0,
   "paddingBottom": 0,
   "minWidth": 0,
   "borderSize": 0,
   "backgroundOpacity": 0,
   "paddingRight": 0
  },
  "mouseControlMode": "drag_acceleration",
  "buttonPlayRight": {
   "width": 40,
   "height": 40,
   "shadow": false,
   "id": "IconButton_5645B517_5EBA_61DA_41C5_1BC8156F8BCF",
   "pressedIconURL": "skin/IconButton_5645B517_5EBA_61DA_41C5_1BC8156F8BCF_pressed.png",
   "transparencyActive": true,
   "mode": "push",
   "paddingTop": 0,
   "iconURL": "skin/IconButton_5645B517_5EBA_61DA_41C5_1BC8156F8BCF.png",
   "cursor": "hand",
   "horizontalAlign": "center",
   "borderRadius": 0,
   "class": "IconButton",
   "rollOverIconURL": "skin/IconButton_5645B517_5EBA_61DA_41C5_1BC8156F8BCF_rollover.png",
   "verticalAlign": "middle",
   "minHeight": 0,
   "paddingLeft": 0,
   "paddingBottom": 0,
   "minWidth": 0,
   "borderSize": 0,
   "backgroundOpacity": 0,
   "paddingRight": 0
  },
  "buttonMoveDown": {
   "width": 32,
   "height": 32,
   "shadow": false,
   "id": "IconButton_5645B517_5EBA_61DA_41D6_E94D03438B44",
   "pressedIconURL": "skin/IconButton_5645B517_5EBA_61DA_41D6_E94D03438B44_pressed.png",
   "transparencyActive": true,
   "mode": "push",
   "paddingTop": 0,
   "iconURL": "skin/IconButton_5645B517_5EBA_61DA_41D6_E94D03438B44.png",
   "cursor": "hand",
   "horizontalAlign": "center",
   "borderRadius": 0,
   "class": "IconButton",
   "rollOverIconURL": "skin/IconButton_5645B517_5EBA_61DA_41D6_E94D03438B44_rollover.png",
   "verticalAlign": "middle",
   "minHeight": 0,
   "paddingLeft": 0,
   "paddingBottom": 0,
   "minWidth": 0,
   "borderSize": 0,
   "backgroundOpacity": 0,
   "paddingRight": 0
  },
  "touchControlMode": "drag_rotation",
  "id": "MainViewerPanoramaPlayer",
  "buttonZoomOut": {
   "width": 32,
   "height": 32,
   "shadow": false,
   "id": "IconButton_5645B517_5EBA_61DA_41D3_52532616D552",
   "pressedIconURL": "skin/IconButton_5645B517_5EBA_61DA_41D3_52532616D552_pressed.png",
   "transparencyActive": true,
   "mode": "push",
   "paddingTop": 0,
   "iconURL": "skin/IconButton_5645B517_5EBA_61DA_41D3_52532616D552.png",
   "cursor": "hand",
   "horizontalAlign": "center",
   "borderRadius": 0,
   "class": "IconButton",
   "rollOverIconURL": "skin/IconButton_5645B517_5EBA_61DA_41D3_52532616D552_rollover.png",
   "verticalAlign": "middle",
   "minHeight": 0,
   "paddingLeft": 0,
   "paddingBottom": 0,
   "minWidth": 0,
   "borderSize": 0,
   "backgroundOpacity": 0,
   "paddingRight": 0
  },
  "viewerArea": "this.MainViewer",
  "buttonPlayLeft": {
   "width": 40,
   "height": 40,
   "shadow": false,
   "id": "IconButton_5645B517_5EBA_61DA_41D1_EAD81768A56B",
   "pressedIconURL": "skin/IconButton_5645B517_5EBA_61DA_41D1_EAD81768A56B_pressed.png",
   "transparencyActive": true,
   "mode": "push",
   "paddingTop": 0,
   "iconURL": "skin/IconButton_5645B517_5EBA_61DA_41D1_EAD81768A56B.png",
   "cursor": "hand",
   "horizontalAlign": "center",
   "borderRadius": 0,
   "class": "IconButton",
   "rollOverIconURL": "skin/IconButton_5645B517_5EBA_61DA_41D1_EAD81768A56B_rollover.png",
   "verticalAlign": "middle",
   "minHeight": 0,
   "paddingLeft": 0,
   "paddingBottom": 0,
   "minWidth": 0,
   "borderSize": 0,
   "backgroundOpacity": 0,
   "paddingRight": 0
  },
  "buttonZoomIn": {
   "width": 32,
   "height": 32,
   "shadow": false,
   "id": "IconButton_5645B517_5EBA_61DA_41D2_D639DF8052CA",
   "pressedIconURL": "skin/IconButton_5645B517_5EBA_61DA_41D2_D639DF8052CA_pressed.png",
   "transparencyActive": true,
   "mode": "push",
   "paddingTop": 0,
   "iconURL": "skin/IconButton_5645B517_5EBA_61DA_41D2_D639DF8052CA.png",
   "cursor": "hand",
   "horizontalAlign": "center",
   "borderRadius": 0,
   "class": "IconButton",
   "rollOverIconURL": "skin/IconButton_5645B517_5EBA_61DA_41D2_D639DF8052CA_rollover.png",
   "verticalAlign": "middle",
   "minHeight": 0,
   "paddingLeft": 0,
   "paddingBottom": 0,
   "minWidth": 0,
   "borderSize": 0,
   "backgroundOpacity": 0,
   "paddingRight": 0
  },
  "class": "PanoramaPlayer",
  "buttonMoveRight": {
   "width": 32,
   "height": 32,
   "shadow": false,
   "id": "IconButton_5645B517_5EBA_61DA_41BA_A43ED1B1FB40",
   "pressedIconURL": "skin/IconButton_5645B517_5EBA_61DA_41BA_A43ED1B1FB40_pressed.png",
   "transparencyActive": true,
   "mode": "push",
   "paddingTop": 0,
   "iconURL": "skin/IconButton_5645B517_5EBA_61DA_41BA_A43ED1B1FB40.png",
   "cursor": "hand",
   "horizontalAlign": "center",
   "borderRadius": 0,
   "class": "IconButton",
   "rollOverIconURL": "skin/IconButton_5645B517_5EBA_61DA_41BA_A43ED1B1FB40_rollover.png",
   "verticalAlign": "middle",
   "minHeight": 0,
   "paddingLeft": 0,
   "paddingBottom": 0,
   "minWidth": 0,
   "borderSize": 0,
   "backgroundOpacity": 0,
   "paddingRight": 0
  },
  "buttonMoveLeft": {
   "width": 32,
   "height": 32,
   "shadow": false,
   "id": "IconButton_5645B517_5EBA_61DA_41A9_0D8835A41A7C",
   "pressedIconURL": "skin/IconButton_5645B517_5EBA_61DA_41A9_0D8835A41A7C_pressed.png",
   "transparencyActive": true,
   "mode": "push",
   "paddingTop": 0,
   "iconURL": "skin/IconButton_5645B517_5EBA_61DA_41A9_0D8835A41A7C.png",
   "cursor": "hand",
   "horizontalAlign": "center",
   "borderRadius": 0,
   "class": "IconButton",
   "rollOverIconURL": "skin/IconButton_5645B517_5EBA_61DA_41A9_0D8835A41A7C_rollover.png",
   "verticalAlign": "middle",
   "minHeight": 0,
   "paddingLeft": 0,
   "paddingBottom": 0,
   "minWidth": 0,
   "borderSize": 0,
   "backgroundOpacity": 0,
   "paddingRight": 0
  },
  "buttonPause": {
   "width": 40,
   "height": 40,
   "shadow": false,
   "id": "IconButton_5645B517_5EBA_61DA_41BC_DB59BC8AC3A2",
   "pressedIconURL": "skin/IconButton_5645B517_5EBA_61DA_41BC_DB59BC8AC3A2_pressed.png",
   "transparencyActive": true,
   "mode": "toggle",
   "paddingTop": 0,
   "iconURL": "skin/IconButton_5645B517_5EBA_61DA_41BC_DB59BC8AC3A2.png",
   "cursor": "hand",
   "horizontalAlign": "center",
   "borderRadius": 0,
   "class": "IconButton",
   "verticalAlign": "middle",
   "minHeight": 0,
   "paddingLeft": 0,
   "paddingBottom": 0,
   "minWidth": 0,
   "borderSize": 0,
   "backgroundOpacity": 0,
   "paddingRight": 0
  },
  "gyroscopeEnabled": true
 },
 {
  "initialSequence": {
   "movements": [
    {
     "class": "DistancePanoramaCameraMovement",
     "yawSpeed": 8.95,
     "easing": "cubic_in",
     "yawDelta": 18.5
    },
    {
     "class": "DistancePanoramaCameraMovement",
     "yawSpeed": 8.95,
     "easing": "linear",
     "yawDelta": 323
    },
    {
     "class": "DistancePanoramaCameraMovement",
     "yawSpeed": 8.95,
     "easing": "cubic_out",
     "yawDelta": 18.5
    }
   ],
   "restartMovementOnUserInteraction": false,
   "class": "PanoramaCameraSequence"
  },
  "class": "PanoramaCamera",
  "id": "panorama_A08179CC_A838_1FE0_41CF_1A619BE9EBE7_camera",
  "automaticZoomSpeed": 10,
  "initialPosition": {
   "yaw": 0,
   "class": "PanoramaCameraPosition",
   "pitch": 0
  }
 },
 {
  "cardboardMenu": "this.Menu_A68378F1_A868_1DA3_41DD_E0FE28FB864D",
  "hfovMin": 60,
  "pitch": 0,
  "hfov": 360,
  "id": "panorama_A0D9EE81_A838_1460_41B9_2558663E5EE9",
  "frames": [
   {
    "top": {
     "levels": [
      {
       "height": 2864,
       "url": "media/panorama_A0D9EE81_A838_1460_41B9_2558663E5EE9_u_hq.jpeg",
       "class": "ImageResourceLevel",
       "width": 2864
      },
      {
       "height": 800,
       "url": "media/panorama_A0D9EE81_A838_1460_41B9_2558663E5EE9_u.jpeg",
       "class": "ImageResourceLevel",
       "width": 800
      }
     ],
     "class": "ImageResource"
    },
    "front": {
     "levels": [
      {
       "height": 2864,
       "url": "media/panorama_A0D9EE81_A838_1460_41B9_2558663E5EE9_f_hq.jpeg",
       "class": "ImageResourceLevel",
       "width": 2864
      },
      {
       "height": 800,
       "url": "media/panorama_A0D9EE81_A838_1460_41B9_2558663E5EE9_f.jpeg",
       "class": "ImageResourceLevel",
       "width": 800
      }
     ],
     "class": "ImageResource"
    },
    "class": "CubicPanoramaFrame",
    "back": {
     "levels": [
      {
       "height": 2864,
       "url": "media/panorama_A0D9EE81_A838_1460_41B9_2558663E5EE9_b_hq.jpeg",
       "class": "ImageResourceLevel",
       "width": 2864
      },
      {
       "height": 800,
       "url": "media/panorama_A0D9EE81_A838_1460_41B9_2558663E5EE9_b.jpeg",
       "class": "ImageResourceLevel",
       "width": 800
      }
     ],
     "class": "ImageResource"
    },
    "overlays": [
     {
      "class": "HotspotPanoramaOverlay",
      "items": [
       {
        "yaw": 0.29263692904435296,
        "image": {
         "levels": [
          {
           "height": 71,
           "url": "media/panorama_A0D9EE81_A838_1460_41B9_2558663E5EE9_0_HS_1_0.png",
           "class": "ImageResourceLevel",
           "width": 71
          }
         ],
         "class": "ImageResource"
        },
        "pitch": -0.10316324483381227,
        "hfov": 2.8589534343336562,
        "roll": 0,
        "class": "HotspotPanoramaOverlayImage"
       }
      ],
      "maps": [
       {
        "yaw": 0.29263692904435296,
        "image": {
         "levels": [
          {
           "height": 35,
           "url": "media/panorama_A0D9EE81_A838_1460_41B9_2558663E5EE9_0_HS_1_0_0_map.gif",
           "class": "ImageResourceLevel",
           "width": 35
          }
         ],
         "class": "ImageResource"
        },
        "class": "HotspotPanoramaOverlayMap",
        "pitch": -0.10316324483381227,
        "hfov": 2.8589534343336562,
        "roll": 0
       }
      ],
      "areas": [
       {
        "mapColor": "#FF0000",
        "toolTip": "Office Space",
        "class": "HotspotPanoramaOverlayArea"
       }
      ],
      "id": "overlay_A2F2F740_A868_14E0_41C8_2E266C9A698D",
      "useHandCursor": true,
      "rollOverDisplay": false
     },
     {
      "class": "HotspotPanoramaOverlay",
      "items": [
       {
        "yaw": 172.33474219220224,
        "image": {
         "levels": [
          {
           "height": 71,
           "url": "media/panorama_A0D9EE81_A838_1460_41B9_2558663E5EE9_0_HS_0_0.png",
           "class": "ImageResourceLevel",
           "width": 71
          }
         ],
         "class": "ImageResource"
        },
        "pitch": -8.187373771149602,
        "hfov": 2.829818532773371,
        "roll": 0,
        "class": "HotspotPanoramaOverlayImage"
       }
      ],
      "maps": [
       {
        "yaw": 172.33474219220224,
        "image": {
         "levels": [
          {
           "height": 35,
           "url": "media/panorama_A0D9EE81_A838_1460_41B9_2558663E5EE9_0_HS_0_0_0_map.gif",
           "class": "ImageResourceLevel",
           "width": 35
          }
         ],
         "class": "ImageResource"
        },
        "class": "HotspotPanoramaOverlayMap",
        "pitch": -8.187373771149602,
        "hfov": 2.829818532773371,
        "roll": 0
       }
      ],
      "areas": [
       {
        "mapColor": "#FF0000",
        "toolTip": "C block Exterior View",
        "class": "HotspotPanoramaOverlayArea",
        "click": "this.mainPlayList.set('selectedIndex', 0)"
       }
      ],
      "enabledInCardboard": true,
      "id": "overlay_A2AA6227_A868_0CAF_41D2_1A37FF382DC3",
      "useHandCursor": true,
      "rollOverDisplay": false
     }
    ],
    "bottom": {
     "levels": [
      {
       "height": 2864,
       "url": "media/panorama_A0D9EE81_A838_1460_41B9_2558663E5EE9_d_hq.jpeg",
       "class": "ImageResourceLevel",
       "width": 2864
      },
      {
       "height": 800,
       "url": "media/panorama_A0D9EE81_A838_1460_41B9_2558663E5EE9_d.jpeg",
       "class": "ImageResourceLevel",
       "width": 800
      }
     ],
     "class": "ImageResource"
    },
    "thumbnailUrl": "media/panorama_A0D9EE81_A838_1460_41B9_2558663E5EE9_t.jpg",
    "left": {
     "levels": [
      {
       "height": 2864,
       "url": "media/panorama_A0D9EE81_A838_1460_41B9_2558663E5EE9_l_hq.jpeg",
       "class": "ImageResourceLevel",
       "width": 2864
      },
      {
       "height": 800,
       "url": "media/panorama_A0D9EE81_A838_1460_41B9_2558663E5EE9_l.jpeg",
       "class": "ImageResourceLevel",
       "width": 800
      }
     ],
     "class": "ImageResource"
    },
    "right": {
     "levels": [
      {
       "height": 2864,
       "url": "media/panorama_A0D9EE81_A838_1460_41B9_2558663E5EE9_r_hq.jpeg",
       "class": "ImageResourceLevel",
       "width": 2864
      },
      {
       "height": 800,
       "url": "media/panorama_A0D9EE81_A838_1460_41B9_2558663E5EE9_r.jpeg",
       "class": "ImageResourceLevel",
       "width": 800
      }
     ],
     "class": "ImageResource"
    }
   }
  ],
  "class": "Panorama",
  "hfovMax": 120,
  "label": "[Group 1]-IMG_0206_IMG_0242-36 images",
  "partial": false,
  "vfov": 180,
  "thumbnailUrl": "media/panorama_A0D9EE81_A838_1460_41B9_2558663E5EE9_t.jpg"
 },
 {
  "initialSequence": {
   "movements": [
    {
     "class": "DistancePanoramaCameraMovement",
     "yawSpeed": 8.95,
     "easing": "cubic_in",
     "yawDelta": 18.5
    },
    {
     "class": "DistancePanoramaCameraMovement",
     "yawSpeed": 8.95,
     "easing": "linear",
     "yawDelta": 323
    },
    {
     "class": "DistancePanoramaCameraMovement",
     "yawSpeed": 8.95,
     "easing": "cubic_out",
     "yawDelta": 18.5
    }
   ],
   "restartMovementOnUserInteraction": false,
   "class": "PanoramaCameraSequence"
  },
  "class": "PanoramaCamera",
  "id": "panorama_A0D9EE81_A838_1460_41B9_2558663E5EE9_camera",
  "automaticZoomSpeed": 10,
  "initialPosition": {
   "yaw": 0,
   "class": "PanoramaCameraPosition",
   "pitch": 0
  }
 },
 {
  "cardboardMenu": "this.Menu_A68378F1_A868_1DA3_41DD_E0FE28FB864D",
  "hfovMin": 60,
  "pitch": 0,
  "hfov": 360,
  "id": "panorama_A0F65CF6_A838_15A1_41D2_91D69933E331",
  "frames": [
   {
    "top": {
     "levels": [
      {
       "height": 2864,
       "url": "media/panorama_A0F65CF6_A838_15A1_41D2_91D69933E331_u_hq.jpeg",
       "class": "ImageResourceLevel",
       "width": 2864
      },
      {
       "height": 800,
       "url": "media/panorama_A0F65CF6_A838_15A1_41D2_91D69933E331_u.jpeg",
       "class": "ImageResourceLevel",
       "width": 800
      }
     ],
     "class": "ImageResource"
    },
    "front": {
     "levels": [
      {
       "height": 2864,
       "url": "media/panorama_A0F65CF6_A838_15A1_41D2_91D69933E331_f_hq.jpeg",
       "class": "ImageResourceLevel",
       "width": 2864
      },
      {
       "height": 800,
       "url": "media/panorama_A0F65CF6_A838_15A1_41D2_91D69933E331_f.jpeg",
       "class": "ImageResourceLevel",
       "width": 800
      }
     ],
     "class": "ImageResource"
    },
    "class": "CubicPanoramaFrame",
    "back": {
     "levels": [
      {
       "height": 2864,
       "url": "media/panorama_A0F65CF6_A838_15A1_41D2_91D69933E331_b_hq.jpeg",
       "class": "ImageResourceLevel",
       "width": 2864
      },
      {
       "height": 800,
       "url": "media/panorama_A0F65CF6_A838_15A1_41D2_91D69933E331_b.jpeg",
       "class": "ImageResourceLevel",
       "width": 800
      }
     ],
     "class": "ImageResource"
    },
    "overlays": [
     {
      "class": "HotspotPanoramaOverlay",
      "items": [
       {
        "yaw": 135.48828445420693,
        "image": {
         "levels": [
          {
           "height": 73,
           "url": "media/panorama_A0F65CF6_A838_15A1_41D2_91D69933E331_0_HS_0_0.png",
           "class": "ImageResourceLevel",
           "width": 73
          }
         ],
         "class": "ImageResource"
        },
        "pitch": -4.688284454206918,
        "hfov": 2.924645283053732,
        "roll": 0,
        "class": "HotspotPanoramaOverlayImage"
       }
      ],
      "maps": [
       {
        "yaw": 135.48828445420693,
        "image": {
         "levels": [
          {
           "height": 36,
           "url": "media/panorama_A0F65CF6_A838_15A1_41D2_91D69933E331_0_HS_0_0_0_map.gif",
           "class": "ImageResourceLevel",
           "width": 36
          }
         ],
         "class": "ImageResource"
        },
        "class": "HotspotPanoramaOverlayMap",
        "pitch": -4.688284454206918,
        "hfov": 2.924645283053732,
        "roll": 0
       }
      ],
      "areas": [
       {
        "mapColor": "#FF0000",
        "toolTip": "A  Block Office Space",
        "class": "HotspotPanoramaOverlayArea",
        "click": "this.mainPlayList.set('selectedIndex', 3)"
       }
      ],
      "enabledInCardboard": true,
      "id": "overlay_A2806DB0_A868_17A0_41C0_C0622F7756F4",
      "useHandCursor": true,
      "rollOverDisplay": false
     }
    ],
    "bottom": {
     "levels": [
      {
       "height": 2864,
       "url": "media/panorama_A0F65CF6_A838_15A1_41D2_91D69933E331_d_hq.jpeg",
       "class": "ImageResourceLevel",
       "width": 2864
      },
      {
       "height": 800,
       "url": "media/panorama_A0F65CF6_A838_15A1_41D2_91D69933E331_d.jpeg",
       "class": "ImageResourceLevel",
       "width": 800
      }
     ],
     "class": "ImageResource"
    },
    "thumbnailUrl": "media/panorama_A0F65CF6_A838_15A1_41D2_91D69933E331_t.jpg",
    "left": {
     "levels": [
      {
       "height": 2864,
       "url": "media/panorama_A0F65CF6_A838_15A1_41D2_91D69933E331_l_hq.jpeg",
       "class": "ImageResourceLevel",
       "width": 2864
      },
      {
       "height": 800,
       "url": "media/panorama_A0F65CF6_A838_15A1_41D2_91D69933E331_l.jpeg",
       "class": "ImageResourceLevel",
       "width": 800
      }
     ],
     "class": "ImageResource"
    },
    "right": {
     "levels": [
      {
       "height": 2864,
       "url": "media/panorama_A0F65CF6_A838_15A1_41D2_91D69933E331_r_hq.jpeg",
       "class": "ImageResourceLevel",
       "width": 2864
      },
      {
       "height": 800,
       "url": "media/panorama_A0F65CF6_A838_15A1_41D2_91D69933E331_r.jpeg",
       "class": "ImageResourceLevel",
       "width": 800
      }
     ],
     "class": "ImageResource"
    }
   }
  ],
  "class": "Panorama",
  "hfovMax": 120,
  "label": "office Space area",
  "partial": false,
  "vfov": 180,
  "thumbnailUrl": "media/panorama_A0F65CF6_A838_15A1_41D2_91D69933E331_t.jpg"
 },
 {
  "initialSequence": {
   "movements": [
    {
     "class": "DistancePanoramaCameraMovement",
     "yawSpeed": 8.95,
     "easing": "cubic_in",
     "yawDelta": 18.5
    },
    {
     "class": "DistancePanoramaCameraMovement",
     "yawSpeed": 8.95,
     "easing": "linear",
     "yawDelta": 323
    },
    {
     "class": "DistancePanoramaCameraMovement",
     "yawSpeed": 8.95,
     "easing": "cubic_out",
     "yawDelta": 18.5
    }
   ],
   "restartMovementOnUserInteraction": false,
   "class": "PanoramaCameraSequence"
  },
  "class": "PanoramaCamera",
  "id": "panorama_A0F65CF6_A838_15A1_41D2_91D69933E331_camera",
  "automaticZoomSpeed": 10,
  "initialPosition": {
   "yaw": 0,
   "class": "PanoramaCameraPosition",
   "pitch": 0
  }
 },
 {
  "cardboardMenu": "this.Menu_A68378F1_A868_1DA3_41DD_E0FE28FB864D",
  "hfovMin": 60,
  "pitch": 0,
  "hfov": 360,
  "id": "panorama_A2A913E5_A818_73A0_41D2_2E5B3782C51A",
  "frames": [
   {
    "top": {
     "levels": [
      {
       "height": 2864,
       "url": "media/panorama_A2A913E5_A818_73A0_41D2_2E5B3782C51A_u_hq.jpeg",
       "class": "ImageResourceLevel",
       "width": 2864
      },
      {
       "height": 800,
       "url": "media/panorama_A2A913E5_A818_73A0_41D2_2E5B3782C51A_u.jpeg",
       "class": "ImageResourceLevel",
       "width": 800
      }
     ],
     "class": "ImageResource"
    },
    "front": {
     "levels": [
      {
       "height": 2864,
       "url": "media/panorama_A2A913E5_A818_73A0_41D2_2E5B3782C51A_f_hq.jpeg",
       "class": "ImageResourceLevel",
       "width": 2864
      },
      {
       "height": 800,
       "url": "media/panorama_A2A913E5_A818_73A0_41D2_2E5B3782C51A_f.jpeg",
       "class": "ImageResourceLevel",
       "width": 800
      }
     ],
     "class": "ImageResource"
    },
    "class": "CubicPanoramaFrame",
    "back": {
     "levels": [
      {
       "height": 2864,
       "url": "media/panorama_A2A913E5_A818_73A0_41D2_2E5B3782C51A_b_hq.jpeg",
       "class": "ImageResourceLevel",
       "width": 2864
      },
      {
       "height": 800,
       "url": "media/panorama_A2A913E5_A818_73A0_41D2_2E5B3782C51A_b.jpeg",
       "class": "ImageResourceLevel",
       "width": 800
      }
     ],
     "class": "ImageResource"
    },
    "overlays": [
     {
      "class": "HotspotPanoramaOverlay",
      "items": [
       {
        "yaw": 70.6516506928722,
        "image": {
         "levels": [
          {
           "height": 77,
           "url": "media/panorama_A2A913E5_A818_73A0_41D2_2E5B3782C51A_0_HS_0_0.png",
           "class": "ImageResourceLevel",
           "width": 77
          }
         ],
         "class": "ImageResource"
        },
        "pitch": 1.285191412390965,
        "hfov": 3.113044386002007,
        "roll": 0,
        "class": "HotspotPanoramaOverlayImage"
       }
      ],
      "maps": [
       {
        "yaw": 70.6516506928722,
        "image": {
         "levels": [
          {
           "height": 38,
           "url": "media/panorama_A2A913E5_A818_73A0_41D2_2E5B3782C51A_0_HS_0_0_0_map.gif",
           "class": "ImageResourceLevel",
           "width": 38
          }
         ],
         "class": "ImageResource"
        },
        "class": "HotspotPanoramaOverlayMap",
        "pitch": 1.285191412390965,
        "hfov": 3.113044386002007,
        "roll": 0
       }
      ],
      "areas": [
       {
        "mapColor": "#FF0000",
        "toolTip": "C Block Exterior View",
        "class": "HotspotPanoramaOverlayArea",
        "click": "this.mainPlayList.set('selectedIndex', 0)"
       }
      ],
      "enabledInCardboard": true,
      "id": "overlay_A28EB29F_A868_0D9E_41E3_534E5721267C",
      "useHandCursor": true,
      "rollOverDisplay": false
     }
    ],
    "bottom": {
     "levels": [
      {
       "height": 2864,
       "url": "media/panorama_A2A913E5_A818_73A0_41D2_2E5B3782C51A_d_hq.jpeg",
       "class": "ImageResourceLevel",
       "width": 2864
      },
      {
       "height": 800,
       "url": "media/panorama_A2A913E5_A818_73A0_41D2_2E5B3782C51A_d.jpeg",
       "class": "ImageResourceLevel",
       "width": 800
      }
     ],
     "class": "ImageResource"
    },
    "thumbnailUrl": "media/panorama_A2A913E5_A818_73A0_41D2_2E5B3782C51A_t.jpg",
    "left": {
     "levels": [
      {
       "height": 2864,
       "url": "media/panorama_A2A913E5_A818_73A0_41D2_2E5B3782C51A_l_hq.jpeg",
       "class": "ImageResourceLevel",
       "width": 2864
      },
      {
       "height": 800,
       "url": "media/panorama_A2A913E5_A818_73A0_41D2_2E5B3782C51A_l.jpeg",
       "class": "ImageResourceLevel",
       "width": 800
      }
     ],
     "class": "ImageResource"
    },
    "right": {
     "levels": [
      {
       "height": 2864,
       "url": "media/panorama_A2A913E5_A818_73A0_41D2_2E5B3782C51A_r_hq.jpeg",
       "class": "ImageResourceLevel",
       "width": 2864
      },
      {
       "height": 800,
       "url": "media/panorama_A2A913E5_A818_73A0_41D2_2E5B3782C51A_r.jpeg",
       "class": "ImageResourceLevel",
       "width": 800
      }
     ],
     "class": "ImageResource"
    }
   }
  ],
  "class": "Panorama",
  "hfovMax": 120,
  "label": "Block A Pano",
  "partial": false,
  "vfov": 180,
  "thumbnailUrl": "media/panorama_A2A913E5_A818_73A0_41D2_2E5B3782C51A_t.jpg"
 },
 {
  "initialSequence": {
   "movements": [
    {
     "class": "DistancePanoramaCameraMovement",
     "yawSpeed": 8.95,
     "easing": "cubic_in",
     "yawDelta": 18.5
    },
    {
     "class": "DistancePanoramaCameraMovement",
     "yawSpeed": 8.95,
     "easing": "linear",
     "yawDelta": 323
    },
    {
     "class": "DistancePanoramaCameraMovement",
     "yawSpeed": 8.95,
     "easing": "cubic_out",
     "yawDelta": 18.5
    }
   ],
   "restartMovementOnUserInteraction": false,
   "class": "PanoramaCameraSequence"
  },
  "class": "PanoramaCamera",
  "id": "panorama_A2A913E5_A818_73A0_41D2_2E5B3782C51A_camera",
  "automaticZoomSpeed": 10,
  "initialPosition": {
   "yaw": 0,
   "class": "PanoramaCameraPosition",
   "pitch": 0
  }
 },
 {
  "id": "mainPlayList",
  "class": "PlayList",
  "items": [
   {
    "media": "this.panorama_A08179CC_A838_1FE0_41CF_1A619BE9EBE7",
    "class": "PanoramaPlayListItem",
    "begin": "this.setEndToItemIndex(this.mainPlayList, 0, 1)",
    "camera": "this.panorama_A08179CC_A838_1FE0_41CF_1A619BE9EBE7_camera",
    "player": "this.MainViewerPanoramaPlayer"
   },
   {
    "media": "this.panorama_A0D9EE81_A838_1460_41B9_2558663E5EE9",
    "class": "PanoramaPlayListItem",
    "begin": "this.setEndToItemIndex(this.mainPlayList, 1, 2)",
    "camera": "this.panorama_A0D9EE81_A838_1460_41B9_2558663E5EE9_camera",
    "player": "this.MainViewerPanoramaPlayer"
   },
   {
    "media": "this.panorama_A0F65CF6_A838_15A1_41D2_91D69933E331",
    "class": "PanoramaPlayListItem",
    "begin": "this.setEndToItemIndex(this.mainPlayList, 2, 3)",
    "camera": "this.panorama_A0F65CF6_A838_15A1_41D2_91D69933E331_camera",
    "player": "this.MainViewerPanoramaPlayer"
   },
   {
    "media": "this.panorama_A2A913E5_A818_73A0_41D2_2E5B3782C51A",
    "class": "PanoramaPlayListItem",
    "begin": "this.setEndToItemIndex(this.mainPlayList, 3, 0)",
    "camera": "this.panorama_A2A913E5_A818_73A0_41D2_2E5B3782C51A_camera",
    "player": "this.MainViewerPanoramaPlayer"
   }
  ]
 },
 "this.Menu_A68378F1_A868_1DA3_41DD_E0FE28FB864D"
], "children": [
 {
  "progressBarBackgroundColor": [
   "#222222",
   "#444444"
  ],
  "playbackBarBorderColor": "#AAAAAA",
  "toolTipBorderColor": "#767676",
  "toolTipTextShadowBlurRadius": 3,
  "toolTipShadowVerticalLength": 0,
  "progressBackgroundOpacity": 1,
  "toolTipFontWeight": "normal",
  "progressBorderColor": "#AAAAAA",
  "playbackBarHeadBackgroundColorDirection": "vertical",
  "playbackBarBottom": 10,
  "playbackBarHeadShadow": true,
  "playbackBarProgressBackgroundColor": [
   "#222222",
   "#444444"
  ],
  "progressBorderSize": 2,
  "toolTipPaddingBottom": 4,
  "toolTipFontStyle": "normal",
  "progressBarBackgroundColorDirection": "vertical",
  "playbackBarHeadOpacity": 1,
  "progressBackgroundColorDirection": "vertical",
  "progressBarBackgroundColorRatios": [
   0,
   1
  ],
  "progressBarBorderColor": "#000000",
  "playbackBarProgressBorderColor": "#000000",
  "transitionMode": "blending",
  "playbackBarBorderRadius": 4,
  "paddingLeft": 0,
  "paddingBottom": 0,
  "playbackBarProgressBorderRadius": 0,
  "progressBarBorderRadius": 4,
  "playbackBarHeadBackgroundColorRatios": [
   0,
   1
  ],
  "progressBarBorderSize": 0,
  "progressOpacity": 1,
  "playbackBarHeadHeight": 30,
  "playbackBarLeft": 0,
  "progressRight": 10,
  "playbackBarHeadShadowBlurRadius": 3,
  "progressHeight": 20,
  "progressBackgroundColorRatios": [
   0,
   1
  ],
  "progressBottom": 1,
  "progressBarOpacity": 1,
  "progressBorderRadius": 4,
  "playbackBarRight": 0,
  "playbackBarProgressBackgroundColorRatios": [
   0,
   1
  ],
  "playbackBarBackgroundColorDirection": "vertical",
  "playbackBarHeadShadowOpacity": 0.7,
  "toolTipFontFamily": "Arial",
  "height": "100%",
  "shadow": false,
  "id": "MainViewer",
  "width": "100%",
  "playbackBarProgressBorderSize": 0,
  "playbackBarHeadBorderSize": 0,
  "playbackBarHeadShadowColor": "#000000",
  "paddingTop": 0,
  "playbackBarHeadWidth": 6,
  "playbackBarHeight": 20,
  "left": 0,
  "toolTipBorderSize": 1,
  "toolTipPaddingTop": 4,
  "playbackBarBackgroundOpacity": 1,
  "playbackBarBackgroundColor": [
   "#EEEEEE",
   "#CCCCCC"
  ],
  "toolTipPaddingRight": 6,
  "toolTipShadowColor": "#333333",
  "toolTipFontColor": "#606060",
  "borderRadius": 0,
  "toolTipBackgroundColor": "#F6F6F6",
  "toolTipBorderRadius": 3,
  "playbackBarBorderSize": 2,
  "toolTipOpacity": 1,
  "class": "ViewerArea",
  "toolTipPaddingLeft": 6,
  "toolTipShadowOpacity": 1,
  "toolTipTextShadowOpacity": 0,
  "playbackBarHeadBackgroundColor": [
   "#111111",
   "#666666"
  ],
  "top": 0,
  "minHeight": 50,
  "toolTipFontSize": 12,
  "progressBackgroundColor": [
   "#EEEEEE",
   "#CCCCCC"
  ],
  "minWidth": 100,
  "borderSize": 0,
  "playbackBarProgressBackgroundColorDirection": "vertical",
  "toolTipTextShadowColor": "#000000",
  "playbackBarHeadBorderColor": "#000000",
  "toolTipShadowSpread": 0,
  "toolTipShadowBlurRadius": 3,
  "playbackBarHeadShadowHorizontalLength": 0,
  "playbackBarProgressOpacity": 1,
  "progressLeft": 10,
  "playbackBarOpacity": 1,
  "playbackBarHeadBorderRadius": 0,
  "playbackBarHeadShadowVerticalLength": 0,
  "toolTipShadowHorizontalLength": 0,
  "paddingRight": 0
 },
 {
  "width": "100%",
  "height": 142,
  "shadow": false,
  "paddingTop": 0,
  "left": "0%",
  "borderRadius": 0,
  "horizontalAlign": "center",
  "class": "Container",
  "layout": "horizontal",
  "overflow": "scroll",
  "scrollBarMargin": 2,
  "verticalAlign": "middle",
  "children": [
   {
    "width": 360,
    "height": "96.479%",
    "shadow": false,
    "id": "Container_5645B517_5EBA_61DA_41CE_5170204AEC9E",
    "paddingTop": 0,
    "borderRadius": 0,
    "class": "Container",
    "horizontalAlign": "center",
    "layout": "horizontal",
    "overflow": "hidden",
    "scrollBarMargin": 2,
    "verticalAlign": "middle",
    "children": [
     "this.IconButton_5645B517_5EBA_61DA_41D3_52532616D552",
     "this.IconButton_5645B517_5EBA_61DA_41D4_85AB00D87A76",
     "this.IconButton_5645B517_5EBA_61DA_41D1_EAD81768A56B",
     "this.IconButton_5645B517_5EBA_61DA_41A9_0D8835A41A7C",
     {
      "width": 40,
      "height": "100%",
      "shadow": false,
      "id": "Container_5645B517_5EBA_61DA_41AF_FC7FEB37EBCD",
      "paddingTop": 0,
      "borderRadius": 0,
      "class": "Container",
      "horizontalAlign": "center",
      "layout": "vertical",
      "overflow": "hidden",
      "scrollBarMargin": 2,
      "verticalAlign": "middle",
      "children": [
       "this.IconButton_5645B517_5EBA_61DA_41D6_9282B67C524F",
       "this.IconButton_5645B517_5EBA_61DA_41BC_DB59BC8AC3A2",
       "this.IconButton_5645B517_5EBA_61DA_41D6_E94D03438B44"
      ],
      "scrollBarVisible": "rollOver",
      "scrollBarWidth": 10,
      "minHeight": 20,
      "contentOpaque": false,
      "paddingLeft": 0,
      "paddingBottom": 0,
      "scrollBarColor": "#000000",
      "minWidth": 20,
      "borderSize": 0,
      "gap": 4,
      "backgroundOpacity": 0,
      "scrollBarOpacity": 0.5,
      "paddingRight": 0
     },
     "this.IconButton_5645B517_5EBA_61DA_41BA_A43ED1B1FB40",
     "this.IconButton_5645B517_5EBA_61DA_41C5_1BC8156F8BCF",
     {
      "width": 40,
      "height": 40,
      "shadow": false,
      "id": "IconButton_5645B517_5EBA_61DA_41CB_7C4DBBC2A8E5",
      "pressedIconURL": "skin/IconButton_5645B517_5EBA_61DA_41CB_7C4DBBC2A8E5_pressed.png",
      "transparencyActive": true,
      "mode": "toggle",
      "paddingTop": 0,
      "iconURL": "skin/IconButton_5645B517_5EBA_61DA_41CB_7C4DBBC2A8E5.png",
      "cursor": "hand",
      "horizontalAlign": "center",
      "borderRadius": 0,
      "class": "IconButton",
      "verticalAlign": "middle",
      "minHeight": 0,
      "paddingLeft": 0,
      "paddingBottom": 0,
      "minWidth": 0,
      "borderSize": 0,
      "backgroundOpacity": 0,
      "paddingRight": 0
     },
     "this.IconButton_5645B517_5EBA_61DA_41D2_D639DF8052CA"
    ],
    "scrollBarVisible": "rollOver",
    "scrollBarWidth": 10,
    "minHeight": 20,
    "contentOpaque": false,
    "paddingLeft": 0,
    "paddingBottom": 0,
    "scrollBarColor": "#000000",
    "minWidth": 360,
    "borderSize": 0,
    "gap": 4,
    "backgroundOpacity": 0,
    "scrollBarOpacity": 0.5,
    "paddingRight": 0
   }
  ],
  "scrollBarVisible": "rollOver",
  "scrollBarWidth": 10,
  "minHeight": 1,
  "bottom": "0%",
  "contentOpaque": false,
  "paddingLeft": 0,
  "paddingBottom": 0,
  "scrollBarColor": "#000000",
  "minWidth": 1,
  "borderSize": 0,
  "gap": 10,
  "backgroundOpacity": 0,
  "scrollBarOpacity": 0.5,
  "paddingRight": 0
 },
 {
  "width": 252,
  "height": 249,
  "shadow": false,
  "id": "Container_A3251B01_A878_1C63_41D8_B3AC890BDB84",
  "paddingTop": 0,
  "left": "2.24%",
  "borderRadius": 0,
  "class": "Container",
  "horizontalAlign": "center",
  "layout": "vertical",
  "overflow": "hidden",
  "scrollBarMargin": 2,
  "verticalAlign": "middle",
  "children": [
   {
    "width": 200,
    "height": 35,
    "shadow": false,
    "id": "Container_A3A80F24_A818_14A0_41D4_0788DCB5B413",
    "backgroundColor": [
     "#FFFFFF",
     "#FFFFFF"
    ],
    "paddingTop": 0,
    "backgroundColorDirection": "vertical",
    "horizontalAlign": "center",
    "borderRadius": 0,
    "class": "Container",
    "layout": "horizontal",
    "overflow": "hidden",
    "scrollBarMargin": 2,
    "verticalAlign": "middle",
    "backgroundColorRatios": [
     0,
     1
    ],
    "scrollBarVisible": "rollOver",
    "scrollBarWidth": 10,
    "minHeight": 35,
    "contentOpaque": false,
    "paddingLeft": 0,
    "paddingBottom": 0,
    "scrollBarColor": "#000000",
    "minWidth": 200,
    "borderSize": 0,
    "gap": 10,
    "children": [
     {
      "width": "84.5%",
      "height": "85.714%",
      "shadow": false,
      "id": "Label_A23DBA4B_A818_1CE7_41D2_0A05286E332F",
      "paddingTop": 0,
      "click": "this.mainPlayList.set('selectedIndex', 3)",
      "fontSize": 16,
      "borderRadius": 0,
      "class": "Label",
      "horizontalAlign": "center",
      "verticalAlign": "middle",
      "fontStyle": "normal",
      "textDecoration": "none",
      "minHeight": 1,
      "fontFamily": "Arial",
      "paddingLeft": 0,
      "paddingBottom": 0,
      "minWidth": 1,
      "borderSize": 0,
      "text": "Block A Office Space",
      "fontWeight": "normal",
      "fontColor": "#000000",
      "backgroundOpacity": 0,
      "paddingRight": 0
     }
    ],
    "backgroundOpacity": 0.3,
    "scrollBarOpacity": 0.5,
    "paddingRight": 0
   },
   {
    "width": 200,
    "height": 35,
    "shadow": false,
    "id": "Container_A3624E9A_A868_1461_41A1_726BA3B1A2D2",
    "backgroundColor": [
     "#FFFFFF",
     "#FFFFFF"
    ],
    "paddingTop": 0,
    "backgroundColorDirection": "vertical",
    "horizontalAlign": "center",
    "borderRadius": 0,
    "class": "Container",
    "layout": "horizontal",
    "overflow": "hidden",
    "scrollBarMargin": 2,
    "verticalAlign": "middle",
    "backgroundColorRatios": [
     0,
     1
    ],
    "scrollBarVisible": "rollOver",
    "scrollBarWidth": 10,
    "minHeight": 35,
    "contentOpaque": false,
    "paddingLeft": 0,
    "paddingBottom": 0,
    "scrollBarColor": "#000000",
    "minWidth": 200,
    "borderSize": 0,
    "gap": 10,
    "children": [
     {
      "width": "76%",
      "height": "85.714%",
      "shadow": false,
      "id": "Label_A2BACCC1_A868_75E0_41C4_16C7FB84DE9F",
      "paddingTop": 0,
      "click": "this.mainPlayList.set('selectedIndex', 1)",
      "fontSize": 16,
      "borderRadius": 0,
      "class": "Label",
      "horizontalAlign": "center",
      "verticalAlign": "middle",
      "fontStyle": "normal",
      "textDecoration": "none",
      "minHeight": 1,
      "fontFamily": "Arial",
      "paddingLeft": 0,
      "paddingBottom": 0,
      "minWidth": 1,
      "borderSize": 0,
      "text": "Block C Reception",
      "fontWeight": "normal",
      "fontColor": "#000000",
      "backgroundOpacity": 0,
      "paddingRight": 0
     }
    ],
    "backgroundOpacity": 0.3,
    "scrollBarOpacity": 0.5,
    "paddingRight": 0
   },
   {
    "width": 200,
    "height": 35,
    "shadow": false,
    "id": "Container_A3F6B6D2_A868_15E0_41E4_6D32E976ECB4",
    "backgroundColor": [
     "#FFFFFF",
     "#FFFFFF"
    ],
    "paddingTop": 0,
    "backgroundColorDirection": "vertical",
    "horizontalAlign": "center",
    "borderRadius": 0,
    "class": "Container",
    "layout": "horizontal",
    "overflow": "scroll",
    "scrollBarMargin": 2,
    "verticalAlign": "middle",
    "backgroundColorRatios": [
     0,
     1
    ],
    "scrollBarVisible": "rollOver",
    "scrollBarWidth": 10,
    "minHeight": 35,
    "contentOpaque": false,
    "paddingLeft": 0,
    "paddingBottom": 0,
    "scrollBarColor": "#000000",
    "minWidth": 200,
    "borderSize": 0,
    "gap": 10,
    "children": [
     {
      "width": "91.5%",
      "height": "100%",
      "shadow": false,
      "id": "Label_A3041FE8_A868_13A1_41D6_58208B4D380A",
      "paddingTop": 0,
      "click": "this.mainPlayList.set('selectedIndex', 2)",
      "fontSize": 16,
      "borderRadius": 0,
      "class": "Label",
      "horizontalAlign": "center",
      "verticalAlign": "middle",
      "fontStyle": "normal",
      "textDecoration": "none",
      "minHeight": 1,
      "fontFamily": "Arial",
      "paddingLeft": 0,
      "paddingBottom": 0,
      "minWidth": 1,
      "borderSize": 0,
      "text": "Block  C Office Space",
      "fontWeight": "normal",
      "fontColor": "#000000",
      "backgroundOpacity": 0,
      "paddingRight": 0
     }
    ],
    "backgroundOpacity": 0.3,
    "scrollBarOpacity": 0.5,
    "paddingRight": 0
   },
   {
    "width": 200,
    "height": 35,
    "shadow": false,
    "id": "Container_A37E83DA_A878_13E0_41D6_547C20FD44D1",
    "backgroundColor": [
     "#FFFFFF",
     "#FFFFFF"
    ],
    "paddingTop": 0,
    "backgroundColorDirection": "vertical",
    "horizontalAlign": "center",
    "borderRadius": 0,
    "class": "Container",
    "layout": "horizontal",
    "overflow": "scroll",
    "scrollBarMargin": 2,
    "verticalAlign": "middle",
    "backgroundColorRatios": [
     0,
     1
    ],
    "scrollBarVisible": "rollOver",
    "scrollBarWidth": 10,
    "minHeight": 35,
    "contentOpaque": false,
    "paddingLeft": 0,
    "paddingBottom": 0,
    "scrollBarColor": "#000000",
    "minWidth": 200,
    "borderSize": 0,
    "gap": 10,
    "children": [
     {
      "width": "93%",
      "height": "85.714%",
      "shadow": false,
      "id": "Label_A328A181_A878_0C60_41D6_D56AEF986328",
      "paddingTop": 0,
      "click": "this.mainPlayList.set('selectedIndex', 0)",
      "fontSize": 16,
      "borderRadius": 0,
      "class": "Label",
      "horizontalAlign": "center",
      "verticalAlign": "middle",
      "fontStyle": "normal",
      "textDecoration": "none",
      "minHeight": 1,
      "fontFamily": "Arial",
      "paddingLeft": 0,
      "paddingBottom": 0,
      "minWidth": 1,
      "borderSize": 0,
      "text": "Block C Exterior View",
      "fontWeight": "normal",
      "fontColor": "#000000",
      "backgroundOpacity": 0,
      "paddingRight": 0
     }
    ],
    "backgroundOpacity": 0.3,
    "scrollBarOpacity": 0.5,
    "paddingRight": 0
   }
  ],
  "scrollBarVisible": "rollOver",
  "scrollBarWidth": 10,
  "minHeight": 240,
  "top": "1.74%",
  "contentOpaque": false,
  "paddingLeft": 0,
  "paddingBottom": 0,
  "scrollBarColor": "#000000",
  "minWidth": 250,
  "borderSize": 0,
  "gap": 23,
  "backgroundOpacity": 0,
  "scrollBarOpacity": 0.5,
  "paddingRight": 0
 }
], 
 "width": "100%",
 "shadow": false,
 "mouseWheelEnabled": true,
 "height": "100%",
 "paddingTop": 0,
 "borderRadius": 0,
 "horizontalAlign": "left",
 "buttonToggleMute": "this.IconButton_5645B517_5EBA_61DA_41CB_7C4DBBC2A8E5",
 "overflow": "visible",
 "scrollBarMargin": 2,
 "verticalAlign": "top",
 "class": "Player",
 "scrollBarVisible": "rollOver",
 "scrollBarWidth": 10,
 "minHeight": 20,
 "start": "this.mainPlayList.set('selectedIndex', 0)",
 "contentOpaque": false,
 "paddingLeft": 0,
 "scripts": {
  "showWindow": function(w, autoCloseMilliSeconds, containsAudio){    var closeFunction = function(){       clearAutoClose();       this.resumePlayers(playersPaused, !containsAudio);       w.unbind('close', closeFunction, this);   };   var clearAutoClose = function(){       w.unbind('click', clearAutoClose, this);       if(timeoutID != undefined){           clearTimeout(timeoutID);       }   };   var timeoutID = undefined;   if(autoCloseMilliSeconds){       var autoCloseFunction = function(){           w.hide();       };       w.bind('click', clearAutoClose, this);       timeoutID = setTimeout(autoCloseFunction, autoCloseMilliSeconds);   }   var playersPaused = this.pauseCurrentPlayers(!containsAudio);   w.bind('close', closeFunction, this);   w.show(this, true); },
  "fixTogglePlayPauseButton": function(player){    var state = player.get('state');   var button = player.get('buttonPlayPause');   if(typeof button !== 'undefined' && player.get('state') == 'playing'){       button.set('pressed', true);   } },
  "updatePlayListsUI": function(playLists){    var changeFunction = function(event){       var playListDispatched = event.source;       var selectedIndex = playListDispatched.get('selectedIndex');       if(selectedIndex < 0)           return;       var media = playListDispatched.get('items')[selectedIndex].get('media');       for(var i = 0, count = playLists.length; i<count; ++i){           var playList = playLists[i];           if(playList != playListDispatched){               var items = playList.get('items');               for(var j = 0, countJ = items.length; j<countJ; ++j){                   if(items[j].get('media') == media){                       if(playList.get('selectedIndex') != j){                           playList.set('selectedIndex', j);                       }                       break;                   }               }           }       }   };   for(var i = 0, count = playLists.length; i<count; ++i){       playLists[i].bind('change', changeFunction, this);   } },
  "startPanoramaWithCamera": function(playList, index, camera){    var playListItem = playList.get('items')[index];   var previousCamera = playListItem.get('camera');   playListItem.set('camera', camera);   var restoreCameraOnStop = function(){       playListItem.set('camera', previousCamera);       playListItem.unbind('stop', restoreCameraOnStop, this);   };   playListItem.bind('stop', restoreCameraOnStop, this);   playList.set('selectedIndex', index); },
  "updateMediaLabelFromPlayList": function(playList, htmlText, playListItemStopToDispose){    var changeFunction = function(){       var index = playList.get('selectedIndex');       if(index >= 0){           var beginFunction = function(){               playListItem.unbind('begin', beginFunction);               setMediaLabel(index);           };           var setMediaLabel = function(index){               var media = playListItem.get('media');               var text = media.get('data');               if(!text)                   text = media.get('label');               setHtml(text);           };           var setHtml = function(text){               htmlText.set('html', '<div style=\"text-align:left\"><SPAN STYLE=\"color:#FFFFFF;font-size:12px;font-family:Verdana\"><span color=\"white\" font-family=\"Verdana\" font-size=\"12px\">' + text + '</SPAN></div>');                          };           var playListItem = playList.get('items')[index];           if(htmlText.get('html')){               setHtml('Loading...');               playListItem.bind('begin', beginFunction);           }           else{               setMediaLabel(index);           }       }   };   var disposeFunction = function(){       htmlText.set('html', undefined);       playList.unbind('change', changeFunction, this);       playListItemStopToDispose.unbind('stop', disposeFunction, this);   };   if(playListItemStopToDispose){       playListItemStopToDispose.bind('stop', disposeFunction, this);   }   playList.bind('change', changeFunction, this);   changeFunction(); },
  "playAudioList": function(audios){    if(audios.length == 0) return;   var currentAudioCount = -1;   var currentAudio;   var playGlobalAudioFunction = this.playGlobalAudio;   var playNext = function(){       if(++currentAudioCount >= audios.length)           currentAudioCount = 0;       currentAudio = audios[currentAudioCount];       playGlobalAudioFunction(currentAudio, playNext);   };   playNext(); },
  "setEndToItemIndex": function(playList, fromIndex, toIndex){    var endFunction = function(){       if(playList.get('selectedIndex') == fromIndex)           playList.set('selectedIndex', toIndex);   };   this.executeFunctionWhenChange(playList, fromIndex, endFunction); },
  "getActivePlayerWithViewer": function(viewerArea){    var players = this.getByClassName('PanoramaPlayer');   players = players.concat(this.getByClassName('VideoPlayer'));   players = players.concat(this.getByClassName('PhotoAlbumPlayer'));   players = players.concat(this.getByClassName('MapPlayer'));   var i = players.length;   while(i-- > 0){       var player = players[i];       if(player.get('viewerArea') == viewerArea && player.get('state') == 'playing') {           return player;       }   }   return undefined; },
  "pauseGlobalAudios": function(caller){    var audios = window.currentGlobalAudios;   window.currentGlobalAudiosActionCaller = caller;   if(!audios) return;   for(var i = 0, count = audios.length; i<count; i++){       audios[i].pause();   } },
  "setComponentVisibility": function(component, visible, applyAt, effect, propertyEffect, ignoreClearTimeout){    var changeVisibility = function(){        if(effect && propertyEffect){            component.set(propertyEffect, effect);        }        component.set('visible', visible);       if(component.get('class') == 'ViewerArea'){           try{               if(visible) component.restart();               else        component.pause();           }           catch(e){};       }   };   var effectTimeoutName = 'effectTimeout_'+component.get('id');   if(!ignoreClearTimeout && window.hasOwnProperty(effectTimeoutName)){       var effectTimeout = window[effectTimeoutName];       if(effectTimeout instanceof Array){           for(var i=0; i<effectTimeout.length; i++){ clearTimeout(effectTimeout[i]) }       }else{           clearTimeout(effectTimeout);       }       delete window[effectTimeoutName];   }   else if(visible == component.get('visible') && !ignoreClearTimeout)       return;   if(applyAt && applyAt > 0){       var effectTimeout = setTimeout(function(){            if(window[effectTimeoutName] instanceof Array) {                var arrayTimeoutVal = window[effectTimeoutName];               var index = arrayTimeoutVal.indexOf(effectTimeout);               arrayTimeoutVal.splice(index, 1);               if(arrayTimeoutVal.length == 0){                   delete window[effectTimeoutName];               }           }else{               delete window[effectTimeoutName];            }           changeVisibility();        }, applyAt);       if(window.hasOwnProperty(effectTimeoutName)){           window[effectTimeoutName] = [window[effectTimeoutName], effectTimeout];       }else{           window[effectTimeoutName] = effectTimeout;       }   }   else{       changeVisibility();   } },
  "getPlayListWithMedia": function(media, onlySelected){    var playLists = this.getByClassName('PlayList');   for(var i = 0, count = playLists.length; i<count; ++i){       var playList = playLists[i];       if(onlySelected && playList.get('selectedIndex') == -1)           continue;       var items = playList.get('items');       for(var j = 0, countJ = items.length; j<countJ; ++j){           if(items[j].get('media') == media)               return playList;       }   }   return undefined; },
  "isCardboardVideMode": function(media, onlySelected){    var players = this.getByClassName('PanoramaPlayer');   return players.length > 0 && players[0].get('viewMode') == 'cardboard'; },
  "stopGlobalAudio": function(audio){    var audios = window.currentGlobalAudios;   if(audios){       var index = audios.indexOf(audio);       if(index != -1){           audios.splice(index, 1);       }   }   audio.stop(); },
  "loopAlbum": function(playList, index){    var playListItem = playList.get('items')[index];   var player = playListItem.get('player');   var loopFunction = function(){       player.play();   };   this.executeFunctionWhenChange(playList, index, loopFunction); },
  "showComponentsWhileMouseOver": function(parentComponent, components, durationVisibleWhileOut){    var rollOutFunction = function(){       var rollOverFunction = function(){           clearTimeout(timeoutID);           dispose();       };       var timeoutFunction = function(){           setVisibility(false);           dispose();       };       var dispose = function(){           parentComponent.unbind('rollOver', rollOverFunction, this);       };       parentComponent.unbind('rollOut', rollOutFunction, this);       parentComponent.bind('rollOver', rollOverFunction, this);       var timeoutID = setTimeout(timeoutFunction, durationVisibleWhileOut);   };   var setVisibility = function(visible){       for(var i = 0, length = components.length; i<length; i++){           components[i].set('visible', visible);       }   };   parentComponent.bind('rollOut', rollOutFunction, this);   setVisibility(true); },
  "pauseGlobalAudiosWhilePlayItem": function(playList, index, caller){    var audios = window.currentGlobalAudios;   if(!audios) return;   var resumeFunction = this.resumeGlobalAudios;   var endFunction = function(){       resumeFunction(caller);   };   this.pauseGlobalAudios(caller);   this.executeFunctionWhenChange(playList, index, endFunction, endFunction); },
  "shareTwitter": function(url){    window.open('https://twitter.com/intent/tweet?source=webclient&url=' + url, '_blank'); },
  "resumeGlobalAudios": function(caller){    if(window.currentGlobalAudiosActionCaller && window.currentGlobalAudiosActionCaller != caller)       return;   window.currentGlobalAudiosActionCaller = undefined;   var audios = window.currentGlobalAudios;   if(!audios) return;   for(var i = 0, count = audios.length; i<count; i++){       audios[i].play();   } },
  "executeFunctionWhenChange": function(playList, index, endFunction, changeFunction){    var self = this;   var changePlayListFunction = function(event){       if(event.data.previousSelectedIndex == index){           if(changeFunction) changeFunction();           if(endFunction) endObject.unbind('end', endFunction, self);           playList.unbind('change', changePlayListFunction, self);       }   };   if(endFunction){       var playListItem = playList.get('items')[index];       var camera = playListItem.get('camera');       if(camera){           var endObject = camera.get('initialSequence');           if(!endObject) return;       }       else{           endObject = playListItem.get('media');       }       endObject.bind('end', endFunction, this);   }   playList.bind('change', changePlayListFunction, this); },
  "pauseCurrentPlayers": function(onlyPauseCameraIfPanorama){    var players = this.getByClassName('PanoramaPlayer');   players = players.concat(this.getByClassName('VideoPlayer'));   players = players.concat(this.getByClassName('PhotoAlbumPlayer'));   var i = players.length;   while(i-- > 0){       var player = players[i];       if(player.get('state') == 'playing') {           if(onlyPauseCameraIfPanorama && typeof player.pauseCamera !== 'undefined'){               player.pauseCamera();           }           else{               player.pause();           }       }       else {           players.splice(i, 1);       }   }   return players; },
  "resumePlayers": function(players, onlyResumeCameraIfPanorama){    for(var i = 0; i<players.length; ++i){       var player = players[i];       if(onlyResumeCameraIfPanorama && typeof player.resumeCamera !== 'undefined'){           player.resumeCamera();       }       else{           player.play();       }   } },
  "loadFromCurrentMediaPlayList": function(playList, delta){    var currentIndex = playList.get('selectedIndex');   var totalItems = playList.get('items').length;   var newIndex = (currentIndex + delta) % totalItems;   while(newIndex < 0){       newIndex = totalItems + newIndex;   };   if(currentIndex != newIndex){       playList.set('selectedIndex', newIndex);   }; },
  "showPopupMedia": function(w, media, playList, popupMaxWidth, popupMaxHeight, autoCloseWhenFinished, containsAudio){    var self = this;   var closeFunction = function(){       this.resumePlayers(playersPaused, !containsAudio);       if(isVideo) {           this.unbind('resize', resizeFunction, this);       }       w.unbind('close', closeFunction, this);   };   var endFunction = function(){       w.hide();   };   var resizeFunction = function(){       var parentWidth = self.get('actualWidth');       var parentHeight = self.get('actualHeight');       var mediaWidth = media.get('width');       var mediaHeight = media.get('height');       var popupMaxWidthNumber = parseFloat(popupMaxWidth) / 100;       var popupMaxHeightNumber = parseFloat(popupMaxHeight) / 100;       var windowWidth = popupMaxWidthNumber * parentWidth;       var windowHeight = popupMaxHeightNumber * parentHeight;       var footerHeight = w.get('footerHeight');       var headerHeight = w.get('headerHeight');       if(!headerHeight) {           var closeButtonHeight = w.get('closeButtonIconHeight') + w.get('closeButtonPaddingTop') + w.get('closeButtonPaddingBottom');           var titleHeight = w.get('titleFontSize') + w.get('titlePaddingTop') + w.get('titlePaddingBottom');           headerHeight = closeButtonHeight > titleHeight ? closeButtonHeight : titleHeight;           headerHeight += w.get('headerPaddingTop') + w.get('headerPaddingBottom');       }       if(!footerHeight) {           footerHeight = 0;       }       var contentWindowWidth = windowWidth - w.get('bodyPaddingLeft') - w.get('bodyPaddingRight') - w.get('paddingLeft') - w.get('paddingRight');       var contentWindowHeight = windowHeight - headerHeight - footerHeight - w.get('bodyPaddingTop') - w.get('bodyPaddingBottom') - w.get('paddingTop') - w.get('paddingBottom');       var parentAspectRatio = contentWindowWidth / contentWindowHeight;       var mediaAspectRatio = mediaWidth / mediaHeight;       if(parentAspectRatio > mediaAspectRatio) {           windowWidth = contentWindowHeight * mediaAspectRatio + w.get('bodyPaddingLeft') + w.get('bodyPaddingRight') + w.get('paddingLeft') + w.get('paddingRight');       }       else {           windowHeight = contentWindowWidth / mediaAspectRatio + headerHeight + footerHeight + w.get('bodyPaddingTop') + w.get('bodyPaddingBottom') + w.get('paddingTop') + w.get('paddingBottom');       }       if(windowWidth > parentWidth * popupMaxWidthNumber) {           windowWidth = parentWidth * popupMaxWidthNumber;       }       if(windowHeight > parentHeight * popupMaxHeightNumber) {           windowHeight = parentHeight * popupMaxHeightNumber;       }       w.set('width', windowWidth);       w.set('height', windowHeight);       w.set('x', (parentWidth - w.get('actualWidth')) * 0.5);       w.set('y', (parentHeight - w.get('actualHeight')) * 0.5);   };   if(autoCloseWhenFinished){       this.executeFunctionWhenChange(playList, 0, endFunction);   }   var isVideo = media.get('video') != undefined;   if(isVideo){       this.bind('resize', resizeFunction, this);       resizeFunction();   }   else {       w.set('width', popupMaxWidth);       w.set('height', popupMaxHeight);   }   var playersPaused = this.pauseCurrentPlayers(!containsAudio);   w.bind('close', closeFunction, this);   w.show(this, true); },
  "shareFacebook": function(url){    window.open('https://www.facebook.com/sharer/sharer.php?u=' + url, '_blank'); },
  "visibleComponentsIfPlayerFlagEnabled": function(components, playerFlag){    var enabled = this.get(playerFlag);   for(var i in components){       components[i].set('visible', enabled);   } },
  "updateVideoCues": function(playList, index){    var playListItem = playList.get('items')[index];   var video = playListItem.get('media');   if(video.get('cues').length == 0)       return;   var player = playListItem.get('player');   var cues = [];   var changeFunction = function(){       if(playList.get('selectedIndex') != index){           video.unbind('cueChange', cueChangeFunction, this);           playList.unbind('change', changeFunction, this);       }   };   var cueChangeFunction = function(event){       var activeCues = event.data.activeCues;       for(var i = 0, count = cues.length; i<count; ++i){           var cue = cues[i];           if(activeCues.indexOf(cue) == -1 && (cue.get('startTime') > player.get('currentTime') || cue.get('endTime') < player.get('currentTime')+0.5)){               cue.trigger('end');           }       }       cues = activeCues;   };   video.bind('cueChange', cueChangeFunction, this);   playList.bind('change', changeFunction, this); },
  "setMapLocation": function(panoramaPlayListItem, mapPlayer){    var resetFunction = function(){       panoramaPlayListItem.unbind('stop', resetFunction, this);       player.set('mapPlayer', null);   };   panoramaPlayListItem.bind('stop', resetFunction, this);   var player = panoramaPlayListItem.get('player');   player.set('mapPlayer', mapPlayer); },
  "changeBackgroundWhilePlay": function(playList, index, color){    var changeFunction = function(event){       if(event.data.previousSelectedIndex == index){           playList.unbind('change', changeFunction, this);           if((color == viewerArea.get('backgroundColor')) && (colorRatios == viewerArea.get('backgroundColorRatios'))){               viewerArea.set('backgroundColor', backgroundColorBackup);               viewerArea.set('backgroundColorRatios', backgroundColorRatiosBackup);           }       }   };   var playListItem = playList.get('items')[index];   var player = playListItem.get('player');   var viewerArea = player.get('viewerArea');   var backgroundColorBackup = viewerArea.get('backgroundColor');   var backgroundColorRatiosBackup = viewerArea.get('backgroundColorRatios');   var colorRatios = [0];   if((color != backgroundColorBackup) || (colorRatios != backgroundColorRatiosBackup)){       viewerArea.set('backgroundColor', color);       viewerArea.set('backgroundColorRatios', colorRatios);       playList.bind('change', changeFunction, this);   } },
  "shareGoogle": function(url){    window.open('https://plus.google.com/share?url=' + url, '_blank'); },
  "showPopupImage": function(image, toggleImage, customWidth, customHeight, showEffect, hideEffect, closeButtonProperties, autoCloseMilliSeconds, audio, stopBackgroundAudio, loadedCallback, hideCallback){    var self = this;   var closed = false;   var playerClickFunction = function() {       zoomImage.unbind('loaded', loadedFunction, self);       hideFunction();   };   var clearAutoClose = function(){       zoomImage.unbind('click', clearAutoClose, this);       if(timeoutID != undefined){           clearTimeout(timeoutID);       }   };   var loadedFunction = function(){       self.unbind('click', playerClickFunction, self);       veil.set('visible', true);       setCloseButtonPosition();       closeButton.set('visible', true);       zoomImage.unbind('loaded', loadedFunction, this);       zoomImage.bind('userInteractionStart', userInteractionStartFunction, this);       zoomImage.bind('userInteractionEnd', userInteractionEndFunction, this);       timeoutID = setTimeout(timeoutFunction, 200);   };   var timeoutFunction = function(){       timeoutID = undefined;       if(autoCloseMilliSeconds){           var autoCloseFunction = function(){               hideFunction();           };           zoomImage.bind('click', clearAutoClose, this);           timeoutID = setTimeout(autoCloseFunction, autoCloseMilliSeconds);       }       zoomImage.bind('backgroundClick', hideFunction, this);       if(toggleImage) {           zoomImage.bind('click', toggleFunction, this);           zoomImage.set('imageCursor', 'hand');       }       closeButton.bind('click', hideFunction, this);       if(loadedCallback)           loadedCallback();   };   var hideFunction = function() {       closed = true;       if(timeoutID)           clearTimeout(timeoutID);       if(autoCloseMilliSeconds)           clearAutoClose();       if(hideCallback)           hideCallback();       zoomImage.set('visible', false);       if(hideEffect && hideEffect.get('duration') > 0){           hideEffect.bind('end', endEffectFunction, this);       }       else{           zoomImage.set('image', null);       }       closeButton.set('visible', false);       veil.set('visible', false);       self.unbind('click', playerClickFunction, self);       zoomImage.unbind('backgroundClick', hideFunction, this);       zoomImage.unbind('userInteractionStart', userInteractionStartFunction, this);       zoomImage.unbind('userInteractionEnd', userInteractionEndFunction, this, true);       if(toggleImage) {           zoomImage.unbind('click', toggleFunction, this);           zoomImage.set('cursor', 'default');       }       closeButton.unbind('click', hideFunction, this);       self.resumePlayers(playersPaused, audio == null || stopBackgroundAudio);       if(audio){           if(stopBackgroundAudio){               self.resumeGlobalAudios();           }           self.stopGlobalAudio(audio);       }   };   var endEffectFunction = function() {       zoomImage.set('image', null);       hideEffect.unbind('end', endEffectFunction, this);   };   var toggleFunction = function() {       zoomImage.set('image', isToggleVisible() ? image : toggleImage);   };   var isToggleVisible = function() {       return zoomImage.get('image') == toggleImage;   };   var setCloseButtonPosition = function() {       var right = zoomImage.get('actualWidth') - zoomImage.get('imageLeft') - zoomImage.get('imageWidth') + 10;       var top = zoomImage.get('imageTop') + 10;       if(right < 10) right = 10;       if(top < 10) top = 10;       closeButton.set('right', right);       closeButton.set('top', top);   };   var userInteractionStartFunction = function() {       if(timeoutUserInteractionID){           clearTimeout(timeoutUserInteractionID);           timeoutUserInteractionID = undefined;       }       else{           closeButton.set('visible', false);       }   };   var userInteractionEndFunction = function() {       if(!closed){           timeoutUserInteractionID = setTimeout(userInteractionTimeoutFunction, 300);       }   };   var userInteractionTimeoutFunction = function() {       timeoutUserInteractionID = undefined;       closeButton.set('visible', true);       setCloseButtonPosition();   };   var veil = this.veilPopupPanorama;   var zoomImage = this.zoomImagePopupPanorama;   var closeButton = this.closeButtonPopupPanorama;   if(closeButtonProperties){       for(var key in closeButtonProperties){           closeButton.set(key, closeButtonProperties[key]);       }   }   var playersPaused = this.pauseCurrentPlayers(audio == null || !stopBackgroundAudio);   if(audio){       if(stopBackgroundAudio){           this.pauseGlobalAudios();       }       this.playGlobalAudio(audio);   }   var timeoutID = undefined;   var timeoutUserInteractionID = undefined;   zoomImage.bind('loaded', loadedFunction, this);   setTimeout(function(){ self.bind('click', playerClickFunction, self, false); }, 0);   zoomImage.set('image', image);   zoomImage.set('customWidth', customWidth);   zoomImage.set('customHeight', customHeight);   zoomImage.set('showEffect', showEffect);   zoomImage.set('hideEffect', hideEffect);   zoomImage.set('visible', true);   return zoomImage; },
  "autotriggerAtStart": function(player, callback){    var stateChangeFunction = function(event){        if(event.data.state == 'playing'){           callback();           player.unbind('stateChange', stateChangeFunction, this);       }   };   player.bind('stateChange', stateChangeFunction, this); },
  "setMediaBehaviour": function(playList, index, mediaDispatcher){    var self = this;   var stateChangeFunction = function(event){       if(event.data.state == 'stopped'){           dispose();       }   };   var changeFunction = function(){       var index = playListDispatcher.get('selectedIndex');       if(index != -1){           indexDispatcher = index;           dispose();       }   };   var dispose = function(){       if(!playListDispatcher) return;       playList.set('selectedIndex', -1);       if(panoramaSequence && panoramaSequenceIndex != -1){           if(panoramaSequence) {               if(panoramaSequenceIndex > 0 && panoramaSequence.get('movements')[panoramaSequenceIndex-1].get('class') == 'TargetPanoramaCameraMovement'){                   var initialPosition = camera.get('initialPosition');                   var oldYaw = initialPosition.get('yaw');                   var oldPitch = initialPosition.get('pitch');                   var oldHfov = initialPosition.get('hfov');                   var previousMovement = panoramaSequence.get('movements')[panoramaSequenceIndex-1];                   initialPosition.set('yaw', previousMovement.get('targetYaw'));                   initialPosition.set('pitch', previousMovement.get('targetPitch'));                   initialPosition.set('hfov', previousMovement.get('targetHfov'));                   var restoreInitialPositionFunction = function(event){                       initialPosition.set('yaw', oldYaw);                       initialPosition.set('pitch', oldPitch);                       initialPosition.set('hfov', oldHfov);                       itemDispatcher.unbind('end', restoreInitialPositionFunction, self);                   };                   itemDispatcher.bind('end', restoreInitialPositionFunction, self);               }               panoramaSequence.set('movementIndex', panoramaSequenceIndex);           }       }       playListDispatcher.set('selectedIndex', indexDispatcher);       if(player){           player.unbind('stateChange', stateChangeFunction, self);       }       if(sameViewerArea){           if(playList != playListDispatcher)               playListDispatcher.unbind('change', changeFunction, self);       }       else{           viewerArea.set('visible', false);       }       playListDispatcher = undefined;   };   if(!mediaDispatcher){       var currentIndex = playList.get('selectedIndex');       var currentPlayer = (currentIndex != -1) ? playList.get('items')[playList.get('selectedIndex')].get('player') : this.getActivePlayerWithViewer(this.MainViewer);       if(currentPlayer) {           if(currentPlayer.get('panorama')) mediaDispatcher = currentPlayer.get('panorama');           else if(currentPlayer.get('video')) mediaDispatcher = currentPlayer.get('video');           else if(currentPlayer.get('photoAlbum')) mediaDispatcher = currentPlayer.get('photoAlbum');           else if(currentPlayer.get('map')) mediaDispatcher = currentPlayer.get('map');       }   }   var playListDispatcher = mediaDispatcher ? this.getPlayListWithMedia(mediaDispatcher, true) : undefined;   if(!playListDispatcher){       playList.set('selectedIndex', index);       return;   }   var indexDispatcher = playListDispatcher.get('selectedIndex');   if(playList.get('selectedIndex') == index || indexDispatcher == -1){       return;   }   var item = playList.get('items')[index];   var itemDispatcher = playListDispatcher.get('items')[indexDispatcher];   var viewerArea = item.get('player').get('viewerArea');   var sameViewerArea = viewerArea == itemDispatcher.get('player').get('viewerArea');   if(sameViewerArea){       if(playList != playListDispatcher){           playListDispatcher.set('selectedIndex', -1);           playListDispatcher.bind('change', changeFunction, this);       }   }   else{       viewerArea.set('visible', true);   }   var panoramaSequenceIndex = -1;   var panoramaSequence = undefined;   var camera = itemDispatcher.get('camera');   if(camera){       panoramaSequence = camera.get('initialSequence');       if(panoramaSequence) {           panoramaSequenceIndex = panoramaSequence.get('movementIndex');       }   }   playList.set('selectedIndex', index);   var player = undefined;   if(item.get('player') != itemDispatcher.get('player')){       player = item.get('player');       player.bind('stateChange', stateChangeFunction, this);   }   this.executeFunctionWhenChange(playList, index, dispose); },
  "playGlobalAudio": function(audio, endCallback){    var endFunction = function(){       audio.unbind('end', endFunction, this);       this.stopGlobalAudio(audio);       if(endCallback)           endCallback();   };   var audios = window.currentGlobalAudios;   if(!audios){       audios = window.currentGlobalAudios = [audio];   }   else if(audios.indexOf(audio) == -1){       audios.push(audio);   }   audio.bind('end', endFunction, this);   audio.play(); },
  "playGlobalAudioWhilePlay": function(playList, index, audio, endCallback){    var changeFunction = function(event){       if(event.data.previousSelectedIndex == index){           this.stopGlobalAudio(audio);           playList.unbind('change', changeFunction, this);           if(endCallback)               endCallback();       }   };   playList.bind('change', changeFunction, this);   this.playGlobalAudio(audio, endCallback); }
 },
 "paddingBottom": 0,
 "scrollBarColor": "#000000",
 "minWidth": 20,
 "borderSize": 0,
 "gap": 10,
 "layout": "absolute",
 "backgroundPreloadEnabled": true,
 "scrollBarOpacity": 0.5,
 "paddingRight": 0
})